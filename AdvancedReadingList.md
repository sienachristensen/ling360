For those who want to pursue more education in Natural Language Processing (NLP) beyond the basics that this class offers.  Then here is a recommended list of books and online resources:

## Elixir specific:
- [Machine Learning in Elixir -Sean Moriarity (unfinished/early access as of 6 January 2024)](https://pragprog.com/titles/smelixir/machine-learning-in-elixir/) (book)
    - Very well written, engaging book
- [Deep Learning Axon Library Presentation at Elixir Conf 2022](https://www.youtube.com/watch?v=NWXSiZ-vi-o)
    - Great presentation by the same author as the book above

## Python-specific:
[Fast AI (free course online)](https://course.fast.ai/)

- \* Natural Language Processing with Transformers, Revised Edition -Lewis Tunstall et al. (book)
    - this book is more advance and has some assumptions about your level of understanding of the Python libraries for deep learning
and related mathematics.  It includes a list of recommended pre-requisites.

## More General:
- [Matrix Algebra for Engineers (free course online)](https://www.youtube.com/watch?v=IZcyZHomFQc&list=PLkZjai-2Jcxlg-Z1roB0pUwFU-P58tvOx)

- No specific resource but you should know some calculus fundamentals

- \* Essential Math for AI -Hala Nelson (book)


(* these books are available via BYU library as digital books)



