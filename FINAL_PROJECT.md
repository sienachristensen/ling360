# Final project info

1. Create a program that:

- helps you to accomplish a linguistic task related to your area of interest
- requires you to draw on programming skills learned in this class
- but also, importantly, stretches your programming ability
- contains appropriate helpful comments

2. Post a screencast video:

- 8-10 minute oral presentation of your project with:

- a clear explanation of the linguistic problem or question you set out to answer and your reason for doing so (one slide)
-. a clear explanation of the logic of your program and the data structures and functions used (one slide)
-i. an explanation of the challenges you faced and the stretching that your programming skills had to go through in order to complete the project (one slide)
-. a short explanation of the (main) result of your project; how can the research question be answered or the problem solved? (one slide)
- in addition to the previous components of your oral presentation, your body language and voice during your presentation will be accessed

3. Turn into the CMS:

- Your script or scripts
- Relevant output from your program (if it cannot be reproduced by the script)
- 900-1,200 word write-up that addresses at least the following:

- What did you set out to accomplish and why?
-. What were the various steps you took to accomplish your goal?
-i. What challenges did you run into and how did you overcome them?
-. What have you learned in this class that (a) helped you with your final project and (b) will/might help you in your future?
- Include a word count at the end of the document.

There is no final exam in this class. Rather, we will use the final exam period for students to post a video or narrated PowerPoint to the CMS (see schedule for due date) and for students to watch and respond to at least 5 presentations of classmates.

Note: Please review the rubrics attached to the three components of the final project (i.e., program, presentation, paper) in the CMS.

