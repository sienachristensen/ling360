# Assignment 1
## English Noun Plurals

**Instructions**:

Create a program that requests a singular noun from the user and prints to screen the plural form of that noun. Use the website below or other resources to become more familiar with singular and plural nouns in English. Do the best you can to account for as many of the irregular plural forms as you can, but don't worry about getting every last irregular form. Turn into the CMS your .py file named with your last name and the assignment number, for example, "Brown_01.py".

[Grammerly Plural Nouns Blog](https://www.grammarly.com/blog/plural-nouns/)

**Optional**:

Modify your program so that it also converts plural nouns into singular nouns and prints to screen.
