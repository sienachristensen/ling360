# Assignment 10
## Web Scraping

[Assignment Preview Video](https://youtu.be/eOod40mrccU)

**Instructions**:

Write a Python program that webscrapes in order to create a micro corpus of texts from the internet. Your program should start at a webpage of your choice and then collect all links on the starting page. Next, the program should randomly visit these links and scrape out the text with `justext` (installation required) until the text of ten webpages has been successfully saved to separate TXT files on your hard drive. If `justext` only finds boilerplate text on a particular webpage or the program fails to retrieve the webpage, your program should not save an empty TXT file, but rather should move onto the next link. If there are fewer than ten links on the starting page, all the links available should be visited. Each TXT file should have a distinct name so that when finished, the program will have written ten TXT files with the text of ten webpages.

Turn into the CMS a .zip file with your Python program as a .py file, and ten .txt files with the text of the webpages your web crawler visited.