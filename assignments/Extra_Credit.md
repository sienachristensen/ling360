# Extra Credit Assignment

## Read these two pages within Hugging Face's NLP Course:

1. [https://huggingface.co/learn/nlp-course/chapter1/2](https://huggingface.co/learn/nlp-course/chapter1/2)

2. [https://huggingface.co/learn/nlp-course/chapter1/c3](https://huggingface.co/learn/nlp-course/chapter1/c3)

Don't worry about doing the code examples unless you want to.  I recommend
just reading through everything first.  Reading both links should take about
15 minutes if you just read through the code examples without trying to do them.

Decide on one of these available tasks to perform on a small amount of text of
your choice (a sentence or a whole document):

- feature-extraction (get the vector representation of a text)
- fill-mask
- ner (named entity recognition)
- question-answering
- sentiment-analysis
- summarization
- text-generation
- translation
- zero-shot-classification

Install `transformers` and write the code to perform said task on the
sentence(s) or text you chose using the transformer's `pipeline` and turn
it in with your text, code, and in 200 words or more write about what you
learned and how you could use this in the future.