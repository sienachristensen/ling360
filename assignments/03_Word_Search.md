# Assignment 3
## Word Search

**Instructions**:

Your task is to define a function in Python that takes as input a short text (200 - 400 words) and returns as output a Python list of specific types of words of your choice. You create a regular expression that matches the words of your choice. If you're having trouble figuring out what words to have your function return, here are some possible ideas: words that start with a vowel and then a consonant (e.g., ant); words that start with two consonants (e.g., cream); words that end in -ed or -en (e.g., walked or taken); words that have two consecutive vowels (e.g., building); words that have two consecutive vowels that are the same (e.g., school or indeed). Be creative and stretch your regular expression writing ability. Once you have the function defined, pass a text of your choice into the function as a single string (i.e., the whole text as one string in Python) and report the words that the function returns. Then, manually check to verify that the function returns the words you were expecting it to return.

Next, write a short report in which you reflect on the process of creating the regular expression to match the words of your choice. Was it difficult? Easy? What did you learn or remember about regular expressions? Etc. Also, paste in the input text and the Python list of words that the function returns. 

**Deliverables**:

Zip together the following two files and submit the zipped file in the CMS by the due date:

1. Your Python script (.py file) that has your function and the input text;
2. Your report with your reflection and the text and returned words.

[Word Search Assignment Preview Video](https://www.youtube.com/watch?v=4pZlywmx-Lk&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=25)

**Note**: The rubric presented in the video above has changed.  See [the current rubric](https://gitlab.com/flash4syth/ling360/-/blob/main/assignments/ASSIGNMENT_RUBRIC.md?ref_type=heads)