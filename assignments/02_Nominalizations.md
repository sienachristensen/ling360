# Assignment 2
## Nominalizations

**Instructions**:

Write a Python program that uses regular expressions and the re.findall() function (which produces a list data structure) to both identify and count all of the nominalizations in a short text. Use [this website](https://www.dailywritingtips.com/nominalized-verbs/) or other resources to familiarize yourself with nominalizations. As input to your program, you can simply paste a short text (300 or so words) into your .py file (or you can do file input, if you know how to). Write a short report in which you give the initial precision and recall accuracy measurements of your program. Then, modify your program (probably just the regular expression) and give the final precision and recall measurements of your program. (If the final precision and recall scores are the same as the initial ones, that's not a problem.) Turn into the CMS a zipped file (with extension .zip) with your .py file as well as your report in a text file.

[Nominalization Assignment Preview Video](https://www.youtube.com/watch?v=rRd-uE4NCOM)

**Note**: The rubric presented in the video above has changed.  See [the current rubric](https://gitlab.com/flash4syth/ling360/-/blob/main/assignments/ASSIGNMENT_RUBRIC.md?ref_type=heads)