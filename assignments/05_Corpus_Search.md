# Assignment 5
## Corpus Search

In the CMS there is a corpus named "Mini-CORE_new.zip" (Corpus of Online Registers of English). It contains 1,600 texts, 200 from each of the following registers:

- HI: How-to/Instructional
- ID: Interactive Discussion
- IN: Informational (Wikipedia)
- IP: Informational Persuasion
- LY: Song Lyrics
- NA: News reports
- OP: Opinion blogs
- SP: Interview transcripts


The filenames are long, but you only need first two-letter code after the "1+", for example, you only need "HI" from the filename: "1+HI+HT+HI-HI-HI-HI+FH-HT-HT-HT+NNNN+0142743.txt". That two-letter code indicates the register.

**Instructions**:

For this assignment, you should select three (3) linguistic features that you think will vary across the registers. Write a Python program that uses regular expressions to find those features within the Mini-CORE corpus. The program should ignore the header in each file. You'll likely have to deal with a character encoding other than "utf-8". Then, the program should calculate normalized rates of occurrence per 1,000 words of each linguistic feature. Next, the program should write the normalized occurrences to a .csv file so that there is a "Register" column which contains the two-letter code for the register (i.e. extract the two letter register code from the filename and write only that to the first column), and a column for each linguistic feature. When imported into spreadsheet software, like Microsoft Excel, your .csv file should have 8 rows (9, if you include the column headers) and look something like the following:

| Register | [Feature 1] | [Feature 2] | [Feature 3] |
|----------|-------------|-------------|-------------|
| HI       | 	15.2 	 |   131.3 	   |   22.4      |
| ID 	   |    14.6 	 |   119.6 	   |   26.9      |
| [6 more registers]  | etc. | etc. | etc. |

Write a brief report in a .docx file that discusses each feature, your hypothesis on how it might vary across registers, and the results and whether your hypothesis for each linguistic feature was confirmed or not. Also, take a small sample of the corpus (a file or two) to measure the precision and recall of your regular expressions and report those accuracy measurements.

Turn into the CMS a .zip file with your Python program as a .py file, your report in prose in a .docx file, and the .csv file that your program produces.

[Corpus Search Assignment Preview Video](https://youtu.be/LqkXdTHGdtA)

**Tips**:
You can choose any linguistic features that you want, however, if you're stuck, here are some features you can consider counting: pronouns (all pronouns or 1st, 2nd, or 3rd), proper nouns, past tense, modals, contractions, punctuation (e.g., ? or !), use of quotation marks, etc.

See [the current rubric](https://gitlab.com/flash4syth/ling360/-/blob/main/assignments/ASSIGNMENT_RUBRIC.md?ref_type=heads)
