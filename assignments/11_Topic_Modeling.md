# Assignment 11
## Topic Modeling

[Assignment Preview Video](https://www.youtube.com/watch?v=JD5wLuI92L8)

## Objective:

Students perform either `topic modeling` or `document similarity` analysis, or both if they choose.

## Instructions:

Download and unzip (expand) the [news_universe.zip](https://gitlab.com/flash4syth/ling360/-/blob/main/python_track/12_week/news_universe.zip?ref_type=heads) file on your computer. After unzipping it, you should have a new directory named "news_universe" with 49 TXT files. Each TXT file has a news article web-scraped from [The Daily Universe](https://universe.byu.edu/).

Then, students choose either Option A or Option B, or both if they'd like.

## Option A

Use the `LDA` (Latent Dirichlet Allocation) algorithm, available in the [gensim Python module](https://radimrehurek.com/gensim/), to discover topics in the news articles. You should spend some time running the algorithm several times, each with a different number of topics, in order to find what looks to be a good number of topics. This can be accomplished by trial-and-error.

Optional: For those wanting to learn more about how to find the natural number of topics, you may consult the paper ["On Finding the Natural Number of Topics with Latent Dirichlet Allocation: Some Observations"](https://link.springer.com/content/pdf/10.1007%2F978-3-642-13657-3_43.pdf) 
and/or section 17 of [this blog post](https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/#17howtofindtheoptimalnumberoftopicsforlda) 

You should use the visualization technique presented in class (and presented in Python code in the solution code for lesson 12.1 on topic modeling to visually inspect the topics.

Write out to one (1) CSV file the top handful of words (probably a number between five and ten words) for each of the topics, one topic per row. When imported into spreadsheet software (like Microsoft Excel), your CSV file should look something like this (your words will obviously be different, as will the number of topics and the number of words per topic):

|   |          |       |       |      |
|---|----------|-------|-------|------|
| 1 | catcher | ball | field | etc. |
| 2	| president	| China	| negotiations | etc. |
| 3	| family | consensus | leave | etc. |
| etc. | etc. | etc. | etc.	| etc. |

Then, directly in the spreadsheet software of your choice, add a new column to the left of the column with the topic numbers and type in labels that you feel adequately reflect the commonality that the words in each row share, and save the CSV (or Excel) file with your human-given labels. (Students who prefer to have the topics organized vertically instead of horizontally can do so if they like.)

## Option B

Students use [doc2vec](https://radimrehurek.com/gensim/auto_examples/tutorials/run_doc2vec_lee.html) in `gensim` to perform document similarity analysis on the same `Daily Universe` mini-corpus [news_universe.zip](https://gitlab.com/flash4syth/ling360/-/blob/main/python_track/12_week/news_universe.zip?ref_type=heads) mentioned above. The students find texts that doc2vec returns as being similar to each other and texts that are different from each other. Then, the students manually inspect the texts to impressionistically confirm or disconfirm the similarity or dissimilarity of the texts that doc2vec identified. Then, the students manually list 5-10 words or ideas that the similar texts share, and then speculate on why the dissimilar texts were identified as dissimilar.

## Deliverables:

If topic modeling (option A): Turn into the CMS a zipped file (with extension .zip) that has your .py script, your .csv (or .xlsx) file, and a .docx or text file with 50-100 words in which you reflect on (1) the process of finding a good number of topics for this assignment and (2) possible applications of topic modeling.

If document similarity (option B): Turn into the CMS a zipped file (with extension .zip) that has your .py script, and a .docx or text file with 50-100 words in which you reflect on (1) the process of getting doc2vec to work for the Daily Universe mini-corpus and (2) the 5-10 words or ideas that the similar texts share and your speculation on why the dissimilar texts were identified as such.