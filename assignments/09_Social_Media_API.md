# Assignment 9
## Social Media API

**Objective**:

*Option 1*: Sign up for a [Kaggle](https://www.kaggle.com/) account which is a website with many datasets, including from Social media sites. Then find a social media data set ([Kaggle datasets search](https://www.kaggle.com/datasets)) on which you can perform sentiment analysis on multiple posts or comments.  Include the link to the dataset you choose in a comment in your code.

*Option 2*: (more challenging) Harvest human-generated language through an API to the social media platform of your choice and run sentiment analysis on the posts or comments.

**Instructions**:

Choose an option above to access social media data.

For *option 1*: simply download a dataset that seems interesting to you which contains social medial text data (posts or comments) from Kaggle.

For *option 2*: use Python to access that social media's API to harvest human-generated comments or posts. First, write the comments or posts to a file or multiple files (whichever makes sense for the social media that you chose). You can choose to save that file or those files in whichever format you feel is best organized and easiest to access later in this assignment. For example, you might write Youtube comments to JSON files (.json), one file per videos. Or you might write Reddit posts to CSV files (.csv), one file per subreddit.

Next, (use a different script if you chose option 2) write a Python program to parse the previously saved files with the human-generated text and perform sentiment analysis on the human-generated language using [vaderSentiment](https://github.com/cjhutto/vaderSentiment).  Simply install `vaderSentiment` from PyCharm's Python packages installation tool. This second script should extract the five most frequent words in the comments or posts which have a positive sentiment score (greater than 0.5 positive score or a higher threshold).  Exclude function words from the frequent words (aka stop words).  You can use [the stop-words Python module](https://pypi.org/project/stop-words/) or [the NLTK stop words](http://www.nltk.org/book/ch02.html#stopwords_index_term) to filter out stop words. The five most frequent words in positive comments in each file should be printed to the screen (i.e., the console).

Write a report in a separate text file in which you interpret the results. Which five frequent words occur in positive comments? Why do you think that his the case? What surprises or errors do you see?




## Deliverables:

Zip together the following files and submit the zipped file:

Your .py file that harvests comments or posts through the API of the social media of your choice
The files that were created by that first harvesting script (e.g., the .json files or the .csv files, etc.)
Your .py file that parses through the saved files and runs sentiment analysis.
Your report in a .docx file or .txt file.