# Assignment 12
## Yelp Reviews

[Assignment Preview Video](https://www.youtube.com/watch?v=1y4Q0LrLHXM)

## Instructions:

For this assignment you need the "yelp_AZ_2021.json" file in the CMS. Note: the file is big, at about 18 MB. This dataset contains 27,334 reviews written in 2021 about businesses within the categories of "Restaurants" and "Food" in Arizona. These reviews were taken from a larger dataset of ~7 million reviews (in a file of 4.4 GB!) made available by Yelp for a recent dataset challenge.

Identify two linguistic features that you believe are correlated with the number of stars a business receives. Be creative and use linguistic features you haven’t yet looked at this semester. Your program should create a .csv file with three columns and 27,334 rows (one row for each review in the sample dataset): Column A = stars, Column B = N_feature1, Column C = N_feature2. On a given row in the .csv file, the stars column should give the number of stars the current review has, the N_feature1 column should give the number of times the first linguistic feature occurs in the current review, and the same should be the case for the N_feature2 column. When imported into a spreadsheet, the .csv file should look something like the following:


| stars | N_feature1 | N_feature2 |
|-------|------------|------------|
| 3	| 0	| 1 |
| 2	| 2	| 0 |
| 5	| 0	| 0 |
| etc. | etc. | etc. |

Next, your program (or a different .py file, if you'd like) should do the following: (1) read back into Python your .csv as a pandas DataFrame data structure and (2) run a Pearson correlation test and (3) create two scatter plots with a regression line, one for each of the linguistic features examined.

Write a report in which you interpret the results. Which feature, if any, correlates with number of stars? In which direction (positive or negative correlation)? Why do you think this happens? Do the results prove or disprove what you thought would be the case (your hypothesis)? Also, take a sample of the dataset (20 or so reviews) and measure precision and recall of the regular expressions you used to find the linguistic features. Report those measurements for each linguistic feature.

Turn into the CMS a .zip file with your Python program in a .py file (or two .py files, if you choose to write two), the .csv file your program creates, and your report in a .docx or text file.