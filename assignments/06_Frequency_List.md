# Assignment 6
## Frequency List

**Instructions**:
Write a program that creates 8 separate frequency lists of words, one for each of the 8 registers in the Mini-CORE corpus, ordered in descending order of frequency, that is, with the highest frequency first. Each frequency list (one per register) should be written to a separate .csv file. The program should exclude stopwords from your frequency lists. Use either the [stop-words Python module](https://pypi.org/project/stop-words/)
  or the stopword list in the [NLTK library](http://www.nltk.org/book/ch02.html#stopwords_index_term) to exclude stopwords.

Links to an external site.

In a .docx file, write 100-200 words describing the differences you see between the frequency lists and what you can learn about these registers based on those differences.

Turn into the CMS a .zip file with your Python program in a .py file, your report in a .docx file, and the 8 .csv files with the frequency lists.

Tips:

    One way to exclude the header of each file is to read in the whole file as a string with the .read() method (on the file connection variable) or .read_text() on a Path object, and then use re.sub() to delete the characters between opening and closing angled brackets (with the regex r"<.*?>").
    One way to exclude stopwords is to use a list comprehension, for example, something like this: wds_to_keep = [wd for wd in wds if wd not in stopword_list] (see SO thread here 

Links to an external site.)
String concatenate the output file name to modify for each register within a for loop, for example, something like this: with open('word_freq_' + current_register + '.csv', mode = 'w') as outfile: