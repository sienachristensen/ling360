# Assignment Rubric

## Task completion

**4 pts** <br>
**Completed**

The script accomplishes the assigned task without erring out and without the human rater changing anything in the script (other than, if needed, pathways to directories or files). If asked for in the instructions, the interpretation of the results by the student is sound. If asked for in the instructions, any additional files are present in the submission.

**3 pts** <br>
**Nearly Completed** 

The script accomplishes the assigned task only after the human rater fixed an error or two (after the rater changed pathways on his/her specific computer, if needed). Additional materials, if asked for in the instructions, may or may not be present.

**2 pts** <br>
**Attempt made**

The student made an attempt to accomplish the assigned task, but the script does not accomplish it and/or it errs out, even after the human rater spends several minutes fixing errors in an attempt to get it to run (after the rater changed pathways on his/her specific computer, if needed). Any additional materials, if asked for in the instructions, are lacking.

## Conciseness of code

**3 pts** <br>
**Reasonably concise**

While not necessarily the most concise way to accomplish the task, the script is reasonably concise and the student applied the principle of DRY, that is, "don't repeat yourself".

**2 pts** <br>
**Somewhat verbose**

The code could have been more concise in some parts.


**0 pts** <br>
**Verbose**

The script takes an inordinately long path in order to accomplish the task; the script could have been substantially more concise.


## Comments for human readers

**3 pts** <br>
**Reasonably concise**

- The script has comments. 
- They do not repeat the code, but inform what is not obvious from the code. 
- For Python use Docstrings on all functions and methods (ex. """This is a docstring"""). 
- For Elixir use @doc for all functions (ex. @doc """Here is an at-doc""")

**2 pts** <br>
**Somewhat verbose**

The script has comments, but they could be improved.

**0 pts**
**Verbose**

If there are comments they are unhelpful or only repeat the code. Human readers are forced to analyze the code in order to understand the script.

---

__10 Points Possible__
