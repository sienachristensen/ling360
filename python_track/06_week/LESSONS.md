# Lesson 6.1: Non-UTF-8 encoding

**Objective**:
Students will be capable of reading in files with character encodings other than UTF-8.

## File encoding standards

- ASCII (American Standard Code for Information Interchange)
    - dates from the 1960s
    - English-centric
    - https://en.wikipedia.org/wiki/ASCII

- Unicode
    - dates from the late 1980s
    - intended to be an "international/multilingual text character encoding system"
    - the "name 'Unicode' is intended to suggest a unique, unified, universal encoding."
    - https://en.wikipedia.org/wiki/Unicode
  

- This [Python Conference talk](https://youtu.be/sgHbC6udIqc) is informative, especially the first 9 minutes. 

- Several main character encodings:
    - "UTF-8" (also written as "utf-8" or "utf8"); the most popular today (since 2008)
    - "UTF-16" ("utf16")
    - "Windows-1252" (also "ISO-8859-1" and "Latin-1" or "latin1")
    - Many, many more: https://docs.python.org/3/library/codecs.html#standard-encodings
  
The `chardet` module has the `detect()` function that makes a guess about the character encoding of a file. It takes as input bytes, so you have to call something like `bytes = path.read_bytes()`, to read in the file (with the `.read_bytes()` method of the Path object) and then pass the read data to `chardet.detect()`. Note: The module also comes with a command-line tool `chardetect pathway/to/filename` that can be used in a Terminal window (PyCharm > View > Tool Windows > Terminal) to (try to) figure out the encoding of a file.

Download the `sample_texts.zip` file from the CMS and use the following code  to detect the encoding of the texts inside the zip file (unzip it to see them).  **Note**: you will need to make sure the paths are correct for your computer.


```
# Using the chardet.detect() function with the chardet module, take the encoding that the tool returns and try to open the file with the read_text(encoding="<detected encoding>") function, replacing <detected encoding> with the encoding that chardet.detect() guessed.

# Aftering installing the chardet module...
import chardet
from pathlib import Path

start_path = Path("/Users/your_name/Documents/LING_360/sample_texts/")
file_path = start_path / "File_of_your_choice.txt"

# Now use the `file_path` Path object to
# read the bytes (sequences of 8 0's and/or 1's)
whole_file_as_bytes = file_path.read_bytes()

# returns information about what chardet.detect() thinks
# the encoding is and its certainty level from 0 to 1    
guessed_encoding = chardet.detect(whole_file_as_bytes)
print(guessed_encoding)


# Attempt 1: taking the encoding that detect() returned
arabic_file = Path("/Users/ekb5/Documents/LING_360/sample_texts/Arabic.txt", encoding="MacCyrillic")
text = arabic_file.read_text(encoding="MacCyrillic")

# the output doesn't look like Arabic
print(text)

# Attempt 2: try other encodings listed for the language on this doc page: https://docs.python.org/3/library/codecs.html
# After several attempts, the encoding "iso8859_6" seems to work best.
text = arabic_file.read_text(encoding="iso8859_6")
print(text)

# Here are some good encodings for the other files:
# Chinese.txt  "GB2312"
# Danish.txt  "ISO-8859-1"
# Russian.txt  "KOI8-R"
# Spanish.txt  "ISO-8859-1"
# Turkish.txt  "iso8859_9"
# Vietnamese.txt  "windows-1258"


### BONUS, using the command-line tool that comes with the chardet module ###

# Using the chardetect command-line tool in a Terminal window, take the encoding that the tool returns and try to open the file with the codecs.open() function (remember to import codecs)

# Within a Terminal window (not in Python REPL mode):
# chardetect /Users/ekb5/Documents/LING_360/sample_texts/Arabic.txt

# returns:
# /Users/ekb5/Documents/LING_360/sample_texts/Arabic.txt: MacCyrillic with confidence 0.5438114253826244


text_path = Path("/Users/ekb5/Documents/LING_360/sample_texts/Vietnamese.txt")
text = text_path.read_text(encoding="iso8859_6")
print(text)
```

## Practice:

1. Download the zipped file sample_texts.zip from the CMS. It contains 7 .txt files with text in different languages, with different character encodings.
2. Install the chardet module if your computer doesn't have it installed

You can install it with pip install or within PyCharm, or you can use pythonanywhere.com. (If you use pythonanywhere.com, you'll need to upload the files and refer to pathways relative to your home directory in pythonanywhere, for example, /home/ekbrown77/Arabic.txt.)

3. Using the `detect()` function with the chardet module, take the encoding that the tool returns and try to open the file with the open() function, specifying an encoding argument.

4. If the file still doesn't open, or if the characters don't look like they came in correctly, try a different encoding listed for the language on this [Python documentation page](https://docs.python.org/3/library/codecs.html#standard-encodings)

# Lesson 6.2: Word documents

**Objective**:
Students will be capable of extracting text from Microsoft Word documents.

Word documents (with extension .docx)

## Practice:

1. download python-docx (in PyCharm's Preferences window, or pip install)
2. import docx
3. Create object with the function Document()
4. Loop over the .paragraphs attribute of the object (mind the lack of parentheses!)
5. Extract the text of the current paragraph with the .text attribute (again, no parentheses)

**Note**: Legacy Word documents with the extension of .doc need to be dealt with in a different way. Check out this Stack OverFlow thread: https://stackoverflow.com/questions/36001482/read-doc-file-with-python

## More Practice:

1. Download and unzip the zipped file Salinas.zip in the CMS.
2. Write a program to print to screen the first file, named Salinas_01_ekb.docx.
3. Modify the program to print to screen all 11 files.
4. Modify the program to not print out lines that begin with "#" (the header and time stamps) and lines that begin with "/" (the questions and comments of the interviewer).
    - _Hint_: The caret symbol ^ in regexes identifies the beginning of a string.
5. Modify the program to create a concordance of words that end in "s" or "z" in all files. Write out to a .csv file the following info, each in a new column (separated by tabs): filename, paragraph number, preceding ten words, match, following ten words. When imported into a spreadsheet, it should look something like this:

# Lesson 6.3: Indexed PDF files

**Objective**:
Students will be capable of extracting text from indexed PDF files.

Not all PDF files are created equal:

- Indexed -> The text is in the metadata of the file and can be extracted.
- Unindexed -> Just a picture; no text available.
 
**Note**: For an unindexed PDF file you would have to perform Optical Character Recognition (OCR), which is not the objective of this lesson. If you need OCR, Google Tesseract OCR and the Python wrapper pytesseract are a good option.

Logic with indexed PDF files:

- Download Xpdf command-line tools (*not XpdfReader), which has the very useful and easy-to-use pdftotext command-line tool, and unzip the zipped file.
- Control the pdftotext command-line tool from within a Python script with the run() function within the subprocess module to produce a TXT file from each PDF file.
- Pass in the syntax of a command-line (aka. shell) call into the subprocess.run() function.
- After creating TXT files from all the PDF files, loop over the TXT files and perform the desired analysis.
 
## Practice:

1. Download the zipped file Jane_Austen.zip from the CMS and unzip it.
2. Write a program to create a TXT file named Emma.txt from Emma.pdf.
 
_Hint 1_: See the documentation for pdftotext here.

_Hint 2_: You need to import subprocess before calling `subprocess.run()`

3. Either in the same script or in a new script, read in the newly created Emma.txt file and print it to the console.
4. Modify the previous program and do some post-processing as you read in Emma.txt so that the Project Gutenberg header and footer are not printed.

_Hint_: Headers usually end with something like "Start of this Project Gutenberg" and footers usually begin with something "End of the Project Gutenberg".

**Note**: For a single PDF file, you could simply determine on which pages the novel starts and ends, and have pdftotext read in only those pages by setting the -f (first page) and -l (last page) flags (see documentation here).

5. Modify the previous program to create a new TXT file named Emma_novel.txt that only has the novel without the Project Gutenberg header and footer.

6. Modify the previous programs to create corresponding novel-only TXT files for all PDF files in the directory.
