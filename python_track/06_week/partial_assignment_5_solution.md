# A partial solution to Assignment 5 (Corpus Search)
```
from pathlib import Path

corpus = Path("Mini-CORE_new")

feature1_count = 0
feature2_count = 0
feature3_count = 0
results = "Register,Feature1,Feature2,Feature3\n"
counter = 0

# Loop over the sequence of files inside the corpus directory
for file in corpus.iterdir():

    # Temporarily limit our files to
    # only 6 (i.e. 0-5 inclusive)
    if counter > 5:
        # Stops the loop
        break

    register = file.name[2:4]
    results = f"{results}{register},{feature1_count},{feature2_count},{feature3_count}\n"

    counter = counter + 1


output = Path("output.csv")
output.write_text(results)

# Another option for the above for loop is to use `.glob()`
# available on the Path variable/object.  This also creates
# a sequence of files (and/or directories) inside the directory
# that you call glob on, but allows you to filter them like this:
# for file in corpus.glob("*.txt"): 
#
# That would start a for loop over only files ending in '.txt'
# you could think of it as like a 'glob' of those files.
```