# Word Count

```
# Demonstration of getting the word count for all the words
# In a single register.  Remember to clean the headers of each
# file first (not demonstrated here) so you do not count those as words
# Also I show how to normalize the count of a feature across the register.
# which are the numbers you want to write to your CSV file for this week's
# assignment.

from pathlib import Path

# Fix this to be the path to your copy of Mini-Core_new
corpus = Path("Mini-CORE_new")

# initialize our word count
word_count = 0
# initialize feature count
feature_1_count = 0

# Loop over only files matching the glob pattern "1+HI*"
# where `*` means anything else after
for file in corpus.glob("1+HI*"):
    # These files are using "cp437" as the encoding
    # so we are specifying that here
    text = file.read_text(encoding="cp437")
    text_as_word_list = text.split()
    word_count += len(text)
    # Equivalent to above
    # word_count = word_count + len(text)

    # If you uncomment this `break` the for loop will stop.
    # It can be a nice way to test on just the first file.
    # break

    # Using a list comprehension to create a list of 'do', then getting
    # the length of that list
    feature_1_count += len([ word for word in text_as_word_list if word == "do"])

# Using an f-string which allows you to place Python expressions
# inside the string by placing them between {} pairs.
print(f"""
Word Count: {word_count}
Normalized Feature 1 Count: {(feature_1_count / word_count) * 1000}
""") # ending ) of print
```