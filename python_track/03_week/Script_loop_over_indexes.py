fruits = ["apple", "banana", "orange", "mango", "kiwi"]

# Option 1: loop over elements
# Note: The variable "fruit" is a placeholder for each successive fruit in the list "fruits"
print("---Option 1---")
for fruit in fruits:
    print(fruit)

# Option 2: loop over indexes
# Note: The variable "index" is a placeholder for each successive index (number) in the list "fruits"
print("---Option 2---")
for index in range(len(fruits)):
    print(index)  # here's the current index
    print(fruits[index])  # index into the list of fruits
