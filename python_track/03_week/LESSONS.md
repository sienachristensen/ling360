# Lesson 3.1: Regexes

[see Video Lesson](https://www.youtube.com/watch?v=7iCsdmevo_c&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=11)

**Objective**: <br> 
Students will be capable of using regular expressions (regexes) to match sequences of characters within text.

Regular expressions (or "regexes")
A domain-specific language (or mini language) to concisely match patterns of characters within strings. 

## Basic Regular Expressions

**Special symbols**

| Regex Function | Function  |
|----------------|-----------|
| \              | toggles special characters (ex. `\n` is for a new line) |
| \|             | or (ex. `this \| that` matches 'this' or 'that' |
| ()             | creates a capture group and keeps the part inside the parens together |
| [def]          | matches one of: d, e, or f |
| [^abc]         | matches any character EXCEPT: a, b, or c |

**Characters**

| Regex Function | Function                                        |
|----------------|-------------------------------------------------|
| . | match any single character                      |
| \d | match a digit                                   |
| \D | match a non-digit                               |
| \s | match whitespace                                |
| \S | match non-whitespace                            |
| \w | match alphanumeric and `_` (e.g. 0-9; a-z; A-Z) |
| \W | non-alphanumeric                                |

**Quantifiers**

| Regex Function | Function                 |
|----------------|--------------------------|
| * | 0 or more times |
| + | 1 or more times |
| ? | 0 or 1 time |
| {m} | exactly m occurrences |
| {m,n} | from m to n occurrences |
| {m,} | at least m occurrences |
| {,n} | no more than n occurrences |

**Delimiters**

| Regex Function | Function                 |
|----------------|--------------------------|
| \b | word boundary (between \w and \W) |
| \B | not a word boundary |
| ^ | matches beginning of a string |
| $ | matches the end of a string |

_Also see_: "Regular expression cheatsheet" at [pythex.org](https://pythex.org/).


Some useful regex functions:

- `re.search()` returns a Boolean value indicating whether a match was found as well as a match object with various methods
- `re.findall()` returns a list with matches
- `re.finditer()` returns an iterator that must be traversed in order to pull info with methods
- `re.sub()` returns a string after making substitutions

The most useful flag to regex functions:

- `re.IGNORECASE`, which has an abbreviation of `re.I`

## Practice:

Write `import re` at the top of your script and use the `re.search()` function to write a regex that matches the following, and prints the matches to the user (you). Use [pythex.org](https://pythex.org/) if you'd like to test regexes (but make sure to run each regex in Python):

- a word with "ed" at the end
- a word with "anti" at the beginning
- both the American and British spellings of "labor" or "labour"
- 'Jack', 'Mack', 'Pack'
- both the American and British spellings of "center/centre"
- 'majestic' as an eight-letter word with 'j' as the third letter and 't' as the sixth letter
- words other than "best" that end in "est"
- a more concise regex than "(make|makes|made)"

_Hint_: Be aware that using capture parentheses in `re.findall()` in your regex pattern has special behavior.

Words that begin with "s" regardless of case, that is, "S" or "s", in the following sentence: "she Sells seashells by the Sea shore."

_Hint 1_: Look up the `re.IGNORECASE` parameter of the `re.findall()` function.

_Hint 2_: `re.IGNORECASE` has an abbreviation of "re.I".

Write a program that asks the user for a couple sentences about him/herself, and you write a regex to match the word "I".

**Formative quiz**: <br>
Identify which word or words, if any, are matched by the following regexes, in the following string:

"""
Linguistics is an exciting and thriving field, especially computational linguistics, which broadly speaking, deals with using computers to analyze language. Some of the best paying jobs for linguists are in this field.
"""


- `r"\w+s\b"`
- `r"\w+[dt]\b"`
- `r"\w*[iu][aei]\w*"`
- `r"\w+(\w)\W+\w+\1\b"`
- `r"\b[^aeiou\W][aeiou]\w+"`
- `r"\b[aeiou][^aeiou]\w+"`

# Lesson 3.2: Measuring accuracy

[see Video Lesson](https://www.youtube.com/watch?v=Ml0jTgT1C1U)

**Objective**:

Students will be capable of measuring the accuracy of regex searches.

### Accuracy

- Precision
- Recall

Look at "precision_recall.pptx" in the CMS


### Practice:

Calculate the precision and recall of the first regex in the formative quiz in the previous lesson for finding word-final /s/ that represents the third-person singular "s" of present tense verbs in English in the short text given in that quiz.

Calculate the precision and recall of that same regex for finding word-final /s/ that represents a plural morpheme.

## Lesson 3.3: Loops

[see Video Lesson](https://www.youtube.com/watch?v=eSF9nFqZINo&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=23)

**Objective**: <br>
Students will be capable of using the for loop and the while loop to traverse (or iterate) over strings and collections, like lists.

### Loops

`while` loop (in Think Python, Chapter 7: Iteration)
A block of code is executed repeatedly as long as a conditional statement evaluates to True at the beginning of each iteration.

**Note**: <br>
Be careful not to create an infinite loop!

**Practice**: 

#### Using a while loop:

1. write a program that asks the user for a number between 1 and 10 and prints out from 1 to the number the user provided.

2. write a program that asks the user to write a sentence and then asks the user to give a single letter (with another call to `input()`), and prints out the sentence, one letter on a line, until the letter given by the user is encountered, at which point the loop is exited and no more of the sentence is printed. If the letter given by the user doesn't occur in the sentence, the whole sentence should print out.

_Hint_: Look up the break keyword.

_Tip_: The video shows another way to increase an integer by 1:
```commandline
# Both are the same
x = x + 1
x += 1
```
### for loop (in ch. 8 of textbook)

A block of code is executed for each element of a string or collection (such as a list).

**Examples**:

print every letter in a string; see [Think Python, Chapter 8](https://greenteapress.com/thinkpython2/html/thinkpython2009.html), Section 8.3 _Traversal with a for loop_

**Practice**:

Write a program that asks the user for a word with at least eight letters, then prints each letter/character on a new line.

Modify the previous program so that only consonants are printed.

_Hint_: Look up the in operator and the continue keyword.

Modify the previous program so that both consonants and vowels are printed, but with vowels in uppercase.

_Hint_: Don't forget about the string method .upper() which can be used on single characters.

Modify the previous program so that instead of printing on new lines, each letter is printed next to each other (as words are normally printed, lIkE thIs).

_Hint_: Look up the end parameter of the print() function.

Formative quiz about indentation in Python: Determine how many times "¡Hola Mundo!" will print to the screen in each of the following blocks of code, and discuss why with a neighbor!

```
# Block 1
for i in range(2):
    pass
    for j in range(2):
        pass
        print("¡Hola Mundo!")

# Block 2
for i in range(2):
    pass
    for j in range(2):
        pass
    print("¡Hola Mundo!")

# Block 3
for i in range(2):
    pass
    for j in range(2):
        pass
print("¡Hola Mundo!")
```

# Lesson 3.4: Lists

**Objective**:

Students will be capable of using the list data structure, a collection, to work with many values.

[Think Python, chapter 10: Lists](https://greenteapress.com/thinkpython2/html/thinkpython2011.html)

A collection can have values of several different types:

- `[1,  2,  3]`
- `['wolf', 'fox', 'coyote']`
or even `[1, 'wolf', 8.5, 'Hey there!']`
or even `['spam', 2.1, 5, [10, 'five']]` (a nested list, that is, a list inside another list)

**Useful list methods**:

`list_name.append('a')` adds `'a'` to end of `list_name`

_Hint_: `list_name += ['a']` is a shorthand for
`list_name.extend(another_list)`. Both are ways to combine two lists.

`list_name.sort()`  modifies in place!

`sorted(list_name)`  creates a copy!

Useful string methods that create lists:

`'How are you today?'.split()` returns ['How', 'are', 'you', 'today?']

Split with regex: 

`re.split(r"[aeiou]", "How are you today?")` 

returns `['H', 'w ', 'r', ' y', '', ' t', 'd', 'y?']`

Useful string method that takes a list as input:
`' '.join(['How', 'are', 'you', 'today?'])` returns 'How are you today?' mind the space between single quotes!

**Practice**:

1. Write a program that asks the user for a sentence or two about him/herself, and simply prints back to the user each word on a new line.

2. Modify the previous program so that the words printed back to the user are in alphabetical order.

_Hint_: You'll have to force case (either to lowercase or to uppercase), as, for example, "H" and "h" are considered to be different letters by the Python interpreter.

3. Modify the original program so that every other word is uppercase.

_Hint1_: You'll need to create a counter to keep track of the number of iterations.

_Hint2_: You'll need to use the modulus operator to determine whether the current iteration is an odd-numbered one or an even-numbered one, for example, `counter % 2 == 0`.

4. Modify the original program so that the vowels are replaced by dashes.

**Bonus**:

Solve this problem in both of the following two ways:

1. with a nested for loop to traverse over the letters of each word, and
2. with the re.sub function from the re module.

