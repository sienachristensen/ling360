# Lesson 8.1: Basic descriptive statistics
[see video lesson here](https://www.youtube.com/watch?v=jVoaGgD22NQ&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=50)

**Objective:**
Students will be capable of producing basic descriptive statistics with the statistics module.

statistics module
Functions to measure central tendency:
`mean()`
`median()`
`mode()`

Functions to measure dispersion or spread (around the central tendency)
```
stdev()  # standard deviation
variance()  # this is the square of the standard deviation
range
```

## Practice:
1. Download (or reuse) a `.txt` file of a novel of your choice from the Gutenberg Project.
2. Calculate the measures of central tendency and the measures of dispersion for sentence length. 

Hint1: It might be easiest to read in the whole file as a string with the `.read()` method of the file handle, and then `re.split()` the string into sentences on punctuation that indicates the end of sentences.

Hint2: Another way to split up a string into sentences is with the `sent_tokenize()` function in the nltk library.
3. Download (or reuse) a `.txt` file of a novel from a different author and calculate the same measurements.
4. Compare the two authors' sentence lengths, looking at measures of central tendency, as well as how varied their sentence lengths are, with measures of dispersion.

# Lesson 8.2: Part-of-speech tagging with NLTK library
[see video lesson here](https://www.youtube.com/watch?v=Ywe1gNUB2T4&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=40)

**Objective:**
Students will tag words for part-of-speech in a text with the NLTK (Natural Language Toolkit) library.

Part-of-speech tagging
Logic/Steps:
1. Download nltk library (in PyCharm: Preferences > Project: [project name] > Project Interpreter > the "+" > search for "nltk" > install packages
2. In a .py file, run the code given here and download "book" ("Everything used in the NLTK Book")
`import nltk`
3. Use the function `word_tokenize(str)` to tokenize text, that is, put words and punctuation in separate elements of a list.
4. Use the function `pos_tag(list)` to tag part-of-speech (POS) of the tokens (the individual words and punctuation). The returned value is a list with 2-element tuples, the first element of which is the token, and the second element is the POS tag.
5. In a REPL (not in your `.py` script), you can use the function `nltk.help.upenn_tagset()` to see what the tags of the Penn Treebank tagset mean, for example, that "NNS" is the code for a plural noun. Or, you can find the tagset online with a simple internet search of keywords like "penn treebank tagset".
The function `pos_tag()` takes the argument `tagset="universal"`, which gives simplified tags.
See examples in chapter 5 of the NLTK book.

Beware! Unfortunately, some example code in the NLTK book isn't completely reproducible, that is, there are some errors.

## Practice:
1. Write a sentence or two about the topic of your choice. Try to include a word or two that has different parts of speech depending on the context, for example, "The judge must record the new world record" (verb / noun) or "Yesterday I talked with her, but I haven't talked with her yet today" (simple past / present perfect).
2. Tag the part-of-speech of the sentence(s) and look up the tags in the Penn Treebank tagset. Determine how many of the words were tagged correctly.
3. Find a paragraph or two online and copy and paste it/them as the input text in your program. Rerun your program and skim the tags and look for any incorrect tags.
4. Modify your previous program to use the "universal" tagset. Next, calculate how many of each tag the paragraph(s) has.
5. Modify your program to tag a text of your choice (perhaps a novel from Gutenberg, or a speech from a political leader). Find all comparative adjectives in the document and (if there aren't too many) report the precision of the tagger with comparative adjectives.

# Lesson 8.3: Part-of-speech tagging with TreeTagger
[see video lesson here](https://www.youtube.com/watch?v=8Jc262cEF24&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=1)

**Objective:**
Students will tag for part-of-speech with the third-party software TreeTagger, using a Python wrapper.

TreeTagger part-of-speech tagger
An oldie but a goodie POS tagger that tags many different languages, freely available here.
There are several Python wrappers, for example, the one linked from the TreeTagger website called treetaggerwrapper. That wrapper has a limited number of languages it supports here.

Logic/Example code:
```
import treetaggerwrapper
tagger = treetaggerwrapper.TreeTagger(
TAGDIR='[pathway/to/treetagger/dir]', TAGLANG='[two-letter_language_code]')
tags = tagger.tag_text('[text_here]')
pretty_tags = treetaggerwrapper.make_tags(tags)
```

## Practice:
1. Tag for part-of-speech 1 Nephi 1:1 in the Book of Mormon in several different languages, from among the languages with parameter files on your hard drive.

Hint: In order to tag text in a language, you must have had the parameter file in the TreeTagger directory on your computer when you installed the software, that is, when you ran the `bash command sh install-tagger.sh` from a Terminal window. If that wasn't the case, you can download the parameter file for the desired language and rerun the installer script, that is, the bash command given above.

# Lesson 8.4: Part-of-speech tagging with Stanza
[see video lesson here](https://www.youtube.com/watch?v=r8zhMBvIUnE&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=16)

**Objective:**
Students will tag words for part-of-speech with Stanza.

Stanza is a language analysis software library produced at Stanford University.
In addition to part-of-speech tagging, Stanza can return the lemma (see here) of a word (and other information; see here).

Steps to part-of-speech tag words with Stanza:
1. Install Stanza (e.g., in Preferences windows in PyCharm or `pip install stanza`)
2. Download the models for languages you want to perform part-of-speech tagging on.
3. Create a Pipeline object.
4. Use the Pipeline object to annotate text.
5. Pull out the information you want about the words, e.g., part-of-speech and lemma.
Note: The gnarly double list comprehension in the sample code can be expanded out to a more transparent double for loop.

## Practice:
1. Download Stanza in PyCharm's Preferences window (or with `pip install stanza`). 
2. Download the language models for the languages that you'd like to perform part-of-speech tagging on. 
3. Write a toy language sample (i.e., a sentence or two) and print out to the console each token and its corresponding part-of-speech (both `upos` and `xpos` tags, if available) and the lemma.
4. Ramp it up to a file with lots of text (e.g., a novel).
5. Use the parts-of-speech to pull out a linguistic construction of your choice.
6. If you can't think of anything, try to pull out the way construction (e.g., she worked her way through college, he cheated his way through high school).
7. Save the tokens of the linguistic construction out to a file on your hard drive.

