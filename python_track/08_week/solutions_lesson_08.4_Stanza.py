"""
Possible solutions to in-class practice exercises for
Lesson 8.4
about part-of-speech tagging with Stanza
"""

# Download Stanza in PyCharm's Preferences window (or with pip install stanza).
# The instructions say it all.

# Download the language models for the languages that you'd like to perform part-of-speech tagging on.
import stanza
stanza.download("en")
stanza.download("es")

# Write a toy language sample (i.e., a sentence or two) and print out to the console each token and its corresponding part-of-speech (both upos and xpos, if available) and the lemma.
import stanza
txt = "I am Earl Brown and I love teaching students how to analyze language with computers."
nlp = stanza.Pipeline("en")
doc = nlp(txt)

### Option 1: double list comprehension
print(*[f'{word.text}\t{word.upos}\t{word.xpos}\t{word.lemma}' for sent in doc.sentences for word in sent.words], sep='\n')

### Option 2: double for loop
# for sent in doc.sentences:
#     for word in sent.words:
#         print(f'{word.text}\t{word.upos}\t{word.xpos}\t{word.lemma}')


# Ramp it up to a file with lots of text (e.g., a novel).
import stanza
from pathlib import Path

pride_and_prejudice_path = Path("./Pride_and_Prejudice.txt")
txt = pride_and_prejudice_path.read_text(encoding="utf-8")
nlp = stanza.Pipeline("en", processors='tokenize,pos,lemma')  # specify only the ones you need to speed up processing
doc = nlp(txt)

### Option 1: double list comprehension
print(*[f'{word.text}\t{word.upos}\t{word.xpos}\t{word.lemma}' for sent in doc.sentences for word in sent.words], sep='\n')

### Option 2: double for loop
# for sent in doc.sentences:
#     for word in sent.words:
#         print(f'{word.text}\t{word.upos}\t{word.xpos}\t{word.lemma}')


# Use the parts-of-speech to pull out a linguistic construction of your choice.
# If you can't think of anything, try to pull out the way construction (e.g., she worked her way through college, he cheated his way through high school).
import stanza, re

# get text from file as string
pride_and_prejudice_path = Path("./Pride_and_Prejudice.txt")
txt = pride_and_prejudice_path.read_text(encoding="utf-8")

# analyze the text
nlp = stanza.Pipeline("en", processors='tokenize,pos,lemma')  # specify only the processors we need in order to speed up processing time
doc = nlp(txt)

# loop over sentences
for sent in doc.sentences:

    # loop over indexes of words in current sentence in order to look at words to the right of the current word
    for i in range(len(sent.words)):

        # use a try-except block to deals with verbs close to the end of sentences
        try:

            # grab current word and three words to the right
            wd0 = sent.words[i]
            wd1 = sent.words[i+1]
            wd2 = sent.words[i+2]
            wd3 = sent.words[i+3]

            # using both POS tags and words, test whether there is a "way" construction
            if re.search(r"^V", wd0.xpos) and re.search(r"PRP\$|WP\$", wd1.xpos) and re.search(r"\bway\b", wd2.text, flags=re.IGNORECASE) and re.search(r"IN", wd3.xpos):
                print(f"{wd0.text} {wd1.text} {wd2.text} {wd3.text}")

        except IndexError:
            continue

# Save the tokens of the linguistic construction out to a file on your hard drive.
import stanza, re

# get text from file as string
pride_and_prejudice_path = Path("./Pride_and_Prejudice.txt")
txt = pride_and_prejudice_path.read_text(encoding="utf-8")

# analyze the text
nlp = stanza.Pipeline("en", processors='tokenize,pos,lemma')  # specify only the processors we need in order to speed up processing time
doc = nlp(txt)

# open connection to outfile
outfile_path = Path("results.txt")
with outfile.open(mode="w", encoding="utf8") as outfile:

    # loop over sentences
    for sent in doc.sentences:

        # loop over indexes of words in current sentence in order to look at words to the right of the current word
        for i in range(len(sent.words)):

            # use a try-except block to deals with verbs close to the end of sentences
            try:

                # grab current word and three words to the right
                wd0 = sent.words[i]
                wd1 = sent.words[i+1]
                wd2 = sent.words[i+2]
                wd3 = sent.words[i+3]

                # using both POS tags and words, test whether there is a "way" construction
                if re.search(r"^V", wd0.xpos) and re.search(r"PRP\$|WP\$", wd1.xpos) and re.search(r"\bway\b", wd2.text, flags=re.IGNORECASE) and re.search(r"IN", wd3.xpos):
                    outfile.write(f"{wd0.text} {wd1.text} {wd2.text} {wd3.text}\n")

            except IndexError:
                continue
