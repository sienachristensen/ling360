'''
Possible solutions to in-class practice exercises
for Lesson 8.1
about basic statistics
'''

### statistics module

# Download (or reuse) a .txt file of a novel of your choice from the Gutenberg Project.
# I've got Pride and Prejudice

# Calculate the measures of central tendency and the measures of dispersion for sentence length.
import statistics, re
from pathlib import Path

# Change the path to match your context
pride_and_prejudice_path = Path("./Jane_Austen/Pride_and_Prejudice.txt")
sentence_lens = []
novel_text = pride_and_prejudice_path.read_text(encoding="utf-8")

sentences = re.split(r"[\.\?!]", novel_text, flags=re.I)

for sentence in sentences:
    wds = sentence.split()
    sentence_lens.append(len(wds))

# Using f-strings to inserted Python expressions into string
print("Info about Pride and Prejudice by Jane Austen:")
print(f"mean sentence length: {statistics.mean(sentence_lens)}")
print(f"median sentence length: {statistics.median(sentence_lens)}")
print(f"the mode of sentence length: {statistics.mode(sentence_lens)}")
print(f"standard deviation of sentence length: {statistics.stdev(sentence_lens)}")
print(f"variance of sentence length: {statistics.variance(sentence_lens)}")

# Download (or reuse) a .txt file of a novel from a different author and calculate the same measurements.
# Huck Finn by Mark Twain

import statistics, re
sentence_lens = []

# Change the path to match your context
huck_finn_path = Path("./Mark_Twain/Huck_Finn.txt")
novel_text = huck_finn_path.read_text(encoding="utf-8")

sentences = re.split(r"[\.\?!]", novel_text, flags=re.I)
for sentence in sentences:
    wds = sentence.split()
    sentence_lens.append(len(wds))

# Using f-strings to inserted Python expressions into string
print("Info about The Adventures of Huckleberry Finn by Mark Twain:")
print(f"mean sentence length: {statistics.mean(sentence_lens)}")
print(f"median sentence length: {statistics.median(sentence_lens)}")
print(f"the mode of sentence length: {statistics.mode(sentence_lens)}")
print(f"standard deviation of sentence length: {statistics.stdev(sentence_lens)}")
print(f"variance of sentence length: {statistics.variance(sentence_lens)}")

# Compare the two authors' sentence lengths, looking at measures of central tendency, as well as how varied their sentence lengths are, with measures of dispersion.
'''
Jane Austen and Mark Twain have very similar central tendencies of sentence length, as measured with the mean and median sentence length in words. The mode is different between the two authors. However, the length of Mark Twain's sentences vary more than Jane Austen's, as seen in the larger measures of dispersion (standard deviation and variance).
'''
