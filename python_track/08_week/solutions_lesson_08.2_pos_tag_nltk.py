'''
Possible solutions to in-class practice exercises
for Lesson 8.2
about part-of-speech tagging with NLTK library
'''


# Write a sentence or two about the topic of your choice. Try to include a word or two that has different parts of speech depending on the context, for example, "The judge must record the new world record" (verb / noun) or "Yesterday I talked with her, but I haven't talked with her yet today" (simple past / present perfect).
text = "I love linguistics as well as computer programming. That is to say, technical approaches to studying language interest me, or in other words, I have an interest in language and computers."

# Tag the part-of-speech of the sentence(s) and look up the tags in the Penn Treebank tagset. Determine how many of the words were tagged correctly.
import nltk
tokens = nltk.word_tokenize(text)
tagged  = nltk.pos_tag(tokens)
print(tagged)
# I looked at the Penn Treebank tagset online at: https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
# It had some trouble with "as well as", and it mistagged the first "interest" as a noun (NN) when it is a verb.

# Find a paragraph or two online and copy and paste it/them as the input text in your program. Rerun your program and skim the tags and look for any incorrect tags.
text = "Welcome! Are you completely new to programming? If not then we presume you will be looking for information about why and how to get started with Python. Fortunately an experienced programmer in any programming language (whatever it may be) can pick up Python very quickly. It's also easy for beginners to use and learn, so jump in!"
import nltk
tokens = nltk.word_tokenize(text)
tagged  = nltk.pos_tag(tokens)
print(tagged)

# Modify your previous program to use the "universal" tagset. Next, calculate how many of each tag the paragraph(s) has.
import nltk
import collections
tokens = nltk.word_tokenize(text)
tagged  = nltk.pos_tag(tokens, tagset="universal")  # uses a simplied tagset
tags = [i[1] for i in tagged]  # uses a list comprehension to get only the tags, which are the second element in each tuple, within the list
tag_freqs = collections.Counter(tags)
print(tag_freqs)

# Modify your program to tag a text of your choice (perhaps a novel from Gutenberg, or a speech from a political leader). Find all comparative adjectives in the document and (if there aren't too many) report the precision of the tagger with comparative adjectives.

# I'll use a speech from Barack Obama from 2005 at the 65th birthday party for John Lewis
# http://obamaspeeches.com/003-John-Lewis-65th-Birthday-Gala-Obama-Speech.htm

from urllib.request import urlopen
from lxml import etree
import nltk, re

###
# get text from website within Python itself
obama = urlopen("http://obamaspeeches.com/003-John-Lewis-65th-Birthday-Gala-Obama-Speech.htm")
obama = etree.parse(obama, etree.HTMLParser())
obama = obama.xpath('//p/font[@size="3"]')
obama = [i.text for i in obama]  # gets the text
obama = [i.split() for i in obama if i is not None]  # deletes a rogue None value
obama = [i for j in obama for i in j]  # flatten list of lists to one list
obama = ' '.join(obama)  # creates a string from the elements of the list
###

obama = nltk.word_tokenize(obama)
obama = nltk.pos_tag(obama)

for i in obama:
    if re.search(r'^JJR$', i[1]):
        print(i[0])

# Five comparative adjectives were found. All of them are in actuality comparative adjectives, and therefore, the precision is 100% (or 1.0).
