'''
Possible solutions to in-class practice exercises
for Lesson 8.3
about part-of-speech tagging with TreeTagger
'''

# Tag for part-of-speech 1 Nephi 1:1 in the Book of Mormon in several different languages.

import treetaggerwrapper

tagger = treetaggerwrapper.TreeTagger(TAGDIR='/Users/ekb5/Documents/treetagger', TAGLANG='es')  # specify the language here

# tags = tagger.tag_text("I, Nephi, having been born of goodly parents, therefore I was taught somewhat in all the learning of my father; and having seen many afflictions in the course of my days, nevertheless, having been highly favored of the Lord in all my days; yea, having had a great knowledge of the goodness and the mysteries of God, therefore I make a record of my proceedings in my days.")
tags = tagger.tag_text("Yo, Nefi, nací de buenos padres y recibí, por tanto, alguna instrucción en toda la ciencia de mi padre; y habiendo conocido muchas aflicciones durante el curso de mi vida, siendo, no obstante, altamente favorecido del Señor todos mis días; sí, habiendo logrado un conocimiento grande de la bondad y los misterios de Dios, escribo, por tanto, la historia de los hechos de mi vida.")
# tags = tagger.tag_text("Сега стана така, че през първата година на управлението на съдиите над народа на Нефи, отсега нататък, след като цар Мосия беше тръгнал по пътя на цялата земя, след като беше воювал добро войнстване и беше ходил правдиво пред Бога, той не остави никой да царува на негово място; при все това, той беше установил закони и те бяха признати от народа; ето защо, те бяха задължени да се придържат към законите, които той беше направил.")
# tags = tagger.tag_text("Ныне было так, что в первый год правления судей над народом Нефиевым царь Мосия, отошедший путём всех земных, воинствовавший, как добрый воин, ходивший в непорочности перед Богом и не оставивший никого, кто правил бы на его месте, тем не менее учредил законы с того времени впредь, и они были признаны народом; а потому народ был обязан следовать законам, которые он установил.")
# tags = tagger.tag_text("Ich, Nephi, stamme von guten Eltern, darum ist mir von allem Wissen meines Vaters etwas beigebracht worden; und da ich im Laufe meiner Tage viele Bedrängnisse erlebt habe, da mir der Herr jedoch alle meine Tage auch viel Gunst erwiesen hat; ja, da mir eine reiche Erkenntnis von der Güte Gottes und seinen Geheimnissen zuteil geworden ist, darum mache ich einen Bericht von meinen Handlungen in meinen Tagen.")

print(tags)  # prints a string with word + tab + POS tag + tab + lemma

pretty_tags = treetaggerwrapper.make_tags(tags)
print(pretty_tags)
# for i in pretty_tags:
#     print(i[0] + "\t" + i[1] + '\t' + i[2])
