# Testing Lesson 1

## Basic test with built-in `unittest`
[See documentation on unittest]()
```
import unittest

class TestMyFirstTest(unittest.TestCase):

    def test_one_plus_one_is_two(self):
        self.assertEqual(1 + 1, 2)
        
    # Write your own test and try a different assertion method
```
For a basic overview of Test Driven Development see [this test-driven development tutorial](https://medium.com/@jph94880/test-driven-development-with-pytest-a-guide-25ac686412d)

## Terminology
- unit testing - writing tests that verify that a small unit of code (often a single function) is working correctly.
- test-driven development (TDD) - a set of steps for writing quality code commonly abbreviated as 1. Red 2. Green 3. Refactor
- refactoring - editing working code to be more efficient or more reader friendly for programmers without adding any new functionality
- regression - when a code change causes the overall program to fail, well written tests can fail to warn the programmer about this
- brittle test - a poorly written test that fails when it shouldn't after making small changes to your code.
- fixture - a piece of data required to be created or setup to properly test a piece of code or function
- assertions - code statements at the end of a test that affirm the correctness of the result

## Basic test with installable `pytest`
[Instructions to setup `pytest` in Pycharm](https://www.jetbrains.com/help/pycharm/pytest.html#enable-pytest)

```

```
## Test Driven Development
- Arrange (setup)
- Act (execution of unit, function or system being tested)
- Assert (assert that the results of execution are as expected)

## Exercise to write tests

## Writing Tests that Fail Helpfully -- Avoid Brittle tests

- Demonstrate how previous test is brittle
- Refactor brittle test to fail helpfully

## Exercise to refactor previous tests
