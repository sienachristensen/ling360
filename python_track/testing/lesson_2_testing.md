# Testing Lesson 2 - Combining End-To-End Tests with Unit Tests

In this lesson we will talk about end-to-end tests (also known as
acceptance tests among other names).  
This testing lesson will be based on assignment 8 in which you were required
to write a program that read in several text files of various registers and
output csv files showing noun-noun sequences of common nouns and their frequency
count.  Here we are simplifying by ignoring register and just outputting a single
CSV file with the noun pairs in the first column and counts in the second.

First we write a failing end-to-end test using the Arrange, Act, Assert pattern.
```
from pathlib import Path
import pos_tagging

def test_creates_frequency_list_csv_file_ordered_from_highest_frequency():
    # Our 'Arrange' or setup here includes having
    # some test input files inside our fixtures directory
    # and some expected content of the result file

    # Arrange
    expected_csv_content = """
    food intake,2
    life patterns,1
    mouse weight,1
    museum reconstructions,1
    """

    # Act
    pos_tagging.generate_noun_noun_frequency_counts("./fixtures", "results.csv")
    result_file = Path("./results.csv")  # './' means current directory

    # Assert
    assert result_file.read_text() == expected_csv_content
```

Now that we have an end-to-end test written, we can focus on writing unit tests that will lead to a passing end-to-end test

First unit test:
```
# test get_text function
import pos_tagging
def test_get_text_extracts_and_combines_all_files_text():
    # Arrange
    target_dir = "./fixtures"
    text_snippet = "food intake"

    # Act
    extracted_text = pos_tagging.get_text(target_dir)

    # Assert
    assert text_snippet in extracted_text
```

Next unit test:

```
def test_tokenize_and_tag_returns_a_list_of_tagged_words():
    # Arrange
    input_text = "I took the dog for a walk."
    expected_output = [
        ("I", "PRP"),
        ("took", "VBD"),
        ("the", "DT"),
        ("dog", "NN"),
        ("for", "IN"),
        ("a", "DT"),
        ("walk", "NN"),
        (".", ".")
    ]

    # Act
    tagged_words = pos_tagging.tokenize_and_tag(input_text)

    # Assert
    assert tagged_words == expected_output
```


Final Solution to all tests
```
```