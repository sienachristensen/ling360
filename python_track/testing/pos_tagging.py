from pathlib import Path


def generate_noun_noun_frequency_counts(dir_path, outfile_name):
    text = get_text(dir_path)
    tagged_words = tokenize_and_tag(text)
    unsorted_counts = count_noun_noun_pairs(tagged_words)
    sorted_counts = sort_counts_high_to_low(unsorted_counts)
    write_to_file(outfile_name, sorted_counts)

def get_text(dir_name):
    all_text = ""
    for file in Path(dir_name).glob("*.txt"):
        all_text += " " + file.read_text()

    return all_text


def tokenize_and_tag(text_input):
    from nltk import pos_tag, word_tokenize

    tokens = word_tokenize(text_input, language="english")

    return pos_tag(tokens, lang="eng")


def count_noun_noun_pairs(tagged_words):
    from nltk import pos_tag, word_tokenize
    last_pos = ""
    last_word = ""
    result = {}
    for (word, pos) in tagged_words:
        if is_noun(last_pos) and is_noun(pos):
            noun_noun = last_word.lower() + " " + word.lower()
            result[noun_noun] = result.get(noun_noun, 0) + 1
        last_pos = pos
        last_word = word

    return result

def is_noun(pos):
    if pos == "NN" or pos == "NNS":
        return True
    else:
        return False


def sort_counts_high_to_low(noun_noun_dict):
    return sorted(noun_noun_dict.items(), key=lambda x: x[1], reverse=True)

def write_to_file(file_name, data):
    import csv

    with open(file_name, mode='w', newline="") as outfile:
        csv_writer = csv.writer(outfile)

        for key,val in data:
            csv_writer.writerow([key,val])

