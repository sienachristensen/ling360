from pathlib import Path
import pos_tagging

def test_creates_frequency_list_csv_file_ordered_from_highest_frequency():
    # Our 'Arrange' or setup here includes having
    # some test input files inside our fixtures directory
    # and some expected content of the result file

    # Arrange
    expected_csv_content = ("food intake,2\n"
    "mouse weight,1\n"
    "museum reconstructions,1\n"
    "life patterns,1\n"
    )

    # Act
    pos_tagging.generate_noun_noun_frequency_counts("./fixtures", "results.csv")
    result_file = Path("./results.csv")  # './' means current directory

    # Assert
    assert result_file.read_text() == expected_csv_content