# test get_text function
import pos_tagging
import pytest

#@pytest.mark.skip
def test_get_text_extracts_and_combines_all_files_text():
    # Arrange
    target_dir = "./fixtures"
    text_snippet = "food intake"

    # Act
    extracted_text = pos_tagging.get_text(target_dir)

    # Assert
    assert text_snippet in extracted_text


#@pytest.mark.skip
def test_tokenize_and_tag_returns_a_list_of_tagged_words():
    # Arrange
    input_text = "I took the dog for a walk."
    expected_output = [
        ("I", "PRP"),
        ("took", "VBD"),
        ("the", "DT"),
        ("dog", "NN"),
        ("for", "IN"),
        ("a", "DT"),
        ("walk", "NN"),
        (".", ".")
    ]

    # Act
    tagged_words = pos_tagging.tokenize_and_tag(input_text)

    # Assert
    assert tagged_words == expected_output


#@pytest.mark.skip
def test_count_noun_noun_pairs():
    # Arrange
    tagged_words = [
        ("house", "NN"),
        ("parties", "NN"),
        ("and", "CC"),
        ("board", "NN"),
        ("games", "NN"),
        ("at", "IN"),
        ("house", "NN"),
        ("parties", "NN")
    ]
    expected_dict = {
        'house parties': 2,
        'board games': 1
    }

    # Act
    result_dict = pos_tagging.count_noun_noun_pairs(tagged_words)

    # Assert
    assert result_dict == expected_dict