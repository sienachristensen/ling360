"""
Possible solutions to in-class practice exercises
Lesson 10.2
about harvesting comments from Youtube videos
"""

# Using your API key, harvest 100 comments from a Youtube video of your choice and save the JSON object to a .json file on your hard drive. If you don't know which video to choose, why not use this one here from Dude Perfect.
from googleapiclient.discovery import build
import json

api_key = "[YOUR API KEY HERE]"

video_id = "9X_nbT89X-c"  # a Dude Perfect video

youtube = build("youtube", "v3", developerKey=api_key)
request = youtube.commentThreads().list(
    part="snippet",
    maxResults="100",
    videoId=video_id,
    textFormat="plainText"
)
response = request.execute()
print(response)
with open("./youtube_comments_" + video_id + ".json", "w") as outfile:
    outfile.write(json.dumps(response, ensure_ascii=False))

### In different script ###

# In a different script, parse through the .json file and drill down into the nested dictionaries to get the comments out.
import json
with open("./youtube_comments.json") as infile:
    comments = json.loads(infile.read())

# print(comments)
# print(type(comments))
# print(comments.keys())
# print(type(comments["items"]))
print(len(comments["items"]))
for c in comments['items']:
    # print(c)
    # print(type(c))
    # print(c.keys())
    # print(c["snippet"])
    # print(type(c["snippet"]))
    # print(c['snippet']['topLevelComment']["snippet"]['textOriginal'])
    # print(type(c['snippet']['topLevelComment']["snippet"]['textOriginal']))
    print(c['snippet']['topLevelComment']["snippet"]['textOriginal'].strip())
