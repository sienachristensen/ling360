# Lesson 10.1: Harvesting tweets from Twitter

Note: In February 2023, Twitter stopped offering free access to their API. I leave this lesson here for reference purposes only.

## Objective:

Students will understand how to harvest tweets using Twitter's API.

Twitter has an API (application programming interface) to allow programmers to harvest (or collect) tweets with computer programs.
Python has several modules that allow Python programmers to access Twitter's API; [tweepy](http://www.tweepy.org/) is a good one.
Twitter's API returns JSON-encoded text.
The developer app has a bearer token that is needed to programmatically (that is, with Python) access Twitter's API.

There are two ways to harvest tweets:

1. Capture tweets in real-time with a streaming API listener.
Listening for a keyword or hashtag--[example here](https://docs.tweepy.org/en/stable/streamingclient.html).

2. Retrieve tweets from (approximately) the last week, from the REST API.
Query strings [here](https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query).
Tweet attributes [here](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet).
Context annotations [here](https://developer.twitter.com/en/docs/twitter-api/annotations/overview).

## Logic:

Apply for and receive a developer's account with Twitter [here](https://developer.twitter.com/en/apply-for-access).
Create a developer's app at [apps.twitter.com](https://apps.twitter.com).

**Two ways to get tweets**:

1. Listener: Using your app's bearer token, create a stream listener on the API.

2. Query: Using your app's bearer token, create a query string and request tweets from Twitter's API.

Save tweets to a .json file (or .csv) on your hard drive.

In a different Python script (.py file), parse the .json file saved to your hard drive and pull out the info you'd like from the tweets.

## Demo/Practice:

Create a streaming listener on the API to listen for tweets with a hashtag of your choice and save the tweets to a .json file on your hard drive--see [example of saving .json files](https://stackoverflow.com/questions/7100125/storing-python-dictionaries). You'll need to manually stop the script after a minute or two (or more or fewer, depending on the popularity of the hashtag and the amount of data you want).
Query the API for recently sent tweets with a query string of your choice and save them to a .json file on your hard drive.


# Lesson 10.2: Harvesting comments from Youtube videos

## Objective:

Students will harvest comments from Youtube videos with Youtube's API

## Steps:

First, [Get an API key](https://console.developers.google.com/) for Youtube at Google's developers console. Then, Start the free trial to activate the key.
Also, download the `google-api-python-client` Python module
- PyCharm > Preferences window > Project: [project_name] > Python interpreter > "+" > search for "google-api-python-client" > Install package
- pip install `google-api-python-client`
- Request comments from videos with video ID (get video ID from URL)
- 100 comments is the max without having to loop over additional pages. To get more pages, [see this Stack Overflow answer](https://stackoverflow.com/a/42531905/2884875).
- Save JSON object to .json file on your hard drive
- In a different Python script, parse through .json file and pull out comments.

## Practice:
 
- Using your API key, harvest 100 comments from a Youtube video of your choice and save the JSON object to a .json file on your hard drive. If you're unsure which video to choose, why not [use this one](https://youtu.be/9X_nbT89X-c) from Dude Perfect.
- In a different script, parse through the .json file and drill down into the nested dictionaries to get the comments out.

# Lesson 10.3: Harvesting posts from Reddit

## Objective:

Students will harvest posts from a subreddit of their choice.

Reddit is a social media platform that allows users to hide behind a cloak of anonymity and be rude and crude as they discuss a myriad of topics in what are called "subreddits".

For the language researcher who wants access to very unrehearsed and natural (written) language, Reddit is a goldmine.

## Steps:

1. Create user account, if you don't already have one.

3. [Create an app](https://www.reddit.com/prefs/apps/) and write down/save the client id code (~20 alphanumeric character code) and the client secret (~30 alphanumeric character code)

4. Download [PRAW](https://praw.readthedocs.io/en/stable/)--a Python module for the Reddit API, either in:
- PyCharm: Settings window > Python:ProjectName > Python Interpreter > + > type "praw" > click "Install Package"
- or `pip install praw` from a terminal command prompt

5. Create a Python script following the instructions given in the [PRAW documentation](https://praw.readthedocs.io/en/stable/).

## Demo:

The instructor demonstrates how to scrape the posts from a subreddit of his choice.

## Practice:

The students take the instructor's script (available in the CMS) and scrape posts from a subreddit of their choice.


# Lesson 10.4: Sentiment analysis

## Objective:

Students will analyze sentiment in social media text with the vaderSentiment sentiment analyzer.

See examples here.

Logic after downloading vaderSentiment module:
```
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
analyzer = SentimentIntensityAnalyzer()
sent = analyzer.polarity_scores("I am happy!")
print(sent["compound"])
```
## Practice:
1. Set up a sentiment analyzer and analyze some short sentences of your own.
2. Download the following files from the CMS (or use your own `.json` files with social media messages):
tweets_happy.json
youtube_comments_9X_nbT89X-c.json
3. First, parse the file with tweets and get the mean compound sentiment score.
Hint: There are 100 lines in the file, each with a JSON object that contains one tweet, so it makes sense to read in one line at a time and convert it to a Python dictionary with `json.loads()`.
4. Second, parse the file with Youtube comments and get the mean compound sentiment score.
Hint: The file has only one line with a big JSON object with all 100 comments in it, so it makes sense to read in the whole file at once (with `.read()`) and then convert it to a Python dictionary with `json.loads()`.

