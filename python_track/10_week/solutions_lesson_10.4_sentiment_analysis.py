'''
Possible solutions to in-class practice exercises
for Lesson 10.4
about sentiment analysis
'''

# Set up a sentiment analyzer and analyze some short sentences of your own.
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

analyzer = SentimentIntensityAnalyzer()
sent = analyzer.polarity_scores("I love this time of year!")
print(sent["compound"])
sent = analyzer.polarity_scores("I like this time of year.")
print(sent["compound"])
sent = analyzer.polarity_scores("I don't like this time of year.")
print(sent["compound"])
sent = analyzer.polarity_scores("I hate this time of year.")
print(sent["compound"])
sent = analyzer.polarity_scores("Everything is awesome!")
print(sent["compound"])


### Using the vader sentiment analyzer, calculate the average "compound" sentiment score for tweets.
import json, statistics
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

analyzer = SentimentIntensityAnalyzer()
all_sentiment = []  # collector for all sentiment scores

with open("./tweets_happy.json") as infile:
    for line in infile:
        tweet = json.loads(line)  # convert the JSON-formatted string to a Python dictionary
        tweet = tweet['text']  # just grab the text of the tweet
        current_sentiment = analyzer.polarity_scores(tweet)
        all_sentiment.append(current_sentiment['compound'])

print("Average sentiment score:", statistics.mean(all_sentiment))


### Second, parse the file with Youtube comments and get the mean compound sentiment score.
import json, statistics
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

analyzer = SentimentIntensityAnalyzer()
all_sentiment = []  # collector for all sentiment scores

with open("./youtube_comments_9X_nbT89X-c") as infile:
    whole_file = infile.read()
    comments = json.loads(whole_file)

for comment in comments['items']:
    comment_text = comment['snippet']['topLevelComment']["snippet"]['textOriginal']
    current_sentiment = analyzer.polarity_scores(comment_text)
    all_sentiment.append(current_sentiment['compound'])

print("Average sentiment score:", statistics.mean(all_sentiment))
