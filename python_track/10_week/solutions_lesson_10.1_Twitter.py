"""
Possible solutions to in-class practice exercises
for Lesson 10.1
on harvesting tweets from Twitter
"""

# NOTE: You will need to adjust this script for your specific situation

### Listener ###
# Create a streaming listener on the API to listen for tweets with a hashtag of your choice and save the tweets to a .json file on your hard drive (see example of saving .json files here). You'll need to manually stop the script after a minute or two (or more or fewer, depending on the popularity of the hashtag and the amount of data you want).

"""Python (v3) script to create a tweet listener on Twitter's API"""

import tweepy, json

# modify tweepy's StreamingClient class a bit
class Listener(tweepy.StreamingClient):
    ''' Handles data received from the stream. '''

    # write out info from tweet to outfile
    def on_tweet(self, tweet):
        output = {"id": tweet.id, "author_id": tweet.author_id, "created_at": str(tweet.created_at), "lang": tweet.lang, "text": tweet.text}
        print(tweet.text)
        with open("output_file.json", mode="a", encoding="utf8") as outfile:
            outfile.write(json.dumps(output, ensure_ascii=False) + "\n")


### main ###
# create empty .json file to collect tweets in
output_file = './tweets_streamed.json'
with open(output_file, "w") as fout:
    pass

# Earl's bearer token
bearer_token = "HERE"

# create an object from the class definition above
streaming_client = Listener(bearer_token=bearer_token)

# add a rule and filter with that rule
streaming_client.add_rules(tweepy.StreamRule(value="happy -is:retweet"))

# specify additional fields to return (tweet ID and tweet text are always returned, so you don't have to specify those) and filter
streaming_client.filter(tweet_fields=["created_at", "author_id", "lang"])

### end listener ###

### Query ###
# Second way to get tweets:
# Query the API for recently sent tweets with a hashtag of your choice and save them to a .json file on your hard drive.

"""Python (v3) script to query Twitter's API"""

import tweepy, json

# your app's bearer token here
bearer_token = "HERE"

client = tweepy.Client(bearer_token)

# create query string
query = "(coronavirus OR covid-19 OR covid19 OR covid) -is:retweet lang:en"
# query = "from:UchtdorfDF"

# search for tweets that match the query string criteria
tweets = client.search_recent_tweets(query=query, tweet_fields=["created_at", "author_id", "context_annotations"], max_results=100)

# write out tweets to JSON file on hard drive
with open("./tweets_queried.json", "w", encoding="utf8") as outfile:
    for tweet in tweets.data:
        output = {"id": tweet.id, "author_id": tweet.author_id, "created_at": str(tweet.created_at), "text": tweet.text}
        print(tweet.text)
        outfile.write(json.dumps(output, ensure_ascii=False) + "\n")

### end query ###
