"""
Lesson 10.3 Scrape posts from Reddit
"""

import praw
import pandas as pd
import datetime as dt

# specify your credentials
reddit = praw.Reddit(client_id= 'HERE',  # your app's ID code
                     client_secret='HERE',  # your app's secret code
                     user_agent= 'HERE',  # your app's name (e.g., Scraper)
                     username='HERE',  # your Reddit username
                     password='HERE')  # your Reddit password

# scrape posts from "compling" (i.e., computational linguistics) subreddit
subreddit = reddit.subreddit('compling')

# retrieve the top 500 posts
top_subs = subreddit.top(limit = 500)

# create an empty dictionary of lists to collect the components of each post
topics_dict = {"title": [],
               "score": [],
               "id": [],
               "url": [],
               "comms_num": [],
               "created": [],
               "body": []}

# loop over the top posts and populate the collector dictionary
for i in top_subs:
    topics_dict["title"].append(i.title)
    topics_dict["score"].append(i.score)
    topics_dict["id"].append(i.id)
    topics_dict["url"].append(i.url)
    topics_dict["comms_num"].append(i.num_comments)
    topics_dict["created"].append(i.created)
    topics_dict["body"].append(i.selftext)

# create a pandas data frame from the dictionary
topics_data = pd.DataFrame(topics_dict)

# show the user the results in the console
print(topics_data)

# define a helper function to convert the timestamp to a human-friendly format
def get_date(created):
    return dt.datetime.fromtimestamp(created)

# create a new column with the human-friendly timestamp
_timestamp = topics_data["created"].apply(get_date)
topics_data = topics_data.assign(timestamp = _timestamp)

# write out a CSV file
topics_data.to_csv('./reddit_posts.csv', index=False)

# write out an Excel file
writer = pd.ExcelWriter('./reddit_posts.xlsx')
topics_data.to_excel(writer,'reddit_posts', index = False)
writer.save()
