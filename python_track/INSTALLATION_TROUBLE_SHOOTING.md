# Help with Installing Additional Python Packages or Other Software

## NLTK

If you're having trouble running NLTK because of SSL errors,
then you can use this code. Paste it into a script or on the Python console, then run it, replacing the last part with the resource that you are missing.

```
import nltk
import ssl

ssl._create_default_https_context = ssl._create_unverified_context
nltk.download('insert_name_of_resource_here')
exit(0)
```


## General Help

If you are unable to get a Python package installed or just want the convenience of an alternative to PyCharm, then another option is to use an alternative coding environment such as Google's [Colab](https://colab.research.google.com/) or [PaperSpace](https://www.paperspace.com/)

Both services require you to sign up (If you have a Google Account then you can use that login for Colab).  You can write or copy/paste code into a notebook and download it as a .ipynb or .py file.  They also let you upload resources including texts that you want your script to process.  Most or all tools that you would need to use should already be installed in Colab including NLTK.  If you get an error that any Python module is not available, you can install it via the notebook in a blank cell by typing this:

```
!pip install name-of-python-package
```