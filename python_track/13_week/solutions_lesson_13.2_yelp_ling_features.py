'''
Possible solution to in-class practice exercise
for Lesson 13.2
about correlating number of stars in Yelp reviews with linguistic features
'''

#####
# Exercise:
# Using the .json file with AZ 2018 Yelp reviews in the CMS, create a .csv file with two columns: column A = number of stars in the current review, column B = number of occurrences of the word "delicious" in the current review.

# Installation requirements:
# matplotlib
# seaborn
# pandas
# scipy
#####

import json, re, csv

# open both the in-file and the out-file with a single "with" keyword
with (open('yelp_AZ_2018.json', mode='r', encoding='utf-8') as fin,
      open('review_data.csv', mode='w', encoding='utf-8', newline='') as fout):
    csv_writer = csv.writer(fout)
    csv_writer.writerow(['stars','N_feature'])
    for review in fin:
        review = json.loads(review)
        matches = re.findall(r'\bdelicious\b', review['text'], flags=re.I)
        num_matches = len(matches)
        csv_writer.writerow([str(review['stars']), str(num_matches)])


# Use the .csv file created in the previous exercise to calculate the correlation between the number of stars and the number of occurrences of "delicious". You can assume a normal distribution of the data and use the most common correlation test: Pearon's r.
import pandas
yelp = pandas.read_csv('review_data.csv', sep=',')

# calculates Pearson's correlation coefficient
from scipy.stats.stats import pearsonr
correlation = pearsonr(yelp.N_feature, yelp.stars)  # the format is: DataFrame.Column
print(correlation)  # prints a two-element tuple: the first element is the correlation coefficient, which ranges from -1 to 0 for negative correlations, and from 0 to 1 for postive correlations; the second element is the p-value indicating statistical significance, with values at or below 0.05 indicating statistical significance; for more info, see https://en.wikipedia.org/wiki/Pearson_correlation_coefficient

# creates scatterplot with regression line
import matplotlib.pyplot as plt
import seaborn

# create the plot
seaborn.lmplot(x='N_feature', y='stars', data=yelp, fit_reg=True)  # 'x' is the x-axis, 'y' is the y-axis, and 'data' is the pandas DataFrame, and 'fit_reg' indicates whether a regression line should be drawn through the data points
plt.show()  # display the plot


exit(0)
#####
# Exercise:
# Correlate the number of stars in the reviews in the sample (100K) dataset with the number of times a vowel is repeated three or more times, for example, "this restaurante is waaay better than that one" or "that business is soooooo overraaaated".
#####

###
# this section creates the .csv file and saves it to the hard drive
import json, re
with open('/Users/ekb5/Documents/LING_360/yelp_AZ_2018.json', encoding='utf-8') as fin, open('/Users/ekb5/Downloads/review_data.csv', mode='w') as fout:
        fout.write('stars,N_feature\n')
        for review in fin:
            review = json.loads(review)

            # gnarly regex to get vowels repeated at least three times
            matches = re.finditer(r'([aeiou])\1{2,}', review['text'], flags=re.I)  # mind finditer (not findall)!
            num_matches = len([i for i in matches])  # can't call len() directly on an iterator, hence the list comprehension
            fout.write(str(review['stars']) + ',' + str(num_matches) + '\n')

###
# this section analyzes the .csv file

# read .csv file into pandas DataFrame
import pandas
yelp = pandas.read_csv('/Users/ekb5/Downloads/review_data.csv', sep=',')

# calculates Pearson's correlation coefficient
from scipy.stats.stats import pearsonr
correlation = pearsonr(yelp.N_feature, yelp.stars)  # the format is: DataFrame.Column
print(correlation)  # prints a two-element tuple: the first element is the correlation coefficient, which ranges from -1 to 0 for negative correlations, and from 0 to 1 for postive correlations; the second element is the p-value indicating statistical significance, with values at or below 0.05 indicating statistical significance; for more info, see https://en.wikipedia.org/wiki/Pearson_correlation_coefficient

# creates scatterplot with regression line
import matplotlib.pyplot as plt
import seaborn
seaborn.lmplot(x='N_feature', y='stars', data=yelp, fit_reg=True)  # 'x' is the x-axis, 'y' is the y-axis, and 'data' is the pandas DataFrame, and 'fit_reg' indicates whether a regression line should be drawn through the data points
plt.show()


#####
# Exercise:
# Correlate the number of stars in reviews with the number of times (double) quotation marks are used.
#####

###
# this section creates the .csv file and saves it to the hard drive
import json, re
with open('/Users/ekb5/Documents/LING_360/yelp_AZ_2018.json', encoding='utf-8') as fin, open('/Users/ekb5/Downloads/review_data.csv', mode='w') as fout:
        fout.write('stars,N_feature\n')
        for review in fin:
            review = json.loads(review)
            matches = re.findall(r'"', review['text'], flags=re.I)
            num_matches = len(matches)
            fout.write(str(review['stars']) + ',' + str(num_matches) + '\n')

###
# this section analyzes the .csv file

# read .csv file into pandas DataFrame
import pandas
yelp = pandas.read_csv('/Users/ekb5/Downloads/review_data.csv', sep=',')

# calculates Pearson's correlation coefficient
from scipy.stats.stats import pearsonr
correlation = pearsonr(yelp.N_feature, yelp.stars)  # the format is: DataFrame.Column
print(correlation)  # prints a two-element tuple: the first element is the correlation coefficient, which ranges from -1 to 0 for negative correlations, and from 0 to 1 for postive correlations; the second element is the p-value indicating statistical significance, with values at or below 0.05 indicating statistical significance; for more info, see https://en.wikipedia.org/wiki/Pearson_correlation_coefficient

# creates scatterplot with regression line
import matplotlib.pyplot as plt
import seaborn
seaborn.lmplot(x='N_feature', y='stars', data=yelp, fit_reg=True)  # 'x' is the x-axis, 'y' is the y-axis, and 'data' is the pandas DataFrame, and 'fit_reg' indicates whether a regression line should be drawn through the data points
plt.show()
