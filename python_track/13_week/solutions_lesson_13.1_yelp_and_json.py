'''
Possible solutions to in-class practice exercises
for Lesson 13.1
about Yelp reviews and parsing JSON-formatted data
'''


# Find the range of dates during which the reviews in the sample (100K) dataset were written.
import json
with open('/Users/ekb5/Documents/LING_360/yelp_AZ_2018.json') as fin:
    dates = []

    # This for loop will loop over each line of the file, one line at a time.
    for review in fin:
        review = json.loads(review)  # converts current review from str to dict
        dates.append(review['date'])

dates.sort()  # sorts dates (modifies in place)
first_date = dates[0]  # gets the first date in the sorted list
last_date = dates[-1]  # gets the last date in the sorted list

print('The first review was written on:', first_date)
print('The last review was written on:', last_date)

# Find the number of reviews written on Wednesday, and the number of reviews written on Saturday.
import json
from datetime import datetime as dt
n_wed = 0
n_sat = 0
with open('/Users/ekb5/Documents/LING_360/yelp_AZ_2018.json') as fin:

    # Process the file, one line at a time
    for review in fin:
        review = json.loads(review)
        if dt.strptime(review['date'], '%Y-%m-%d %H:%M:%S').weekday() == 2:
            n_wed += 1
        elif dt.strptime(review['date'], '%Y-%m-%d %H:%M:%S').weekday() == 5:
            n_sat += 1
        else:
            continue

print('The number of reviews written on Wednesday is', n_wed)
print('The number of reviews written on Saturday is', n_sat)

# Find the average number of stars for reviews written on Wednesday and compare it to the average number of stars for reviews written on Saturday.
import json
from datetime import datetime as dt
from statistics import mean
star_wed = []
star_sat = []
with open('/Users/ekb5/Documents/LING_360/yelp_AZ_2018.json') as fin:
    # Process file one line at a time
    for review in fin:
        review = json.loads(review)
        if dt.strptime(review['date'], '%Y-%m-%d %H:%M:%S').weekday() == 2:
            star_wed.append(review['stars'])
        elif dt.strptime(review['date'], '%Y-%m-%d %H:%M:%S').weekday() == 5:
            star_sat.append(review['stars'])
        else:
            continue

print('The average star ranking for reviews written on Wednesday is', mean(star_wed))
print('The average star ranking for reviews written on Saturday is', mean(star_sat))
