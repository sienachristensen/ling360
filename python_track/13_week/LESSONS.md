# Lesson 13.1: Yelp reviews

## Objective:

Students will analyze Yelp reviews.

## Yelp customer reviews

Yelp makes available (some) customer reviews in JSON format (and as a SQL database) for data analysis challenges.

I've created a couple .json files:

## "yelp_AZ_2021.json"

has 27,334 reviews written in 2021 about businesses in Arizona with "Restaurants" and/or "Food" in their "categories" description. The file is 18 MB.

## "yelp_AZ_2018.json"

has 119,376 reviews written in 2018 about all businesses in Arizona. The file is 83 MB.

Download one or both files from the CMS to your harddrive.

## Logic:

1. `import json`
2. Open a connection to the .json file on your hard drive.
3. Loop over the connection with a for loop in order to read in each review, as each review is on a separate line.
4. In each iteration of the for loop, convert the JSON-formatted string to a Python dictionary with the json.loads() function.
5. Access the value associated with the key that you want to analyze, and add it to a collector list or use it in some other way.

## Practice:

1. Find the range of dates during which the reviews in the sample AZ 2021 dataset or the AZ 2018 dataset were written.
2. Find the number of reviews written on Wednesdays, and the number of reviews written on Saturdays.
3. Hint: You'll need to use the datetime module. See StackOverflow here and here, and read the docs here.
4. Find the average number of stars for reviews written on Wednesdays and compare it to the average number of stars for reviews written on Saturdays.
5. **Bonus**: find average of stars for all days


# Lesson 13.2: Yelp reviews and linguistic features

## Objective:

Students will correlate the number of stars in Yelp reviews with linguistic features.

## Logic:

1. Create a .csv file with as many rows as there are reviews in the .json file, and that has several columns: business id, number of stars, number of occurrences of linguistic feature(s).
2. Import the .csv file into Python (or into R, Julia, or Excel) and get the Pearson's r correlation value between the column with the number of stars and the column with the number of occurrences of the linguistic feature in question.
3. Visualize the same two columns with a scatterplot, with a regression line, and/or a boxplot.

## Practice:

1. Using the .json file with AZ 2021 or AZ 2018 Yelp reviews in the CMS, create a .csv file with two columns: column A = number of stars in the current review, column B = number of occurrences of the word "delicious" in the current review.
2. Use the .csv file created in the previous exercise to calculate the correlation between the number of stars and the number of occurrences of "delicious". For this practice exercise, you can assume a normal distribution of the data and therefore use the most common correlation test: Pearson's r. See example of Python code here.
3. Correlate the number of stars in the reviews in the AZ 2018 or AZ 2021 Yelp dataset with the number of times a vowel is repeated three or more times, for example, "this restaurant is waaay better than that one" or "that business is soooooo overraaaated".
4. Correlate the number of stars in reviews with the number of times (double) quotation marks are used.
Choose another linguistic feature and correlate it with the number of stars in the reviews in the sample dataset. Be prepared to share with the class what you find.

# Lesson 13.3: Macros in Microsoft Word

## Objective:

Students will run already-made macros for Microsoft Word.

Macros in Microsoft Word (and Excel and LibreOffice and Google Docs) are snippets of computer code that perform specific tasks on a Word document (or other type of file).

Paul Beverley has written a veritable plethora of macros for Microsoft Word geared towards editing tasks; see the complete list of macros here.
The main pre-editing macro is FRedit, which is a global find-and-replace macro (Find-and-Replace edit).
Download the FRedit macro and follow the instructions (i.e., "1_instructions.docx" file) to get it running.
Add some new find-and-replace instructions in the "4_Sample_List.docx" file and rerun the macro.
ProperNounAlyse: Identifies proper nouns that might be misspelled.
NumberToText: Converts a numeral into the spell-out version of the number.
MatchDoubleQuotes: Identifies any paragraphs that contain an odd number of opening and closing double quotation marks.

## Practice:

Students get FRedit working on their computer.
Students get at least one other macro of your choice working on their computer.
