'''
Possible solutions to in-class practice exercises
for Lesson 2.2
on if...else
'''

# Create a program that asks the user for a number between 1 and 10 and prints back to the user one of two messages stating whether the number is: (a) less than five, or (b) greater than or equal to five.

number = input("Give me a number between 1 and 10, please: ")
number = int(number)  # converts from type str(ing) to type int(eger)
if number < 5:
    print("The number you gave me is less than five!")
else:
    print("The number you gave me is greater than or equal to five!")


# Modify the previous program so that the program prints back to the user one of three messages: (a) less than, (b) equal to, or (c) greater than five.

number = input("Give me a number between 1 and 10, please: ")
number = int(number)  # converts from type str(ing) to type int(eger)
if number < 5:
    print("The number you gave me is less than five!")
elif number == 5:
    print("The number you gave me is equal to five, that is, it's five!")
else:
    print("The number you gave me is greater than five!")


# Modify the previous program so that the message printed back to the user indicates whether the number is: (a) between one and three, (b) between four and six, or (c) between seven and ten.

number = input("Give me a number between 1 and 10, please: ")
number = int(number)  # converts from type str(ing) to type int(eger)
if number >= 1 and number <= 3:
    print("The number you gave me is between one and three!")
elif number >= 4 and number <= 6:
    print("The number you gave me is between four and six!")
elif number >= 7 and number <= 10:
    print("The number you gave me is between seven and ten!")
else:
    print("You didn't give me a number between 1 and 10. Sup with that?!")


# Create a program that asks the user for his/her two favorite fruits and prints a message stating whether the first fruit given by the user comes before or after the second one in a dictionary (in alphabetical order).

fruit1 = input("Please give me a fruit: ")
fruit2 = input("Now, please give me another one: ")
if fruit1 == fruit2:
    print("You gave me the same fruit, sillykins!")
elif fruit1 < fruit2:
    print("The first fruit you gave me '" + fruit1 + "' comes before the second one '" + fruit2 + "' in a dictionary (in alphabetical order).")
else:
    print("The first fruit you gave me '" + fruit1 + "' comes after the second one '" + fruit2 + "' in a dictionary (in alphabetical order).")


# Modify the previous program so that the program asks the user for only one fruit and the message printed back to the user indicates if the fruit given by the user falls between "guava" and "passion fruit" in the dictionary.

fruit = input("Please give me a fruit: ")
if fruit > "guava" and fruit < "passion fruit" :
    print("The fruit you gave me falls between 'guava' and 'passion fruit'")
elif fruit < "guava":
    print("The fruit you gave me comes before 'guava' in a dictionary")
else:
    print("The fruit you gave me comes after 'passion fruit' in a dictionary")


# Bonus: Modify your program so that if the user gives "guava" or "passion fruit," the message states this fact.

fruit = input("Please give me a fruit: ")
if fruit == 'guava' or fruit == 'passion fruit':
    print("You gave me 'guava' or 'passion fruit'. How coincidental!")
