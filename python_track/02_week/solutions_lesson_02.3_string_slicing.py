"""
Possible solutions to in-class practice exercises
for Lesson 2.3
on string slicing
"""
### String slicing

# Write a program that asks the user for a word of at least six letters in length, then prints back to the user on one line the first three letters, and then on a second line prints from the fourth letter to the end of the word.
wd = input("Please give me a word that is at least six letters in length: ")
print(wd[:3])
print(wd[3:])

# Modify the program to print out every other letter.
wd = input("Please give me a word that is at least six letters in length: ")
print(wd[::2])

# Modify the program to print out the word spelled backward.
wd = input("Please give me a word that is at least six letters in length: ")
print(wd[::-1])

# Write a program that asks the user for a word and prints back whether the word is a palindrome (that is, spelled the same way forward and backward, for example, "noon").
wd = input("Please give me a word, any word: ")
wd = wd.lower()  # converts input to lowercase
wdrev = wd[::-1]  # gets the word spelled backward
if wd == wdrev:  # compares the word and its reverse; mind the comparison operator, the double equal sign!
    print("You gave me a palindrome! Thanks!")
else:
    print("You gave me a boring word. Try again.")
