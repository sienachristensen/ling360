# pluralize creates the plural of a singular noun
def pluralize(word):
    # if the last three characters are 'man' then create
    # a new string that ends in 'en'
    if word[-3:] == "man":
        return word[0:-2] + "en"
    # if the word is moose just return it
    elif word == "moose":
        return word
    # if the word is something like 'goose' return 'geese'
    elif word[-4:] == "oose":
        return word[0:-4] + "eese"
    # in all other cases return a string with an 's' appended
    else:
        return word + "s"

print(pluralize("woman"))
print(pluralize("dog"))
print(pluralize("moose"))