## Week 2

## Lesson 2.1: Errors
**Objective**: <br>
Students will identify and distinguish three types of errors in code.

Three types of errors can occur when writing computer code, as presented in [Think Python, chapter 2, section 2.8](http://greenteapress.com/thinkpython2/html/thinkpython2003.html#sec23)

1. Syntax error
2. Run-time error (e.g. trying to string concatenate a str with an int or float without first converting the int or float to a str; trying to divide an int or float by zero)
3. Semantic error

**Formative quiz**: <br>
Identify the type of error in the following situations, code, or error messages.
The programmer wanted the first three letters of each word, but the program gave him/her the first four letters.
```
print('hello' + 4) > TypeError: must be str, not int
# prints last letter of word
# print("hello"[-1])
```

The programmer tries to take the log of zero.
```
print("hello world"
print("What's your name: ") asks the user for his/her name
7 / 0 > ZeroDivisionError
```

## Lesson 2.2: if...else

**Objective**: <br>
Students will write conditional execution code using logical operators (and, or, not) and conditional statements (if...else, if...elif...else).

Think Python, Chapter 5: Conditionals and Recursion
Boolean expressions

### Practice:
Use boolean operators (==, !=, >, <, >=, <=) with various sets of number to understand how they work with values of type int and float, for example

```
5 > 8 # False
5 != 8  # True
5.7 <= 8  # True
7 >= 7  # True
9 == 9.02  # False
Use the same boolean operators with various letters and words to understand how they work with values of type str, for example:
'a' < 'b'
'f' > 'y'
'apple' <= 'banana'
'orange' == 'naranja'
'lanky' != 'tall'
'antelope' > 'alpaca'
```

### Logical operators: and, or, not

Pretty darn similar to their meaning in English. They're used in conditional execution (see next item!).
Conditional execution and alternative execution, also called "conditional statements," "control flow," or simply if...else statements
See examples in textbook, section 5.4.

_Note_: Mind the indentation and colon (in Python)!

### Practice:

Create a program that asks the user for a number between 1 and 10 and prints back to the user one of two messages stating whether the number is: (a) less than five, or (b) greater than or equal to five.

_Hint_: You will have to use the `int()` function to change the input from data type str to int.
Modify the previous program so that the program prints back to the user one of three messages: (a) less than, (b) equal to, or (c) greater than five.
Modify the previous program so that the message printed back to the user indicates whether the number is:

1. between one and three
2. between four and six, or
3. between seven and ten.

_Hint_: You will likely need to use and in your conditional statements.
Create a program that asks the user for his/her two favorite fruits and prints a message stating whether the first fruit given by the user comes before or after the second one in a dictionary (in alphabetical order).
Chained conditionals: `if...elif...else`

_See examples in section 5.6._

### Practice:

Modify the previous program so that the program asks the user for only one fruit and the message printed back to the user indicates if the fruit given by the user falls between "guava" and "passion fruit" in the dictionary.

_Hint_: You might use the logical operator and and several elif statements.
Bonus: Modify your program so that if the user gives "guava" or "passion fruit," the message states this fact.

## Lesson 2.3: String slicing

**Objective**: <br>
Students will be capable of slicing strings by indexing in order to extract only a certain amount of a string.

[Think Python, Chapter 8: Strings](https://greenteapress.com/thinkpython2/html/thinkpython2009.html)

Strings are sequences of characters; see Figure 8-1 in Downey, Chapter 8, Section 8.4 "String slices".

Strings can be sliced from the left side with a positive index, or the right side with a negative index.

```
# (Mind the zero-based indexing of Python!)
'orange'[2] # returns 'a' 
'orange'[-1] # returns 'e'
'orange'[-2:] # returns 'ge'
```

There are three parameters: `'some_string'[ begin : end : step_size ]`

```
'orange'[2:] # returns 'ange'
'orange'[:2] # returns 'or'
'banana'[::2] # returns 'bnn'
'orange'[:-2] # returns 'oran'
'university'[::2] # returns 'uiest'
'apple'[::-1] # returns 'elppa' (Shorthand for reversing a string)
```

### Practice:

Write a program that asks the user for a word of at least six letters in length, then prints back to the user on one line the first three letters, and then on a second line prints from the fourth letter to the end of the word.

Modify the program to print out every other letter.

Modify the program to print out the word spelled backward.

Strings have useful methods which are accessed with dot notation:

```
'apple'.upper() returns 'APPLE'
'apple'.title() returns 'Apple'
'México'.lower() returns 'méxico'
'banana'.islower() returns True
'Banana'.islower() returns False
'Banana'.isupper() returns False
'BANANA'.isupper() returns True
'México'.find('x') returns 2
'banana'.replace('a', 'x') returns 'bxnxnx'
'  orange '.strip() (flanking spaces, tabs, newlines) returns 'orange'
'banana'.count('a') returns 3
```

### Practice:
Write a program that asks the user for a word and prints back whether the word is a palindrome (that is, spelled the same way forward and backward, for example, "noon").

_Hint_: You'll need to force case to uppercase or lowercase to deal with capitalization. For the Python interpreter, "Hannah" is not a palindrome, but "HANNAH" and "hannah" are.

