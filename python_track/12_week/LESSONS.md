# Lesson 12.1: Topic modeling with LDA

[Lesson Video](https://www.youtube.com/watch?v=xr79xZ7nFGQ)

## Objective:

Students will extract topics from many texts with the [gensim](https://radimrehurek.com/gensim/) module.

Topic modeling is an unsupervised machine learning technique.
`Latent Dirichlet allocation (LDA)` ([see Wikipedia article](https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation)) is a popular algorithm for performing topic modeling.
"Unsupervised" means that no human input is needed for the algorithm to detect topics.
See [Tutorial "Beginner's Guide to Topic Modeling in Python"](https://www.analyticsvidhya.com/blog/2016/08/beginners-guide-to-topic-modeling-in-python/) and/or another one called ["Topic Modeling with Gensim"](https://towardsdatascience.com/topic-modeling-with-gensim-a5609cefccc), or this very detailed one: ["Topic Modeling with Gensim (Python)](https://www.machinelearningplus.com/nlp/topic-modeling-gensim-python/).

The last article explains a strategy to help you find the optimal number of topics to specify.

A quick and dirty way to choose number of topics is trial-and-error and visualizing results.

The [pyLDAvis](https://github.com/bmabey/pyLDAvis) module creates an interactive visualization as an HTML page.
See the original paper ["LDAvis: A method for visualizing and interpreting topics"](https://nlp.stanford.edu/events/illvi2014/papers/sievert-illvi2014.pdf) that gives details on this interactive visualization.

## Demo:

Using the code given in the tutorials linked to above, extract topics from a recent General Conference of the Church.

# Lesson 12.2 Document similarity with doc2vec

[Lesson Video](https://www.youtube.com/watch?v=eRTtKgdgL0o)

## Objective:

Students perform document similarity analysis.

Word embeddings are a way to place words into a multi-dimensional space.
Similar words are close to each other in that space, while dissimilar words are far from each other.

A demo with [fasttext](https://fasttext.cc/).

Documents can also be placed in a multi-dimensional space, and documents that are similar to each other are close to each other, while dissimilar documents are far from each other.

## Demo:

See a demo of a Google Colab [notebook tutorial of doc2vec](https://radimrehurek.com/gensim/auto_examples/tutorials/run_doc2vec_lee.html).

Note: You may [download this doc2vec notebook .ipynb file](https://radimrehurek.com/gensim/_downloads/e543440eddacbe89cee184b57e6ec27b/run_doc2vec_lee.ipynb) and upload it to your personal Google Colab account.

# Lesson 12.3: Fuzzy string matching

[Lesson Video](https://www.youtube.com/watch?v=wFoGtz-GTt4)

## Objective:

Students will use fuzzy matching to determine the similarity of strings based on edit distance.

What question does string1 == string2 answer?
Fuzzy matching allows users to answer the question: "How similar are two strings?"
The [Levenshtein Distance](https://en.wikipedia.org/wiki/Levenshtein_distance) algorithm is probably the most popular edit distance algorithm.
The Python module called [thefuzz](https://github.com/seatgeek/thefuzz) implements the Levenshtein Distance algorithm and provides seven useful functions.

Good [tutorial on thefuzz here](https://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/).
**Note**: thefuzz was previously called fuzzywuzzy.

Fuzzy matching is available in other programming languages, for example, in R, Julia, Java, and many others.

## Activity:

Take a couple minutes to run the example code for each of the following functions and, importantly, understand and explain to a neighbor what each function does. Find the following functions on [this github page for thefuzz](https://github.com/seatgeek/thefuzz#simple-ratio): `ratio()`, `partial_ratio()`, `token_sort_ratio()`, `token_set_ratio()`, `partial_token_sort_ratio()`.

Take a few minutes to understand what `extract()` and `extractOne()` do.

The default scorer is `WRatio()` [see its source code on this page](https://github.com/seatgeek/thefuzz/blob/master/thefuzz/fuzz.py#LC224) (search for 'def WRatio').

## Formative quiz with a partner:

Discuss what the five functions would return for the following pairs of two strings, and then check your guess by using the five functions:

```
string1 = "Linguistics is the study of language."
string2 = "Linguistics is the study of language!"
```

```
string1 = "Computers are awesome, like, sweet"
string2 = "Computers are awesome, like, sweet sweet"
```

```
"a b c d e f g h i j k l m n o p q r s t u v w x y z"
"z y x w v u t s r q p o n m l k j i h g f e d c b a"
```

```
"abcdefghijklmnopqrstuvwxyz"
"zyxwvutsrqponmlkjihgfedcba"
```

## Brainstorm:

What are possible applications of fuzzy string matching?
Spell-checkers, using a big list of words like the one here.
Joining two tables based on fuzzy matching of strings; see here.
FamilySearch hints of matches of the same person.
Search engine autocorrect search terms.
What else?

