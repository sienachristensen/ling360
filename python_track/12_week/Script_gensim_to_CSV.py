"""Toy example demonstrating one way to extract topic number and corresponding words from gensim output and write them horizontally to CSV"""
import re
results = [(0, '0.001*"book" + 0.002*"magazine"'), (1, '0.234*"banana" + 0.345*"apple"')]
with open("/Users/ekb5/Downloads/results.csv", mode="w") as fout:
    # Inside the loop write one row at a time to `fout`
    for result in results:
        output = str(result[0]+1) + ","
        # this regex matches the words inside double quotes (e.g. "word")
        words = re.findall(r'"([^"]+)"', result[1])

        # build up a csv row in the variable `output`
        for word in words:
            output += word + ","
        output = output[:-1]
        output += "\n"

        # write the row
        fout.write(output)
