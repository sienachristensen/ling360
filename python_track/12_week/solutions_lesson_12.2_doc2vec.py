"""
Solution file for 
Lesson 12.2 document similarity analysis
Most of this code was produced by ChatGPT
"""

# load the workhorses
from pathlib import Path
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize

# Change into the directory with the TXT files
dir_path = Path("/Users/ekb5/Documents/LING_360/gen_conf_2022_10")

# Get files as a list of Path objects
files = [file for file in dir_path.glob("*.txt")]

# Initialize a documents list
documents = []

# Loop through each file in the directory
for file in files:

    # Tokenize the file content
    tokens = word_tokenize(file.read_text(encoding="utf-8"))

    # Append the file content as a TaggedDocument to the documents list
    documents.append(TaggedDocument(tokens, [file.name]))

# Train the Doc2Vec model
model = Doc2Vec(documents, vector_size=100, window=5, min_count=1, workers=4)

# Get a specific document
file_path = Path("general_conference_talk_of_your_choice.txt")
file_text = file_path.read_text(encoding="utf8")

# Tokenize the document
tokens = word_tokenize(file_text)

# Infer a vector for the document
new_vector = model.infer_vector(tokens)

# Find the most similar documents to the document
similar_docs = model.dv.most_similar([new_vector], topn=5)

# Print the most similar documents
for doc in similar_docs:
    print(doc)
