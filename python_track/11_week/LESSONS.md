# Lesson 11.1: Webscraping

## Objective:

Students will webscrape the text and links out of webpages.

## Logic:

Have Python (using [the requests module](https://pypi.org/project/requests/)) act like a web browser and retrieve (aka. scrape) the source code of a webpage.

A "User-Agent" of an HTTP request specifies information about the requesting system (see [User-Agent](https://en.wikipedia.org/wiki/User_agent) Wikipedia article). Some webpages require a user-agent (e.g., [https://en.wikipedia.org/wiki/User_agent](https://en.wikipedia.org/wiki/User_agent) and [https://fullofwholesome.com/egg-scramble-with-spinach-feta-and-tomato/](https://fullofwholesome.com/egg-scramble-with-spinach-feta-and-tomato/)), so we need to specify one in our request (see [https://stackoverflow.com/q/61022535/2884875](https://stackoverflow.com/q/61022535/2884875)).

Use the Python module [jusText](https://github.com/miso-belica/jusText) to retrieve text and do something with it:
Print to screen;
Write out to the harddrive in a .txt file;
[Whatever else you might want to do with human-generated text].
Use the Python module [bs4 (BeautifulSoup)](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) to retrieve links.

## Clean up links:
- Remove empty strings;
- Remove duplicate links;
- Remove same-document links (that begin with "#");
- Create absolute links (i.e., links that start with "http") from relative links (i.e., links that start with "/").

## Practice:

### Create a program that webscrapes a blog of your choice and print the text to the screen.
- Hint1: Be sure to specify a common user-agent in your request; see [SO thread](https://stackoverflow.com/q/61022535/2884875).
- Hint2: The Python module jusText will be your friend here.
- Modify the previous program to save the text to a .txt file on your hard drive.
- Hint: File output is the name of the game here (review Lesson 5.1 "File I/O" if needed).
- Create a program that prints to screen the links on a webpage, that is, the URLs within the `href` attribute of anchor tags `<a>`.
- Hint: Choose your own adventure, whether with `BeautifulSoup` or with the Python module `lxml` and `xpath`.

### Modify the previous program to clean up the links by:

- removing empty strings in the list of links, if any;
- removing duplicate links, if any;
- The `set` data structure/type is the way to go here; passing a list to `set()` will eliminate duplicates.
- removing same-document links that start with "#", if any;
- creating absolute links from relative links (i.e., links that start with "/") by concatenating the domain with the relative path. Example of absolute link: https://www.r-bloggers.com/2021/03/workshop-31-03-21-bring-a-shiny-app-to-production/
- Hint: Look up `urllib.parse()`.

### Modify the previous program to randomly choose a few links from among the list of clean links, then retrieve and print to screen the text on those few webpages.

- Hint: The Python module `random`, and specifically the `shuffle()` function, will be helpful here.
- Word of caution: `random.shuffle()` modifies/mutates in place (it does not create a copy), so you should not save the list of links back to its same variable name.


# Lesson 11.2: Automating web browsers

## Note: The tools and skills taught in this lesson are not required for any homework, but you may use them on your final project if you so choose.

## Objective:

Students will automate the web browser Google Chrome with Selenium.

## [Selenium](https://www.seleniumhq.org/) automates web browsers.  It is comprised of different software components written in multiple languages.

- To use Selenium in Python you'll use a wrapper module called (you guessed it!) [selenium](http://selenium-python.readthedocs.io/) which lets you control Selenium.
- Automating a web browser, rather than webscraping the HTML code, is necessary in order to run javascript functions and get data that is returned by those javascript functions.

See [a very basic video tutorial](https://youtu.be/eDrFWRi13DY).

See [Available Downloads of ChromeDriver](https://sites.google.com/chromium.org/driver/downloads).

Highly important note: Make sure the version of ChromeDriver that you download has the same version number as your Google Chrome browser, otherwise you'll get an error message like "This version of ChromeDriver only supports Chrome version NN Current browser version is NN".

Implicit waits cause the driver to wait up to a certain number of seconds while trying to locate an element; [see here](https://selenium-python.readthedocs.io/waits.html?highlight=implicit#implicit-waits).

Elements on a webpage can be located in a number of ways: ID attribute, xpath, the text of a link, part of the text of a link, the name of an HTML tag, class attribute name, and by CSS selector; see [locating elements](https://selenium-python.readthedocs.io/locating-elements.html).

Note: Use the `driver.find_element()` and `driver.find_elements()` function, not the `driver.find_element_by_*()` functions, etc.

You can write text in a search box with the `.send_keys()` method of an element (after locating it), and you can click on a button or a link with the `.click()` method of an element.

## Practice:

- Write a Python script to open a Google Chrome window, navigate to the homepage of Youtube, wait five seconds and close the window.
- Modify your script to enter text in the search box at the top of the screen, click the search button (with magnifying glass), wait five seconds, and then close the window.
- Hint: You can see the HTML element of specific elements of a webpage by right-clicking and selecting "Inspect" (or similar).
- Modify your script to click on the first video on the page, wait five seconds, and then close the window.
- Hint: Inspect the title of the video, not the video thumbnail.
- Modify your script to scroll down a lot, so that (more) comments are loaded.
- Modify your script to scrape out the comments and print them to the Python console.

