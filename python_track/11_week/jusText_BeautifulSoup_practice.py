import requests
from bs4 import BeautifulSoup as bs
import justext
import urllib

# define url
url = "https://pypi.org/project/jusText/"
# define User-Agent header, you can Google how to find your browser's User-Agent if you can't find it
headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"}
# get website response
response = requests.get(url, headers=headers)

# scrape text using jusText
paragraphs = justext.justext(response.content, justext.get_stoplist("English"))
for paragraph in paragraphs:
    if not paragraph.is_boilerplate:
        print(paragraph.text)

# scrape for hyperlinks
soup = bs(response.content, 'html.parser')
anchors = soup.find_all("a")
links = []
for anchor in anchors:
    try:
        links.append(anchor.attrs['href'])
    except KeyError:
        print("no href")
# clean up link list
links = [l for l in links if len(l)>0]
links = set(links)
links = [l for l in links if l[0] != '#']
# define a function to complete relative links
def absolute_link(url, relative_link):
    parsed_url = urllib.parse.urlparse(url)
    return urllib.parse.urljoin(parsed_url.scheme + '://' + parsed_url.netloc, relative_link)
print(links)