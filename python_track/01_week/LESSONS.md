# Day 1 Introduction

1. Look at PyCharm together.
2. Source file vs. console
3. Challenge Level Versus Skill Level

https://alifeofproductivity.com/how-to-experience-flow-magical-chart/
https://uxdesign.cc/getting-into-the-flow-what-does-that-even-mean-58b16642ef1d

---

# Lesson 1.1: Types

[See Video Lesson](https://youtu.be/sZsqYB-IxW0)

**Objective**: <br>
Students will understand Python's basic (or primitive) type system and be capable of determining the type of a given value (with the type() function).

Problem-solving is a central skill to successfully creating a program; analogy of toolbox and palette of raw materials (lumber, nails, etc.) and its similarity to a programming language (the toolbox) a directory full of text files (the raw materials).

Values and types: int, float, str, bool

## Practice:

use the type() function to find out the type of the following sets of values:

- 4, -3, 6
- 4.5, -3.4, 6.27
- 'hello', 'world', 'hello world', 'BYU is where I study.'
- True, False (mind the capitalization or the lack thereof with each letter)


# Lesson 1.2: Script mode vs. REPL mode

[see Lesson Video](https://youtu.be/1oiK1FcZYHY)

**Objective**: <br>
Students will understand the difference between script mode and REPL (read-eval-print loop, interactive) mode.

Script mode vs. REPL (read-eval-print loop, in the console)

## Practice:

Open up a Python console (within PyCharm): View > Tool Windows > Python Console

Write a few simple arithmetic statements and press Enter (after each one), for example: 2 + 3, 76 – 20, 4 * 7, 56 / 8

Open a new Python file: (within PyCharm) In left-hand menu, right-click on the project name (e.g., "sandbox" if your project name is "sandbox") > New > Python file

write the same arithmetic statements from above, save the file, and run it (Run > Run… > [select file name and press Enter])

Put the arithmetic statements from above in the print() function and rerun your script (a.k.a., source file)

In a console window:

- run: 'hello' + 'world'
- run: 'hello' * 3

In either a console or a script file:

- run: print('hello' + 4)

Read the last line of the error message that the interpreter throws/prints

What kind of error occurred?

Look up the str() function to see how to convert an int, float, or bool to str

## Lesson 1.3: Variables

[see Lesson Video](https://youtu.be/uFXDG8mlj0g)

**Objective**:
Students will be capable of assigning values to variables.

### Variables:

**Formative quiz**:

Identify the well-formed variable names and discuss why the ill-formed variable names are ill-formed.

- first = 'Earl'
- 5kids = 'awesome'
- yourProfession = 'professor'
- your_name = 'Earl Brown'
- ca$h = 1000000
- class = 'text processing and analysis'
- houses2 = 'expensive'
- x = 2
- and = "What else can I tell you?"
- current_sentence = "How may I help you?"

## Order of operations

Like in math, parentheses have the highest precedence, then multiplication and division, then addition and subtraction:

What does 2 * 3 - 1  return? Why?

How about 2 * (3 - 1)?

## Practice:

What would each of the following statements return? Try them to verify your guesses.

- print('hello' + 'world' * 3)
- print(('hello' + 'world') * 3)

Create a program (in a script) that asks the user for his/her name with a prompt (look up the input() function), saves his/her name to a variable, and prints back to the user the message:

"Well, hello there [his/her name]!", replacing "[his/her name]" with his/her actual name.

_Bonus_: If your computer has Python 3.6 or higher (check in PyCharm: File > Settings > Project > Project interpreter), modify your program to use string interpolation. Check out this question on Stack Overflow: https://stackoverflow.com/questions/4450592/is-there-a-python-equivalent-to-rubys-string-interpolation

Find a (short) madlib online (possibly from madlibs.com itself) and create a program that asks the user for the nouns, adjectives, etc., that are needed to complete the story, and then print out the story once the user has supplied all the required fields. Change computers with a neighbor and run his/her madlib.

## Comments in scripts

Make the logic of computer code more easily understood by humans, whether other people who read your code, or your later-self when you look at code you previously wrote.
Either on the previous line or at the end of the line, after code
docstring for functions: three single or double quotes to begin and end a block of comments.

**Example**:

```
def my_function(a):
"""Here is a docstring comment"""
    return a
```

PyCharm has a keyboard shortcut (Ctrl + /) to add/remove comments to/from the current line or all highlighted lines.

## Practice:

Modify the program you previously wrote by putting in comments that describe what each line of your script does, and then rerun the program in order to verify that the interpreter didn't do anything with the comments.

## Possible solutions to in-class exercises

Create a program (in a script) that asks the user for his/her name with a prompt (look up the input() function), saves his/her name to a variable, and prints back to the user the message: "Well, hello there [his/her name]!", replacing "[his/her name]" with his/her actual name.


Find a (short) mad lib online (possibly from madlibs.com itself) and create a program that asks the user for the nouns, adjectives, etc., that are needed to complete the story, and then print out the story once the user has supplied all the required fields. Change computers with a neighbor and run his/her madlib.
first few sentences of [this madlib](http://www.madlibs.com/content/uploads/2016/04/VacationFun_ML_2009_pg15.pdf)


## Answer to Practice Exercise 1.3:

```
name = input("Hey, what's your name: ")

print('Well, hello there ' + name + '!')

# This second `print()` prints the same thing 
# using string interpolation with an f-string (formatted string)
print(f'Well, hello there {name}!')


adj1 = input("Please be so kind as to give me an adjective: ")
adj2 = input("And a second adjective please: ")
noun1 = input("How about a noun now: ")
noun2 = input("What about a second noun: ")
noun_plural1 = input("Now, how about a plural noun: ")
game = input("Can you give me the name of a game: ")
noun_plural2 = input("And how about a second plural noun: ")
verb1 = input("Please offer me a verb ending in 'ing': ")
verb2 = input("And a second verb with 'ing': ")

input("Ready to read the hilarity that is your madlib?! Press <Enter> to split your gut!")

# simply puts a space before the madlib is printed
print()

# This is a triple-quoted (''' or """) f-string which allows you to
enter it on multiple lines AND include (interpolate)
# variables inside it like this: {variable_name}
print(f'''
    A vacation is when you take a trip to some '{adj1}' place with your '{adj2}' family.
    Usually you go to some place that is near a/an '{noun1}' or up on a/an '{noun2}'.
    A good vacation place is one where you can ride '{noun_plural1}' or play '{game}'
    or go hunting for '{noun_plural2}'. I like to spend my time '{verb1}' or '{verb2}'.
    ''')
```