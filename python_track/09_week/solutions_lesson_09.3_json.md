# Possible solutions to in-class practice exercises
## Lesson 9.3 on parsing JSON-formatted data

**Note**: This code differs from the Youtube lesson video.

## Write a program to print to screen the id and text of the tweets
```
import json
from pathlib import Path
json_file = Path("tweets_queried_anonymous.json")
text = json_file.read_text(encoding="utf-8")

# json.loads() Converts the text into a list of Python Dictionaries
# (e.g. [{"id": 1, "text": "hello"}, {"id": 2, "text": "goodbye"}] )
loaded_tweets = json.loads(text)

for tweet in loaded_tweets:
    break
    print(f"id: {tweet['id']}\ntext: {tweet['text']}\n")
```

## Modify your program to create a frequency dictionary of lower case words.

```
import json
from pathlib import Path
json_file = Path("tweets_queried_anonymous.json")
text = json_file.read_text(encoding="utf-8")

loaded_tweets = json.loads(text)
freqs = {}  # empty dictionary to collect gender/freq items
for tweet in loaded_tweets:
    lower_case_text = tweet['text'].lower()
    words = [word for word in lower_case_text.split()]

    # look for word in the freqs dictionary and
    # add one to its current value or to 0 if
    # it is not yet in freqs
    for word in words:
        freqs[word] = freqs.get(word, 0) + 1

# json.dumps() with the indent=2 key word will
# convert and format the dictionary into a nice string
# so when we print it
# the output will not be on one line, but rather
# multiple lines and indented with 2 spaces
formatted_freqs = json.dumps(freqs, indent=2)

print(formatted_freqs)
```

