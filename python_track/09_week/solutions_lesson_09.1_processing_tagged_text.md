# Possible solutions to in-class exercises
## for Lesson 9.1
## about processing tagged text
'''

Download the Mini-CORE_tagd.zip file from LS > Content > Corpora and unzip it on your hard drive.
Write a program to print out to the screen all the adjective + noun pairs.

```
from pathlib import Path
import re
corpus_path = Path("./Mini-CORE_tagd")

for file_path in corpus_path.glob("*.txt"):
    with file_path.open(encoding="cp437") as fin:
        lines = fin.readlines()  # reads in lines and returns a list with each line in a separate element of the list
        lines = [i.strip() for i in lines]  # uses a list comprehension to strip off the newline break on the right side of each element
        lines = [i.split('_') for i in lines]  # uses a list comprehension to split up each word/POS tag pair on the underscore separating them
        for i in range(len(lines)):  # loop over indexes of lines
            try:
                if re.search(r'^JJ', lines[i][1]) and re.search(r"^NN", lines[i+1][1]):
                    print(f"{lines[i][0]} {lines[i+1][0]}")
            except IndexError:
                print("we apparently have a document-final adjective here")
```

Modify your program to calculate the frequency of the adjective-noun pairs and write to them a .csv file, ordered in descending order by frequency.

```
from pathlib import Path
import re

corpus_path = Path("./Mini-CORE_tagd")

freqs = {}  # creates an empty dictionary to collect the frequencies
for file_path in corpus_path.glob("*.txt"):
    with file_path.open(encoding="cp437") as fin:
        lines = fin.readlines()  # reads in lines and returns a list with each line in a separate element of the list
        lines = [i.strip() for i in lines]  # uses a list comprehension to strip off the newline break on the right side of each element
        lines = [i.split('_') for i in lines]  # uses a list comprehension to split up each word/POS tag pair on the underscore separating them
        for i in range(len(lines)):  # loop over indexes of lines
            try:
                if re.search(r'^JJ', lines[i][1]) and re.search(r"^NN", lines[i+1][1]):
                    adj = lines[i][0].upper()
                    noun = lines[i+1][0].upper()
                    adj_noun = adj + " " + noun
                    freqs[adj_noun] = freqs.get(adj_noun, 0) + 1
            except IndexError:
                print("we apparently have a document-final adjective here")

# creates a sorted list of the frequencies
freq_list = sorted(freqs.items(), key=lambda x:x[1], reverse=True)
#
# writes the sorted list to a .csv file
csv_path = Path(".freqs_adj_noun.csv")

## I use utf-8 encoding here to avoid an error I was getting
with csv_path.open(mode='w', encoding="utf-8") as fout:
    fout.write("adj_noun\tN\n")
    for k, v in freq_list:
        fout.write(f'{k}\t{v}\n')
```

Modify your program to write to a .csv file all the superlative adjective + non-proper plural noun pairs.

```
from pathlib import Path
import re
corpus_path = Path("./Mini-CORE_tagd")

freqs = {}  # creates an empty dictionary to collect the frequencies
for file_path in corpus_path.glob("*.txt"):
    with file_path.open(encoding="cp437") as fin:
        lines = fin.readlines()  # reads in lines and returns a list with each line in a separate element of the list
        lines = [i.strip() for i in lines]  # uses a list comprehension to strip off the newline break on the right side of each element
        lines = [i.split('_') for i in lines]  # uses a list comprehension to split up each word/POS tag pair on the underscore separating them
        for i in range(len(lines)):  # loop over indexes of lines
            try:
                if re.search(r'^JJS$', lines[i][1]) and re.search(r"^NNS$", lines[i+1][1]):  # mind the tags from the Penn Treebank tagset!
                    adj = lines[i][0].upper()
                    noun = lines[i+1][0].upper()
                    adj_noun = adj + " " + noun
                    freqs[adj_noun] = freqs.get(adj_noun, 0) + 1
            except IndexError:
                print("we apparently have a document-final adjective here")

# creates a sorted list of the frequencies
freq_list = sorted(freqs.items(), key=lambda x:x[1], reverse=True)

# writes the sorted list to a .csv file
csv_file = Path("./freqs_adj_noun.csv")
# I use utf-8 encoding here to avoid an error I was getting
with csv_file.open(mode='w', encoding="utf-8") as fout:
    fout.write("adj_noun\tN\n")
    for k, v in freq_list:
        fout.write(f'{k}\t{v}\n')
```
