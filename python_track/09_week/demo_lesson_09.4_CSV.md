# Possible solutions to in-class exercises
## Lesson 9.4 on parsing CSV files
## Demo of parsing text data in a specific column in a CSV file

```
# import two modules
from pathlib import Path
import pandas as pd


# read in the data in the CSV file and create a data frame
df = pd.read_csv("2022-12-31 GJL RMCL.csv")

# loop over the rows of the data frame
for index, row in df.iterrows():
    
    # print the data in two columns in the data frame
    print(row["start"], row["text"])
```
