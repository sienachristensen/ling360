# Lesson 9.1: Processing tagged text

[Lesson 9.1 Video](https://www.youtube.com/watch?v=22ANOFI12Q0)

## Objective:

Students will be capable of extracting information from text with part-of-speech tags.

## Logic:

The logic of the code will vary according to how the part-of-tags are organized.
In general, first you need to isolate tokens (words and punctuation) from their accompanying part-of-speech tags.
Then, you search for specific words, likely with regular expressions, that have certain POS tags, again, likely specified with regexes.

## Practice:

- Download the Mini-CORE_tagd.zip file from the CMS and unzip it on your hard drive.
- Write a program to print out to the screen all the adjective + noun pairs.
- **Hint**: It might be easiest to loop over the indexes of word/POS pairs within each file, so that you can check if an adjective is followed by a noun, and if so, print both words based on the indexes.
- **Hint2**: .readlines() is useful here.
- Modify your program to calculate the frequency of the adjective-noun pairs and write them to a .csv file, ordered in descending order by frequency.
- Modify your program to write to a .csv file all the superlative adjective + plural non-proper noun pairs.

# Lesson 9.2: parsing XML data

[Lesson 9.2 Video](https://www.youtube.com/watch?v=JWRzJF7Z0mQ)

## Objective:

Students will parse XML-formatted data with XPath syntax.

## XML (Extensible Markup Language)

A markup language for formatting data to be read by computers (and humans, to some extent).
Simple example here and slightly more complex one here, and one with attributes here.

## XPath

XPath is a mini-language, along the lines of regular expressions, as XPath allows users to extract certain data from an XML or HTML document with specialized syntax.
See tutorial here and cheatsheet here.

## Basic usage:
- '/breakfast_menu'  selects  "breakfast_menu" if it's the root element
- '/breakfast_menu/food'  selects all "food" elements that are children of the root element "breakfast_menu"
- '//food'  selects all "food" elements anywhere in the document
- '//food/price'  selects all "price" elements that are children of "food" elements
- '//food/price[@range]'  selects all "price" elements that have a "range" attribute
- '//food/price[@range="cheap"]'  selects all "price" elements that have a "range" attribute with a value of "cheap"
- '//food[contains(price, "7")]'  selects all "food" elements with a "price" child element whose text contains the character "7"
- '//food[starts-with(price, "$7")]/name'  selects all the "name" elements that are children of "food" elements that have a "price" child element whose text starts with the two characters "$7"
- '//food[contains(name, "Waffles")]/name'  selects all the "name" elements whose text contain the characters "Waffles"
- '//food/name[following-sibling::price[@range="cheap"]]'  selects all "name" elements that have a following sibling of "price", which has an attribute of "range" with a value of "cheap"
- '//food/name[following-sibling::price[@range="cheap"] and following-sibling::description[@summary = "delicious"]]'  selects all "name" elements that have a following siblings elements of "price" and "description", each with an attribute with a value

## Logic/sample script:

```
from lxml import etree
tree = etree.parse("pathway_to_xml_file_here")
nodes = tree.xpath("xpath_here")
for i in nodes:
for j in i.itertext():
print(j)
```

## Practice:

- Download the sample .xml file from the British National Corpus entitled "AHS.xml", available in the CMS.
- Open the .xml in a web browser (so that it will pretty-print) and visually inspect the tags that fall within the "s" tags. What do you think the "s", "w" and "c" tags stand for? What do you think the "c5", "hw", and "pos" attributes of "w" tags stand for? Verify with the BNC XML documentation here.
- Write a program to print to screen the text within the sentences of the file.
- Modify your script so that the words are printed horizontally to the right rather than vertically.
- **Hint**: Look at the documentation for the end= parameter of the print() function here.
- Modify your script to print to screen all the (unmarked) adjectives.
- **Hint**: The CLAWS 5 tagset here will be helpful.
- Modify your script to print to screen all the (unmarked) adjectives that are followed by a common singular noun anywhere in the sentence.
- Modify your script to print to screen all the (unmarked) adjectives that are followed by a common singular noun that is the first word after the adjective.
- Modify your script to print to screen any adjective followed by any noun that is the first word after the adjective.
- Modify your script to print to screen all the text in sentences with any adjective followed by any noun that is the first word after the adjective.

# Lesson 9.3: Parsing JSON data

[Lesson 9.3 Video](https://youtu.be/cDDkBHs6jxA)

## Objective:

Students will parse JSON-formatted data.

## JSON

JSON (Javascript Object Notation) is a data format that is commonly used for sending information from a web server to a client, that is, a person using a computer or a cell phone to access a website or a computer program intended to retrieve lots of data.
A JSON object is very similar in format to a Python dictionary.

See example on wikipedia and here.

Read more about JSON here:
https://www.json.org/
https://en.wikipedia.org/wiki/JSON

## Logic to parse JSON:

- `import json`
- Load .json file with .load() function or convert a JSON-formatted string to a Python dictionary with .loads() function (mind the "s"!).
- Iterate over the Python dictionary (which may have a list in it), pulling out values with their corresponding keys.

## Practice:

- Download the following file to your hard drive: https://raw.githubusercontent.com/sitepoint-editors/json-examples/master/src/db.json
- Write a program to print to screen the gender of the clients.
- Modify your program to create a frequency dictionary of those genders.
- Modify your program to calculate the mean age of the clients.
- Modify your program to calculate the mean age by gender, that is, one mean age for the women and another mean age for the men.
- Download the file tweets_queried_anonymous.json from the CMS. Print to the console the text of each tweet.
- **Hint**: The .load() function won't work with this file because the tweets are each on their own line. Instead, loop over the file line by line and use the .loads() function.

# Lesson 9.4: Parsing CSV files with pandas

[Lesson 9.4 Video](https://youtu.be/1Rp1_juOUc0)

## Objective:

Students will read data stored in a CSV file into their Python script.

## CSV Files

CSV: Comma-Separated Values files are tabular datasets (think spreadsheet tables), with columns and rows.
The Python module pandas provides a tabular data structure called a "data frame".

Data can be read in from a CSV file or an Excel file.

## Importing the pandas module

- Read in the data in the CSV (or Excel) file
- Loop over the rows
- Within the body of the for loop, access the columns you need

## Demo:

Instructor shows how to read in the data from a CSV file and pull out text from a specific column. 
The CSV file was created by the Whisper Automated Speech Recognition (ASR) system on the audio of this
interview here.
