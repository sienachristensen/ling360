# Possible solutions to in-class practice exercises
## Lesson 9.2 on parsing XML data

Write a program to print to screen the text within the sentences of the file.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s")
for i in nodes:
    for j in i.itertext():
        print(j)
```

Modify your script so that the words are printed horizontally to the right
rather than vertically.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s")
for i in nodes:
    for j in i.itertext():
        print(j, end="")  # print words horizontally rather than vertically
    print("\n")  # add newline between sentences
print(f"There were {len(nodes)} hits.")  # this is a "formatted string", which is only available in Python 3.6+
```

Modify your script to print to screen all the (unmarked) adjectives.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s/w[@c5='AJ0']")
for i in nodes:
    for j in i.itertext():
        print(j, end="")
    print()  # puts words on new lines
print(f"There were {len(nodes)} hits.")  # this is a "formatted string", which is only available in Python 3.6+
```


Modify your script to print to screen all the (unmarked) adjectives that are
followed by a common singular noun anywhere in the sentence.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s/w[@c5='AJ0' and following-sibling::w[@c5='NN1']]")
for i in nodes:
    for j in i.itertext():
        print(j, end="")
    print()  # puts words on new lines
print(f"There were {len(nodes)} hits.")
```


Modify your script to print to screen all the (unmarked) adjectives that
are followed by a common singular noun that is the first word after the adjective.
```
from lxml import etree
tree = etree.parse("AHS.xml")

nodes = tree.xpath("//s/w[@c5='AJ0' and following-sibling::w[1][@c5='NN1']]")  # mind the "[1]"
for i in nodes:
    for j in i.itertext():
        print(j, end="")
    print()  # puts words on new lines
print(f"There were {len(nodes)} hits.")
```


Modify your script to print to screen any adjective followed by any noun that
is the first word after the adjective.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s/w[starts-with(@c5, 'AJ') and following-sibling::w[1][starts-with(@c5, 'NN')]]")
for i in nodes:
    for j in i.itertext():
        print(j, end="")
    print()  # puts words on new lines
print(f"There were {len(nodes)} hits.")  # this is a "formatted string", which is only available in Python 3.6+
```


Modify your script to print to screen all the sentences with any adjective followed
by any noun that is the first word after the adjective.
```
from lxml import etree
tree = etree.parse("AHS.xml")
nodes = tree.xpath("//s[w[starts-with(@c5, 'AJ') and following-sibling::w[1][starts-with(@c5, 'NN')]]]")
for i in nodes:
    for j in i.itertext():
        print(j, end="")
    print("\n")  # puts words on new lines
print(f"There were {len(nodes)} hits.")  # this is a "formatted string", which is only available in Python 3.6+
```
