def main():
    """
    This script splits the text below on spaces into a list.  Then it
    loops over this list, adding each item/word to a dictionary with
    the value 1 or 1 + its current value and
    afterwards printing all the dictionary--the word counts.  Note
    that it does not clean up punctuation nor remove stop words.
    """

    text = """
    The grizzly bear (Ursus arctos horribilis), also known as the North American brown bear or simply grizzly, is a population or subspecies[2] of the brown bear inhabiting North America.

    In addition to the mainland grizzly (Ursus arctos horribilis), other morphological forms of
    brown bear in North America are sometimes identified as grizzly bears. These include three
    living populations—the Kodiak bear (U. a. middendorffi), the Kamchatka bear (U. a. beringianus),
    and the peninsular grizzly (U. a. gyas)—as well as the extinct California grizzly (U. a.
    californicus†),[3][4] Mexican grizzly (formerly U. a. nelsoni†), and Ungava-Labrador grizzly
    (formerly U. a. ungavaesis†).[5][6] On average, grizzly bears near the coast tend to be larger
    while inland grizzlies tend to be smaller. 
    """

    words = text.split()

    # Initialize the dictionary to gather counts

    word_counts = {}

    for word in words:
        # the .get() method has a second option argument that
        # will be the return value of .get() if the key
        # is not in the dictionary
        word_counts[word] = word_counts.get(word, 0) + 1

    print(word_counts)


if __name__ == "__main__":
    main()