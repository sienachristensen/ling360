def main():
    """
    Creates a dictionary and sorts it by the integer or count
    of each word from greatest to least
    """
    my_dictionary = {
        'apple': 1,
        'aardvark': 3,
        'dog': 2,
        'cat': 5,
        'ostrich': 4,
    }

    # the .items() method turns a dictionary into a list of tuples like this:
    # [('apple', 1), ('dog', 2), ... etc. ]
    # sorted returns the list sorted by the return value of the function
    # we refer to with `key=`.  Finally, you can reverse the sort order
    # with `reverse=True`
    my_list = sorted(my_dictionary.items(), key=sort_by, reverse=True)


    # print my list of tuples sorted by integers (counts) from greatest to least
    print(my_list)


# This function needs to return the value that we
# want to sort by.  In our case, each `x` is a tuple
# (see the .items() method above) and we want to sort by
# the second ([1]) thing in each tuple (the integer or count)
def sort_by(x):
    return x[1]


if __name__ == '__main__':
    # starts the main() function
    main()
