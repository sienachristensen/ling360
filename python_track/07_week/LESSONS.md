# Lesson 7.1: dictionaries
[see video lesson here](https://www.youtube.com/watch?v=dNDwOyTo4VE&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=18)

**Objective**:
Students will create Python dictionaries with key-value pairs.

[Think Python, chapter 11: dictionaries](https://greenteapress.com/thinkpython2/html/thinkpython2012.html)
Made up of key-value pairs, called items.
Analogy from coursera Python data structures course of a purse `(dict)` with belongings `(value)` with a sticky note `(key)` on them.
Watch an informative video about Python dictionaries from Coursera [here](https://www.youtube.com/watch?v=wwq-3i61RAY) (you don't have to join the course to watch the video).

Can be created with curly braces or with the function `dict()`
`dict_name = {}`
`dict_name = dict()`
Several useful dictionary methods:
`.keys()` returns an iterator with the keys of each item
`.values()`  same, but with the values
`.items()`  returns a `dict_items object`, which is like a list, with 2-element tuples (see next section)
Add items to an existing dictionary with: `dict_name['key'] = 'value'`, where `dict_name` is the variable you assigned your dictionary to,  and key and value have your actual data.
See thread on SO [here]().

## Practice:
1. Create an empty dictionary and then add words as keys and definitions of those words as values.
2. Iterate over the dictionary and print to screen the keys. Iterate again, this time printing the values. Now finally, print the key, a colon, and then the value.

# Lesson 7.2: Tuples
[see video lesson here](https://www.youtube.com/watch?v=IoLNic33fLw&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=45)

**Objective:**
Students will be capable of converting dictionaries to lists with tuples.

[Think Python, chapter 12: tuples](https://greenteapress.com/thinkpython2/html/thinkpython2013.html)
Like lists, but are immutable, that is, they cannot be changed after they are created.
By convention and, sometimes, by necessity, they are written with parentheses:
`t = ('a', 'b', 'c', 'd', 'e')`
dictionaries can be converted into dict_items objects (much like lists) with 2-element tuples with the `.items()` dictionary method.

## Practice:

1. Use the dictionary method `.items()` to create a `dict_items` object of 2-element tuples from your dictionary from above. Iterate over the dict_items and write to a `.csv` file each word-definition pair, with the words in one column and their definitions in the next column.

# Lesson 7.3: Lambda functions
[see video lesson here](https://www.youtube.com/watch?v=qtylA6oGf_w&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=34)

**Objective:**
Students will be capable of sorting lists with tuples by using lambda functions.

Sorting dictionaries and lists with lambda functions
The function `sorted()` and the list method `list_name.sort()` have an argument key that specifies a function to be applied to each element while sorting. The default is the first item/element/character.

Lambda function are "anonymous" functions because you don't have to give them a name and because they are usually immediately thrown away after use. They are often used with sorting.

So, while you could supply a function created with the def keyword, lambda functions are usually used as the function supplied to the key argument of `sorted()`.

In order to sort a dictionary, you must first create a list with 2-element tuples from the dictionary.

**Example:**
```
# creates dictionary
fam_dict = {'Lydia': 15, 'Earl': 45, 'Eve': 11, 'Kristi': 45, 'Esther': 16, 'Hannah': 12, 'Seth': 19}

# returns dict_items object (like a list) with tuples
fam_list = fam_dict.items()

# uses a lambda function to sort by the second element of each tuple (mind the zero-basedness of Python!)
print(sorted(fam_list, key=lambda x:x[1]))

		The following two definitions create the same function:

# normal way of defining a function
def second_element(x):
return x[1]

		# the lambda way (which is weird to name it)
		second_element = lambda x:x[1]
```

## Practice:
1. Create a dictionary with the ages of your family. Convert it to a list with tuples with the list() function, and print to screen.
2. Sort the list by names and print to screen. Sort again, this time by age, and print to screen.
3. Sort the list by age in reverse order, and write to a .csv file.

Hint: Look up the reverse argument of the function sorted().

# Lesson 7.4: Word frequency dictionaries
[see video lesson here](https://www.youtube.com/watch?v=PAelxJ-BJg4&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=11)

**Objective:**
Students will be capable of creating word frequency lists using dictionaries.

Six different (but similar under the hood) ways to create word frequency lists:
`if...else`
See example in Section 11.2 of textbook.
`try...except`
See example on SO here.
dictionary method `.get()`
See example on SO here.
`defaultdict()` function in module collections
See example in Section 19.7 of textbook and on SO here.
`Counter()` function in module collections
See example in Section 19.6 of textbook and on SO here.
`FreqDist()` function and `.most_common()` method from `nltk` library
See example in Section 3.1 of NLTK book.

**Formative (review) quiz:**
In a previous lesson [(Lesson 5.1 File I/0)](https://gitlab.com/flash4syth/ling360/-/blob/main/python_track/05_week/LESSONS.md) we learned how to read text saved in a file into a Python program. In an effort to review that material, choose the best way to accomplish the desired outcome in each scenario. You can assume that a file connection object is held by the variable infile. While there are more than two ways to read in text from a file (review Lesson 5.1), only two options are given here because they are reasonable ways to accomplish the tasks given below.

**Options:**

`infile.read()`

`for line in infile:`

**Tasks:**
Read in each line (i.e., hard return / newline break) in the file, one at a time.
Read in all text of the file as a single string.
You want to get frequencies of words in a file (and the file is small enough to fit in memory).
You have a massive file (say, 50 GB) that won't fit in the working memory of your computer, and you want to get frequencies of words.

## Practice:
1. Write a few sentences about your favorite restaurant and/or food. Be sure to repeat at least a few words, in order to double check that your word frequency list works correctly.
2. Using the `if...else technique`, create a word frequency list of the words in your few sentences and print the dictionary to the screen.
Hint: Be sure to deal with capitalization by converting all words to either uppercase or lowercase.
3. Modify your program to use try...except, then the dictionary method `.get()`, then the `defaultdict()` function in the module collections, and then the `Counter()` function in the module collections, and finally, the `FreqDist()` function from the nltk library.
4. Modify your program to sort the dictionary (after converting it to a list) in reverse (descending) order based on the frequencies (values).
5. Read docs on sorting lists at http://www.python.org.
6. Modify your program to create a word frequency list of a .txt file of your choice (possibly from http://www.gutenberg.org/).
7. Modify your program to create a word frequency list of several `.txt` files.
8. Modify your program to write the word frequency list to a `.csv` file, again in descending order based on the frequencies.

Bonus: Modify your program to first sort in descending order by frequency, and then, secondly, in ascending order by word, so that words with the same frequency are arranged in alphabetical order. The first comment on the answer of this Stack Overflow thread will be useful (but you'll need to modify the code a bit in order to get both the key and the value, not just the key): https://stackoverflow.com/questions/9919342/sorting-a-dictionary-by-value-then-key

Modify your program to exclude the words in the following list: `stop_list = ['the', 'a', 'an', 'of', 'from', 'with', 'to', 'and']`
