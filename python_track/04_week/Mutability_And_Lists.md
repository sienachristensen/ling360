In class we talked about the concept of immutability and mutability.  In Python the following types of values are immutable, meaning they cannot be changed once they are created and assigned to a variable:

- str (string)
- int (integer)
- float (decimal point number)
- bool (boolean)

In practice what that means is that they do not have any methods (functions attached to them) that will mutate their value.

Python also has mutable value types.  So far we have only looked at one data or value type that is mutable which is a list.  In the following code I will demonstrate how a list is mutable and how that can lead to confusion in your code AND how to avoid that confusion.

Immutability is often associated with **functional programming** which I would describe as a style of writing computer programs.  Python in contrast supports mostly an **object-oriented programming** style; however, it also has some support to write in a functional programming style.  These different **styles** of programming are also called **paradigms**.

Other languages support the functional programming paradigm more than Python such as Elixir and Rust.

Why are we talking about this?  Why would you want to write in a functional paradigm/style?  Well, it is often the case that a functional style is easier to understand and easier to debug (find the errors).  Immutability is often associated with the functional programming paradigm, but is not exclusive to functional programming.

**Note**: All common programming languages have functions, but functional languages place more
emphasis on using functions while object-oriented languages place more emphasis on objects.

Functional languages typically do not have objects.  We won't go into further explanation here.

```
def main():
    """Some code that demonstrates mutable and immutable values stored in variables"""

    # Creates a list (a mutable/changeable type of data)
    short_list = [1,2,3]

    print("Example of mutation: ")
    print(f"short_list Before mutation: {short_list}")

    # Below we define (def) a function called make_longer
    # def make_longer(x, element_to_add)
    # when I call it here
    # x = short_list and element_to_add = 4
    make_longer(short_list, 4)

    print(f"short_list After mutation:  {short_list}\n\n")

    # Let's show a mixed example
    list_of_animals = ["cow", "sheep", "horse"]
    print(f"list_of_animals before: {list_of_animals}")

    # Pretend that I forgot about this function call (activation)
    make_longer(list_of_animals, "dog")

    # Because I forgot the above (mutating) function call the result
    # of this next line is surprising

    new_list_of_animals = functional_style_make_longer(list_of_animals, "cat")

    print("""
          The resulting list has an unexpected dog added to the list because we called
          make_longer(list_of_animals, "dog") on the list of animals, forgetting
          that that will mutate the list by adding "dog" to it.  What we wanted was 
          just ["cow", "sheep", "horse", "cat"]
          """)

    print(f"what we got was: {new_list_of_animals}\n\n")
    print("So passing a list to a function allows the function to change the same (mutable) list;\n"
          " whereas doing the same with a string or other immutable type will not lead to\n"
          "any such surprises.")


    ####################################################################
    # An immutable example, considered an aspect of a functional style #
    ####################################################################
    print("An example of not mutating the list:")
    list_a = [4,5,6]

    print(f"list_a Before: {list_a}")

    # call a functional style function that DOES NOT mutate list_a
    # but makes a copy of it (see function's definition)
    list_b = functional_style_make_longer(list_a, 4)

    print(f"list_a After:  {list_a}.  It was NOT mutated")
    print(f"list_b:        {list_b}.  list_b is a new, different list from list a.\n\n")

    # Stop the program!
    # exit(0)

    ##########################################
    # Processing strings (an immutable type) #
    ##########################################

    message = "Hello"

    print(f"message before: '{message}'")

    message_copy = message

    message_copy = message_copy + " Susy!"

    print(f"message after: '{message}', it did not change.")
    print(f"message_copy was not changed, but a new string was created: '{message_copy}'\n\n")

    # That last example may seem like what you would expect when setting one variable equal
    # to another as in the line: `message_copy = message`.  You get an independent copy.
    # This is not so with lists.  See a near equivalent example using lists:

    print("Similar example as the last, but with lists (Warning Mutable!):")

    list_c = ["a", "b", "c"]

    print(f"list_c before: {list_c}")

    list_d = list_c

    # Append the letter 'd' to list_d only (or at least that's what you may think)
    list_d.append("d")

    print(f"After adding another letter to list_d, list_c is: {list_c}")
    print(f"After adding another letter to list_d, list_d is: {list_d}\n\n")



    # Stop!
    #exit(0)

    # Bonus on joining a list of strings to create a new string
    print("Mutate a list of strings. The strings are not being mutated but rather\n"
          "their list container.\n")

    greeting = ["Hello"]

    # The long way, using list mutation
    make_longer(greeting, "Bob")
    make_longer(greeting, "Jill")
    make_longer(greeting, "John")
    make_longer(greeting, "Jane")

    # Use the join method of the `" "` string to
    # join a list of strings with a space between each
    greetings_to_all = " ".join(greeting)

    print("Our result: \n")
    print(greetings_to_all + "\n\n")

    # A lot more concise
    print("A more concise example using a `for .. in` loop (loops repeat)")
    fresh_greeting = ["Hey!"]
    for name in ["Bob", "Jill", "John", "Jane"]:
        make_longer(fresh_greeting, name)

    # join a list of strings as above
    final_greeting = " ".join(fresh_greeting)

    print(f"My final greeting:\n {final_greeting}")


def make_longer(x, element_to_add):
    """
    The .append() method mutates the same list that it is called on.
    This can make the code hard to trouble shoot if you forget that
    you made a call to `make_longer` which will change your list!
    """
    # While .append() does mutate the list, its return value is None
    # So don't try this: `result = x.append(element_to_add)`
    # because `result` will equal `None` after
    x.append(element_to_add)

    # An implicit `return None` since we didn't specify a return type
    # return None



def functional_style_make_longer(x, element):
    """
    I call this functional style because it does not mutate
    the list stored in 'x', but rather makes a copy of it,
    creating a new independent list
    """
    # Use indexing syntax to copy the entire list--[:]
    copy_of_x = x[:]

    # we now can mutate the copy_of_x safely
    # avoiding mutating `x` (the original list we had)
    copy_of_x.append(element)

    return copy_of_x


if __name__ == '__main__':
    main()
```

The moral of the story is that if you want to make your life writing code more enjoyable, try adding some immutability to your code, so you save some future pain trying to fix unexpected problems.  Happy coding!
