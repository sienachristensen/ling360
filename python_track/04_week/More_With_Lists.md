```
import re
# Me to ChatGPT:
# Write me a poem in the style of Emily Dickinson:
poem = """
In Provo's quiet embrace, where mountains touch the sky,
A linguist's heart, with code entwined, begins to fly.
In Elixir's dance, and PostgreSQL's embrace,
The professor weaves a tale of language, time, and space.

Graphs unfold like whispered words, in Neo4j's domain,
A master of the linguist's craft, where wisdom finds its reign.
From BYU's halls to ventures bold, a journey shaped by code,
In Emily's spirit, truths unfold, in every line and ode.
"""

# List created in the global scope (outside of any functions)
# This may not be the best practice, especially if you mutate
# The list
global_list = [9, 2, 16, 4]

def main():
    """The definition of main, don't forget to call (execute) it below"""
    
    # global_list.sort()
    #
    # print(global_list)


    # message = "immutable string"
    #
    # print(message.upper())


    # message = say_hello("Bob")
    #
    # print(message)

    # my_list = [9, 2, 16, 4]
    # new_list = sort_list(my_list)
    #
    # print(new_list)


    # Split original poem text above into a list of words: ["word1", "word2", etc...]
    word_list = poem.split()

    ##############################################################################################
    ## Three ways to make a list of all the words in `word_list` that have a capital first letter
    ##############################################################################################
    # You can combine variations of these
    
    # First: A Regular for loop
    ###########################
    # requires creating a starting empty list
    new_list = []
    for word in word_list:
        # Only append if the first letter `word[0]` is in A through Z
        if re.search(r'[A-Z]', word[0]):
            new_list.append(word)


    # print(f"our new_list: {new_list}")
    
    
    # Second: A List Comprehension with a regular expression
    ######################################################
    
    # Here parts of a list comprehension with lots of space added to label them below:
    # [ x                   for   x                   in   some_sequence_type if true_or_false_expression ]
    #  returned expression        variable name            sequence of things  optional `if` filter
    # don't forget to wrap it all in a pair of []'s
    
    # Here we have our list comprehension
    new_list = [word for word in word_list if re.search(r'[A-Z]', word[0]) ]

    # print(f"our new_list: {new_list}")
    
    
    # Third: Using the built-in filter() function
    ######################################################
    
    # filter() takes 2 arguments:  First a function that will return a True/False (or something that can be
    # made True or False with bool(), and Second the sequence (a list in this case) that we want to filter
    # Note: You must not add () to the end of the function, as filter just needs the name
    # It will take care of calling (executing) the function for each word of the list
    new_list = list(filter(is_first_letter_capitalized, word_list))
    
    # We wrapped it in list() because filter() returns a filter object, the filtered data is in there,
    # but we have to extract it which list() will do for us

    # print(f"our new_list: {new_list}")


# Access first letter then check if it's upper
def is_first_letter_capitalized(word):
    return word[0].isupper()

def say_hello(name):
    # say_hello has access to the `global_list` since it was created outside of any function
    # but this a confusing way to write his code because you have to hunt down for the global_list,
    # better to create and use it close together when possible
    # global_list.sort()

    # Use an f-string to insert name like this: `{name}`
    print(f"Hello {name}!")


def sort_list(some_list):
    return sorted(some_list)



# execute the main function, if you forget this you are just defining
# functions but your code won't actually use them
main()
```