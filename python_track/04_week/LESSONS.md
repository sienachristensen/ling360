# Lesson 4.1: functions

[see Video Lesson](https://www.youtube.com/watch?v=tHDgXMiDShs&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=36)

**Objective**:
- Students will define their own functions, often referred to as "user-defined" functions.

[Think Python, chapter 3: Functions](https://greenteapress.com/thinkpython2/html/thinkpython2004.html)

_Hint_: Mind the colon and indentation!

**Example function 1**:

```
def uppercase_vowels(input_string):
"""Takes as input a string and returns as output a string with the vowels converted to uppercase."""
output_string = ""
for character in input_string:
    if character in 'aeiou':
        # Here are two ways to do the same thing--append a character to the end of a string
        output_string += character.upper()
        #output_string = output_string + character.upper()
    else:
        output_string += character
        
    return output_string
```

**Example function 2**:

```
def dash_vowels(x):
"""Takes as input a string and returns as output a string with vowels replaced with dashes."""
output = ""

for i in x:
    if i in 'aeiou':
        output += '-'
    else:
        output += i
        
return output
```

## Practice:

Define a function that takes as input two strings and returns as output a Boolean value indicating whether the two strings are anagrams of each other (that is, the letters of one word can be reordered to spell the other word, like with "secure" and "rescue").

_Hint1_: sorted(list_name)or list_name.sort() will be your friend here.

_Hint2_: You should not ask the user for two strings with input(), rather you should include two arguments in your function, one for each word.

_Hint3_: Be sure to account for (possible differences in) capitalization.

**Bonus**: Modify your function so that it throws an error message if the user doesn't supply two strings as arguments to the function call and/or if the user doesn't supply values of type str.

## Other practice exercises:

- Whether the letters of a word are in alphabetical order.
- Whether the vowels of a word are in alphabetical order.
- Whether a word has all five (orthographic) vowels, like "sequoia".
- Whether a shorter word appears in a longer word, in the same order, like "out" in "counter".

## Solutions to Lesson 4.1 Exercises

**Note**: You will learn better if you try your best on your own before looking at these!

```
"""
Possible solutions to in-class practice exercises
for Lesson 4.1
on functions
"""

# Define a function that takes as input two strings and returns as output a Boolean value indicating whether the two strings are anagrams of each other (that is, the letters of one word can be reordered to spell the other word, like with "secure" and "rescue").
# Bonus: Modify your function so that it throws an error message if the user doesn't supply two strings as arguments to the function call and/or if the user doesn't supply values of type str.
def is_anagram(wd1, wd2):
    """
    Takes as input two strings and returns as output a Boolean value indicating whether the two strings are anagrams of each other.
    """
    
    # Ensure you received two strings as arguments
    if type(wd1) != str or type(wd2) != str:
        raise TypeError("You need to give strings. Please try again.")
        
    # Empty string evaluate to False, so we need to write `not wd1` to
    # have it evaluate to True when wd1 == '', then the same for wd2
    # Thus if both are equal to '' the raise ValueError will be executed.
    if not wd1 and not wd2:
        raise ValueError("Strings must not be empty! Please try again.")
        
    # Ensure we are comparing all lower case letters
    wd1 = wd1.lower()
    wd2 = wd2.lower()
    
    # After `sorted(word)` these words/strings will be a list of characters
    # like ['a', 'b', 'c']
    wd1_signature = sorted(wd1)
    wd2_signature = sorted(wd2)
    
    if wd1_signature == wd2_signature:
        return True
    else:
        return False

# tests out the anagram function
print("-- testing anagram function --")
print(is_anagram("secure", "rescue"))  # returns True
print(is_anagram("book", "libro"))  # returns False

# Possible other functions:
# Whether the letters of a word are in alphabetical order.
def is_alphabetical_order(word):
    """
    Takes as input a string and returns as output a Boolean value indicating whether the characters of the string are in alphabetical order
    """
    word_signature = sorted(word)
    if list(word) == word_signature:
        return True
    else:
        return False

# tests out the alphabetical order function
print("-- testing alphabetical order function --")
print(is_alphabetical_order("act"))  # return True
print(is_alphabetical_order("tree"))  # return False

# Whether the vowels of a word are in alphabetical order.
def is_vowel_alphabetical_order(word):
    """
    Takes as input a string and returns as output a Boolean value indicating whether the vowels of the string are in alphabetical order
    """
    import re
    # deletes non-vowel characters
    vowels = re.sub(r"[^aeiou]", "", word, flags = re.I)  
    vowels_signature = sorted(list(vowels))
    if list(vowels) == vowels_signature:
        return True
    else:
        return False

# tests out the vowel alphabetical order function
print("-- testing vowel in alphabetical order function --")
print(is_vowel_alphabetical_order("Cameron"))  # returns True
print(is_vowel_alphabetical_order("Juanito"))  # returns False


# Whether a word has all five (orthographic) vowels, like "sequoia".
# Solution A
def has_five_vowels(word):

    if 'a' in word:
        if 'e' in word:
            if 'i' in word:
                if 'o' in word:
                    if 'u' in word:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False

# tests out five vowel function
print("-- testing five vowels function --")
print(has_five_vowels("sequoia"))  # returns True
print(has_five_vowels("university"))  # returns False

# Solution B
def has_five_vowels(word):
    if 'a' in word and 'e' in word and 'i' in word and 'o' in word and 'u' in word:
        return True
    else:
        return False

# tests out five vowel function
print("-- testing five vowels function --")
print(has_five_vowels("sequioa"))  # returns True
print(has_five_vowels("university"))  # returns False



# Whether a shorter word appears in a longer word, in the same order, like "out" is in "counter".
def short_in_long(short, long):
    short = short.lower()
    long = long.lower()
    
    # this is could also be written as `for indexes = list()`
    indexes = []  
    
    for letter in short:
        try:
            cur_index = long.index(letter)
        except:
            return False
        # this is shorthand for indexes.append(cur_index)
        # It works differently for lists than for strings
        indexes += [cur_index]  
    if sorted(indexes) == indexes:
        return True
    else:
        return False

print("-- testing short in long function --")
print(short_in_long("out", "counter"))  # returns True
print(short_in_long("out", "university"))  # returns False
```

# Lesson 4.2: Filtering lists

[see Video Lesson](https://www.youtube.com/watch?v=649RTK6DbNA&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=52)

**Objectives**:
- Students will be capable of filtering lists with a conditional statement in a for loop.
- Students will be capable of filtering lists with a conditional statement in a list comprehension.

Filtering lists in a for loop

## Practice:

- Create an empty list, often referred to as an "accumulator" or "collector", then
traverse (or loop over) a list that already has elements in it.
In each iteration, determine if the current element evaluates to True in a conditional statement
if True, append the element to the collector list; if False, move on to the next iteration

- Using the technique of filtering lists within a for loop, define a function that takes as input a string and returns as output a list of repeated words, if any, in the input string. If there are no repeated words, the function should return the special value None. Next, write a program that uses the function and that asks the user for a couple of sentences about him/herself and prints any repeated words.
 
- Modify the previous program to print back to the user words that begin with the syllable structure CV, that is, a consonant followed by vowel.

- Modify the previous program to print words, if any, that begin with the negative prefix "in/im/i" as in "inadequate/impossible/irreal/illegal".

## Filtering with list comprehensions (in [ch. 19 of Think Python](https://greenteapress.com/thinkpython2/html/thinkpython2020.html), section 19.2)

List comprehensions are a concise way of filtering collections or sequences, such as lists, if the filtering is simple, that is, can be written in one line.

See examples in Section 19.2 of textbook.
- capitalize all
- only uppercase

## More Practice:

- Write a list comprehension that creates a list with the first character of the words in the following list:  numbers = ['one', 'two', 'three', 'four', 'five']

- Using the same numbers list from the previous exercise, write a list comprehension that creates a list with only words that have an "e".
Rewrite the function above that filters words with a CV syllable at the beginning, using a list comprehension (instead of a for loop).

## Possible solutions to in-class practice exercises


Using the technique of filtering lists within a for loop, define a function that takes as input a string and returns as output a list of repeated words, if any, in the input string. If there are no repeated words, the function should return the special value None. Next, write a program that uses the function and that asks the user for a couple of sentences about him/herself and prints any repeated words.
```
def has_duplicates(input_str):

    # convert string to uppercase
    # Does NOT change the string, but rather creates a copy,
    # so I need to save the result to a variable.
    #
    # Remember: strings are immutable--cannot be changed (unlike lists or dictionaries)
    input_str = input_str.upper()

    # split sentences on non-letters.
    # Mind the caret symbol `^` at the beginning of
    # the character class (the square brackets)!
    # the caret `^` negates the match, so
    # anything that isn't in the brackets
    # will be matched
    import re
    words = re.split(r"[^a-z]+", input_str, flags=re.IGNORECASE)

    # Sorts words into alphabetical order, changing the list that was created by re.split() above.
    # Lists are mutable (i.e. can be changed) and `sort()` changes the same string it belongs to,
    # no need to save to a variable as the original variable `words` is permanently changed
    words.sort()

    # create empty list in order to add repeated words to it; this is often referred to as a "collector"
    repeated_wds = []

    # range(n) produces a list of integers from 0 to one less than n.
    # range(m, n) produces a list of integers from m to one less than n.
    
    # loop over the indexes of the words and add any repeated words to the collector list
    # Note: the last index will be one less then the length of `words`.
    for i in range(len(words)):

        # if on first word, jump to the next iteration of the for loop (jump to the second word)
        if i == 0:
            continue

        # if the current word is identical to the previous word, add the current word to the collector list
        if words[i] == words[i - 1]:
            # modifies/changes the list in place
            repeated_wds.append(words[i])

    # Here the for loop is finished (mind the indentation!).

    # See if the "repeated_wds" list has anything in it.
    # An empty list `[]` is considered False.
    # If the list is not empty, return that list to the user, otherwise return "None".
    if repeated_wds:
        return repeated_wds
    else:
        return None


# main program
sentences = input("Please write a couple sentences about yourself: ")
rep_wds = has_duplicates(sentences)  # call the function defined above
if rep_wds:
    print("You repeated the following words:")
    print(rep_wds)
else:
    print("You didn't repeat any word. Congratulations on the breadth of your vocabulary!")

```

Modify the previous program to print back to the user words that begin with the syllable structure CV, that is, a consonant followed by vowel.

```
def cv_words(input_str):

    # splits sentences on non-letters; mind the caret symbol at the beginning of the character class (the brackets)!
    import re
    words = re.split(r"[^a-z]+", input_str, flags = re.I)

    # create the collector
    cv_wds = []

    # Here we loop over the words directly,
    # We don't have to use range() as before,
    # because we don't need indexes to compare
    # the previous word (i.e. words[i - 1])
    for word in words:
        if re.search(r"\b[^aeiou][aeiou]\w*", word, flags = re.I):
            cv_wds.append(word)  # append to the collector

    if cv_wds:
        return cv_wds
    else:
        return None

# test the function just defined
sentences = input("Please write a few sentences about yourself: ")
output = cv_words(sentences)
if output:
    print("Here are the words that have CV syllable structure at the beginning:")
    print(output)
else:
    print("You didn't write a single word with a CV syllable structure at the beginning. That's hard to do!")
```

Modify the previous program to print words, if any, that begin with the negative prefix "in/im/i" as in "inadequate/impossible/irreal/illegal".
```
def neg_in_words(input_str):

    # splits sentences on non-letters
    # mind the caret symbol at the beginning of the character class (the brackets)!
    import re
    words = re.split(r"[^a-z]+", input_str, flags = re.I)

    neg_wds = []  # creates the collector
    for word in words:
        if re.search(r"\bi[mnrl]+\w+", word, flags = re.I):
            neg_wds.append(word)  # appends to the collector

    if neg_wds:
        return neg_wds
    else:
        return None

# test the function just defined
sentences = input("Please write a few sentences about yourself: ")
output = neg_in_words(sentences)

if output:
    print("Here are the words that might have the negative prefix 'in/im/i' at the beginning:")
    print(output)
else:
    print("You didn't write any words that might have the negative prefix 'in/im/i' at the beginning.")
```

Filtering with list comprehensions

Write a list comprehension that creates a list with the first character of the words in the following list:  `numbers = ['one', 'two', 'three', 'four', 'five']`

```
numbers = ['one', 'two', 'three', 'four', 'five']
new_list = [word[0] for word in numbers]
print(new_list)

# Using the same numbers list from the previous practice exercise, write a list comprehension that creates a list with only words that have an "e".
numbers = ['one', 'two', 'three', 'four', 'five']
new_list = [i for i in numbers if 'e' in i]
print(new_list)

# Rewrite the function above that filters words with a CV syllable at the beginning, using a list comprehension (instead of a for loop).
import re
numbers = ['one', 'two', 'three', 'four', 'five']
new_list = [i for i in numbers if re.search(r"\b[^aeiou][aeiou]\w+", i, flags=re.I)]

print(new_list)
```

# Lesson 4.3: Processing sentences

[see Video Lesson](https://www.youtube.com/watch?v=7L95Liar3hI&list=PL6feCz_CL2KCOyB2Hkhl_NtZgeJfMrdWr&index=48)

*Objective*:
Students will be capable of dividing paragraphs into sentences, in order to work with individual sentences.

Processing sentences

## Practice:

- Split up a paragraph at punctuation that indicates the end of a sentence
using a regular expression and `re.split()`
or with the NLTK sentence tokenizer function. First, you need to download all the resources used in the NLTK book, using some code here.
Work with each sentence in a loop, collecting desired information during each iteration.

- Define a function that takes as input a string with a paragraph and returns as output a float with the average number of words per sentence in the paragraph given.

## Possible solutions to in-class practice exercises

Define a function that takes as input a paragraph as a string and returns as output a float with the average number of words per sentence in the paragraph given.

```
def avg_words_per_sent(paragraph):
    """
    Takes as input a string and returns as output a floating point number that indicates the average length in words of the sentences in the string.
    """
    import re
    sentences = re.split(r"[\.\?\!]", paragraph)
    sentences = [i for i in sent if len(i) > 0]  # list comprehension to delete the empty last element of the list

    # option 1: filter with for loop
    len_words = []  # collector list
    for sent sentences:
        wds = sent.split()
        len_wds.append(len(wds))

    # option 2: filter with list comprehension (it's commented out right now)
    # len_words = [len(i.split()) for i in sent]
    
    # does some simple math to get average (sum up the lengths of each sentence, divided by the nunber of sentences)
    output = sum(len_words) / len(sent)  
    return output

# test it out!
text = "I teach linguistics, including and especially technical tools for linguistic analysis. I teach these wonderful topics at BYU, in Provo, Utah! Where do you work?"

print(avg_words_per_sent(text))
```
