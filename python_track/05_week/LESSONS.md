# Lesson 5.1: File I/O (input/output)

[see Video Lesson](https://youtu.be/QYxoHjjZSHI)

(Also see code examples from the video at the end of this Lesson 5.1)

**Objective**: Students will be capable of navigating to a directory and reading, writing, and appending to text files.


This lesson is inspired by [this article on Python's pathlib](https://realpython.com/python-pathlib/)

A 'path' simply means the location of a file or directory.  You can think of it as the path through directories that you have to follow to get to the directory or file that you want to work in or work with.  There are two kinds of paths:

1. relative path
2. absolute path

A relative path means, a path relative to the location (the directory) that you are currently in--in other words, where you need to go, or what directories you need to follow to get to the target file or directory.  A relative path often starts with a `.` which means 'the current directory' (e.g. `./relative/path/to/file.txt`).  Two dots (`..`) are used to refer to a parent directory--the directory containing the current location in the path.  In the case of an absolute path, the path starts from the very root of the file system represented by forward slash `/` on most systems, but back slash ` \ ` on Windows for historical reasons (e.g. `/absolute/path/to/file.txt` or Windows style: `\absolute\path\to\file.txt`).

Python traditionally has used the `os` module to work with paths to directories and files.  I'll touch on this older, and still widely used (and valid) way of working with files and directories first, so that you know what it looks like as you are likely to see it or perhaps already use it yourself.  Afterward, I'll show you a more modern and straightforward way to work with files and directories in Python.  Feel free to skip over the traditional way if you just want to get straight to doing it more simply.

__Note__: I used longer, explicit names for variables and instructional comments for your educational benefit ([You're Welcome Video from Moana](https://www.youtube.com/watch?v=79DijItQXMM&pp=ygUOeW91J3JlIHdlbGNvbWU%3D)); however, the style of comments and variable names may not necessarily be appropriate (clear and informative) for the context of your own code.  Use good judgement. 

## Traditional Way--using the `os` module
```
import os

# the getcwd function's name stands for 'get current working directory'
current_dir = os.getcwd()

# print the (absolute) path of the current directory
print(current_dir)

# get a list all files and directories in the current directory
directory_list = os.listdir('.')

# (Example 1) open and print each file, skipping directories
for file_or_dir in directory_list:

    if os.path.isdir(file_or_dir):
        # skips to the next file_or_dir
        continue

    # If the `if` statement above found that the `isdir` function returned
    # false, then we have a file and we can open it for reading with 'r'
    # as the second argument
    file_object = open(file_or_dir, 'r')
    
    file_text = file_object.read()
    
    # we should close the file after we get the text
    # this saves memory
    file_object.close()
         
    print(file_text)
    

# (Example 2) process each file or directory

(Assume the same code as before Example 1.)

for file_or_dir in directory_list:

    if os.path.isdir(file_or_dir):
        # skips to the next file_or_dir
        continue

    
    # Use a `with` statement
    with open(file_or_dir, 'r') as file_object:
        # This must be indented
        file_text = file_object.read()
        
    # indention for the `with` statement ends here, so file is
    # automatically closed, we don't have to call `file_object.close()`
    
    print(file_text)
```


## New Way--using the `pathlib` module

The `pathlib` module is a more recent tool (Python version 3.4) for managing paths and even opening files (something the `os` module does not provide).  It provides simpler ways to work with files.

```
import pathlib

# print the current working directory
print(pathlib.Path.cwd())

# Create a Path object representing the current directory to provide
# many useful methods (methods are functions attached to an object)
path_object = pathlib.Path('.')

# Create a generator object containing names (as strings) of 
# files and directories within the current directory
gen_object = path_object.iterdir()

# transform the generator object above into a normal list
directory_list = list(gen_object)

# create a generator object containing only text files
# (files ending with file extension `.txt`)
glob_gen_object = path_object.glob("*.txt")

# transform this generator object into a list
text_files = list(glob_gen_object)

# get the first text file in the list
# Note: this is also a path object like
# the original
first_file = text_files[0]

# extract the text
file_text = first_file.read_text()

print(file_text)
```

See how we have done away with all the ceremony that is required with:

`file_object = open(file_path, 'r')` or `with open(file_path, 'r') as file_object:`

The indentation required after the `with` statement can also be confusing in my experience, since loops use indentation too, but `with` IS NOT A LOOP!  It only happens once, then when the indentation after `with` ends, your file is closed automatically.

The designers of Path objects made closing files an automatic process (it still needs to be done) when reading or writing to them with the `read_text()` and `write_text()` methods.

Here is an example to write text to a file:

I'm using a variation on the `import` statement to get direct access to `Path` inside the `pathlib` module instead of having to refer to it this way: `pathlib.Path`.
```
from pathlib import Path

path_object = Path('.')

# Get all files whose name starts with "INFO_"
files_as_gen_object = path_object.glob("INFO_*")

# Note: each file inside the generator object is 
# a `Path` object

# Iterate (loop) over each file in the generator object,
# overwriting the contents of each
for my_file in files_as_gen_object:
    my_file.write_text("Hello there, sorry to overwrite whatever you had here before")
    
# Above we loop over every file (as a Path object)
# that the `files` variable holds.
#
# We overwrite the text, rather than add the text to the end of the file.
# Be careful, as this is probably not what you intended.

```

    
You can use the same loop above to read the text of each file.  Just change
`my_file.write_text()` to `file_text = my_file.read_text()`

then process the file's text as needed.

We haven't completely escaped the need to use `with` though.  There are two situations (at least) that you would still want to use `with`.

1. In the case that we want to append more text to a file, which we certainly will want to do sometimes, the best practice is to use `with` in combination with a `Path` object.  The following code demonstrates this.

```
from pathlib import Path

# Create a Path object for the file
file_path = Path("example.txt")

# Append text to the file--'a' is for append
with file_path.open('a') as file_object:
    file.write("This text will be appended.")
```

To Specify whether you want to "r"ead, "w"rite, "r+"ead and write, or "a"ppend to the file. Use "r", "w", "r+"The default is "r".

2. In the case of reading a very large file, using the `read_text()` method on a Path object will use a lot of memory, since it will read ALL of the text at once.  You can instead read the file line by line, thus not having to load the entire file's text into your computer's memory at once, but rather just a line at a time.  This can improve your program's performance greatly with particularly large files.  See the example below:

```
from pathlib import Path

large_file = Path("./relative/path/to/large/file.txt")

# Save some memory by only reading one line at a time from the file object
with large_file.open('r') as file_object:
    for line in file_object:
        print(line)
```

One final thing to demonstrate with `Path` objects, is their special way to create new `Path`[s].  The following code demonstrates this:

```
from pathlib import Path

current_dir = Path('.')

# relative path to another directory
# follows the parent directory (two dots) then another directory inside that
other_dir = current_dir / "../other_dir"

# Print the absolute path of the newly created `Path`
print(other_dir.absolute)

# Create and print a list of its contents
# Note: I'm combining the generator creation and
# list creation into one line
list_of_contents = list(other_dir.iterdir())

print(list_of_contents)
```


**Practice**:

- Download the text file from: [http://greenteapress.com/thinkpython2/code/words.txt](http://greenteapress.com/thinkpython2/code/words.txt). Write a program that navigates to the directory with the words.txt file, reads the file, and prints only the words with 20 or more letters.

- Write a function called `has_no_e()` that takes as input a string and returns as output a Boolean value indicating whether the string has the letter "e" in it.

- Modify your previous program to print only words that are 15 or more letters and don't have the letter "e" in them.

- Modify the previous program so that instead of simply printing to the screen, the words are written to a file named long_words_without_e.txt, in the same directory with words.txt.


## Code Examples from Video

```
# Traditional way
import os

print(os.getcwd())

files = os.listdir('corpus')

for file in files:
    with open('corpus/' + file, 'r') as file_object:
        text = file_object.read()
    
    # File closed
    print(text)

# New way using pathlib
import pathlib

# You can import Path directly this way so as to avoid typing `pathlib`
# every time before `Path`
# from pathlib import Path

current_path = pathlib.Path('.')

corpus_path = current_path = "corpus"

for file in corpus_path.iterdir():
    text = file.read_text()
    print(text)

# New file
new_path = current_path / "new_file.txt"

new_path.read_text()
s = "Hello\nthere\nperson\n!"

with new_path.open('a') as file_obj:
    for word in s.split():
        file_obj.write(word + "\n#")
# closed here

```

# Lesson 5.2: Handling exceptions with try...except

**Objective**: Students will be capable of catching exceptions (errors) in their scripts.

## [Think Python, chapter 14](https://greenteapress.com/thinkpython2/html/thinkpython2015.html), section 14.5: Catching exceptions (errors)

The `try/except` words allow Python programmers to try to execute a line or block of code and deal with problems (exceptions), if they happen.

See example from Section 14.5 of textbook.

Conceptually similar to `if/else` statements in that if the conditional statement evaluates to True, the else block of code isn't executed. If all goes well with the try block of code, the except block isn't executed.

**Practice**:

- Write a program that asks the user for a number between 100 and 1000 and prints out that number divided by -5, and then by -4, and then by -3, until you reach 5. Use a `try/except` block so that when the number given by the user is divided by 0 an error message is thrown, but the script continues.

_Hint_: The range() function is handy here.

## Exercise Solutions for Lesson 5.2

Write a program that asks the user for a number between 100 and 1000 and prints out that number divided by 5, and then by 4, and then by 3, until you reach -5. Use a try...except block so that when the number given by the user is divided by 0 an error message is thrown, but the script continues.

```
num = input("Please give me a number (an integer) between 100 and 1000: ")
num = int(num)  # converts from string to integer
for i in range(-5, 6):
    try:
        # converts several integers to strings in order to concatenate the message parts
        print(f"{num} divided by {i} equals {num / i}")  
    except:
    # comment the except line above and uncomment the one below to only catch
    # exceptions which are division by zero
    #except ZeroDivisionError:
        print("You can't divide by zero, you silly goose!")
```
## Lesson 5.3: Normalizing counts

**Objective**:
Students will be capable of producing normalized counts of specific linguistic features in text files.

## Normalized counts

Let's suppose that:
Corpus A has 10,000 words and Corpus B has 5,000 words;
"handsome" occurs 30 times in Corpus A, but only 25 times in Corpus B;

Question: In which corpus is "handsome" more frequent?

According to raw counts, "handsome" is more frequent in Corpus A, because 30 is larger than 25.
However, according to normalized counts (per 1,000 words), "handsome" is more frequent in Corpus B, because it occurs 5 times per 1,000 words in Corpus B (25 / 5,000 * 1,000 = 5), but only 3 times per 1,000 words in Corpus A (30 / 10,000 * 1,000 = 3).

_Conclusion_: According to raw counts, "handsome" is more frequent in Corpus A, but according to normalized counts (per thousand words) "handsome" is more frequent in Corpus B.
Take-home message: In order to compare counts of words or linguistic features between different corpora or texts, you first have to normalize counts.

Logic:
1. Navigate to the directory with the files and get their names.
2. Create two counter variables, one for the total number of words and one for the number of matches of the desired linguistic feature.
3. Loop over the files.
4. Within each iteration, split the current file into words.
5. Loop over the words in the current file.
6. Within each iteration, increment the total words counter. Then, check if the current word matches the regex, and if so, increment the linguistic feature counter.
7. After the loop has finished, divide the linguistic feature counter by the total words counter to create a ratio, and then multiply that ratio by the base of the normalized count (for example, by 1,000 if the base is per 1,000 words).
8. Print out the results to the screen or write them to an output file.

**Practice**:

1. From the [Gutenberg Project](https://www.gutenberg.org/), download several .txt files of novels from an author of your choice. Write a program to get a count normalized to 1,000 for a regex pattern of your choice in one of the files. Write the output to a .csv file, specifying that the columns be separated by a tab (\t) and that the rows be separated by a newline break (\n). Import the output .csv file into a spreadsheet (Excel, google spreadsheet, etc.) of your choice.

2. Modify the program to get normalized counts for each of the novels you downloaded. Modify the code that produces the output .csv file so it is easy to compare the normalized counts across the novels.

3. Download several .txt files from another author. Modify your program to get normalized counts for each novel. Again, modify the code that produces the output .csv file to make it easy to compare the normalized counts across the novels.

4. Modify your program to get normalized counts for each author (two normalized counts, one per author). Again, modify the code that produces the output .csv file to make it easy to compare the normalized counts across the two authors.

## Solutions to Exercises for Lesson 5.3


Here we downloaded six novels by Jane Austen.

The included regex finds all forms of "handsome" (e.g., "handsome", "handsomest", "handsomer", etc.)

```
import re
from pathlib import Path

count_feature = 0
count_total = 0
file_path = Path("/Users/ekb5/Documents/LING_240/Jane_Austen/Pride_and_Prejudice.txt")

with file_path.open('r') as fin:
    # read in the lines of the file, one at a time
    for line in fin:
    
        # split the line up into words
        words = re.split(r"[^-'a-z]+", line, flags=re.I)  
        
        # loop over the words, one at a time
        for word in words:  
            # if the current list element isn't empty
            if len(word) > 0:
                # increment the total word counter
                count_total += 1  
                
                # if the current word matches the regex, increment the feature counter
                if re.search(r"\bhandsome\w*", word, flags=re.I):
                    count_feature += 1

    # Here the outer loop has finished (mind the indentation!)
    # And all lines have been read in
    
    # Now calculate the normalized count
    # because division and multiplication are in the same order of operation, the division will happen before the multiplication because division comes first in the line
    norm_count = count_feature / count_total * 1000  
    
    download_path = Path("/Users/ekb5/Downloads/")
    results_path = download_path / "results.csv"
    
    # "w" for writing to the results file
    with results_path.open("w") as fout:
        fout.write("feature\tnorm_count_1000\n")
        fout.write("handsome and derived forms\t" + str(norm_count) + "\n")
```

Modify the program to get normalized counts for each of the novels you downloaded. Modify the code that produces the output .csv file so it is easy to compare the normalized counts across the novels.


```
import re
from pathlib import Path

book_dir_path = Path("/Users/ekb5/Documents/LING_240/Jane_Austen")

# gets filenames with extension .txt, regardless of case (e.g., .txt, .TXT, .Txt)
file_names = [i for i in book_dir_path.iterdir() if re.search(r"\.txt$", i.name, flags=re.I)]  

output_file = Path("/Users/ekb5/Downloads/results.csv")

with output_file.open("w") as fout:
    # add columns headings (mind the "w" in the previous line)
    fout.write("feature\tnovel\tnorm_count_1000\n")
    for novel in file_names:
        count_feature = 0
        count_total = 0
        
        # the previous fout file handle is still open here
        with novel.open('r') as fin:
        
            # read in the lines, one at a time
            for line in fin:
                # split the line up into words
                words = re.split(r"[^-'a-z]+", line, flags=re.I)
                
                # loop over the list with words, one at a time
                for word in words:
                    if len(word) > 0:  # if the current list element isn't empty
                        count_total += 1  # increment the total word counter
                        if re.search(r"\bhandsome\w*", word, flags=re.I):
                            count_feature += 1  # if the current word matches the regex, increment the feature counter

            # once all lines have been read in (mind the indentation!), calculate the normalized count
            norm_count = count_feature / count_total * 1000  # division happens before multiplication
            output = "handsome and derived forms\t" + os.path.basename(novel) + "\t" + str(norm_count) + "\n"  # create output row, with columns separated by tabs and then a newline break at the end
            fout.write(output) # appends the current row to the output file (because of the "a" in the active file handle)
```

Download several .txt files from another author. Modify your program to get normalized counts for each novel. Again, modify the code that produces the output .csv file to make it easy to compare the normalized counts across the novels.

I downloaded four novels by Mark Twain and put them in a new directory.

```
import re
from pathlib import Path

# gets full pathway and filenames from two different directories
mypath = "/Users/ekb5/Documents/LING_240/Jane_Austen/"
austen_files = [os.path.join(mypath, i) for i in os.listdir(mypath) if re.search(r"\.txt$", i, flags=re.I)]
mypath = "/Users/ekb5/Documents/LING_240/Mark_Twain/"
twain_files = [os.path.join(mypath, i) for i in os.listdir(mypath) if re.search(r"\.txt$", i, flags=re.I)]

# creates a single list with filenames from both directories
file_names = []
file_names.extend(austen_files)
file_names.extend(twain_files)

output_file = "/Users/ekb5/Downloads/results.csv"
with open(output_file, "w") as fout:
    fout.write("feature\tauthor\tnovel\tnorm_count_1000\n")  # add columns headings (mind the "w" in the previous line)
    for novel in file_names:

        count_feature = 0
        count_total = 0

        # gets author name
        if re.search(r"emma|pride|sense|persuasion|northanger|mansfield", novel, flags=re.I):
            author = "Jane Austen"
        elif re.search(r"huck|sawyer|connecticut|pauper", novel, flags=re.I):
            author = "Mark Twain"
        else:
            author = "not sure"

        # opens current novel and increments counters, as appropriate
        with open(novel) as fin:
            for line in fin:  # read in the lines, one at a time
                words = re.split(r"[^-'a-z]+", line, flags=re.I)  # split up the line into words
                for word in words:  # loop over the list with words, one at a time
                    if len(word) > 0:  # if the current list element isn't empty
                        count_total += 1  # increment the total word counter
                        if re.search(r"\bhandsome\w*", word, flags=re.I):
                            count_feature += 1  # if the current word matches the regex, increment the feature counter

            # once all lines have been read in (mind the indentation!), calculate the normalized count
            norm_count = (count_feature / count_total) * 1000  # division happens before multiplication
            output = "handsome and derived forms\t" + author + "\t" + os.path.basename(novel) + "\t" + str(norm_count) + "\n"  # create output row, with columns separated by tabs and then a newline break at the end
            fout.write(output) # appends the current row to the output file (because of the "a" in the active file handle
```

Modify your program to get normalized counts for each author (two normalized counts, one per author). Again, modify the code that produces the output .csv file to make it easy to compare the normalized counts across the two authors.

```
# puts the bulk of the previous program in a function so that I can call it several times, in order to not repeat myself (DRY = "don't repeat yourself")
def get_norm_count(pathway, regex):
    """Take as input two strings: one indicating a pathway to a directory with .txt files, the second is a regex to get the normalized count for within all .txt files in the directory"""

    import re
    from pathlib import Path

    # gets filenames with the full pathway, not just the filenames themselves
    file_names = [os.path.join(pathway, i) for i in os.listdir(pathway) if re.search(r"\.txt$", i, flags=re.I)]

    count_feature = 0
    count_total = 0

    for novel in file_names:

        # opens current novel and increments counters, as appropriate
        with open(novel) as fin:
            for line in fin:  # read in the lines, one at a time
                words = re.split(r"[^-'a-z]+", line, flags=re.I)  # split up the line into words
                for word in words:  # loop over the list with words, one at a time
                    if len(word) > 0:  # if the current list element isn't empty
                        count_total += 1  # increment the total word counter
                        if re.search(regex, word, flags=re.I):
                            count_feature += 1  # if the current word matches the regex, increment the feature counter

    norm_count = count_feature / count_total * 1000  # division happens before multiplication
    return norm_count

# calls function
regex = r"\bhandsome\w*"  # again, DRY ("don't repeat yourself")
austen_count = get_norm_count("/Users/ekb5/Documents/LING_240/Jane_Austen/", regex)
twain_count = get_norm_count("/Users/ekb5/Documents/LING_240/Mark_Twain/", regex)

with open("/Users/ekb5/Downloads/results.csv", "w") as fout:
    fout.write("feature\tauthor\tnorm_count_1000\n")
    fout.write("handsome and derived forms\tJane Austen\t" + str(austen_count) + "\n")
    fout.write("handsome and derived forms\tMark Twain\t" + str(twain_count) + "\n")
```
