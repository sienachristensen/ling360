# Practice Writing a Word Counter

One solution to this is below, but try to figure it out on your own first.

Write a word counter that reads some text from a file, or copy and paste the text directly into quotes
inside your program.  Then write the code to count the words, collecting them into a new variable, and finally
write the words to a new file when you reach a certain count that you choose.  Here is some very simple text you
can use to write only 5 or less words:

```
text = """
some words here\nword4 word5 word6
"""
```

Also, you can also search for free texts on [Project Gutenberg](https://www.gutenberg.org/).  I recommend using 
the plain text UTF-8 encoding version of the text you choose.

You can use `Path` from the pathlib module like this: `from pathlib import Path`
after creating an object that represents a path using `path = Path("path/to/file/or/directory")`
This path variable can be used to get text `text = path.read_text()` if it refers to a file
with text.

```
# If you use this import line directly below, you'll have to prefix pathlib.Path
#import pathlib
import re
# allows using Path directly
from pathlib import Path

# Creates a Path object of the current directory '.' means current directory
curr_dir = Path('.')

# Creates another Path object to the corpus directory
corpus_dir = curr_dir / "corpus"

# Creates a Path object to the file fileA.txt
fileA = corpus_dir / "fileA.txt"

# Reads the text from the fileA Path object
text = fileA.read_text()

print(text)

# Stops the program dead in its tracks if uncommented
#exit(0)


# Another example using text pasted into the program
####################################################

text = """
some words here\nword4 word5 word6
"""

# Get words as a list
word_list = re.split(r'\s', text)

# Another alternative to the above re.split
# word_list = text.split()

# Create a Path object saved in out_file
out_file = Path("outfile.txt")

# Some initialized variables
new_text = ""
word_count = 0

# Look at each word, skipping empty strings ''
for word in word_list:
    if word == '':
        continue
    
    # Increase the word count since it is not equal to ''
    word_count = word_count + 1
    
    # Add the word to our new string
    new_text = new_text + " " + word

    # Write the words to the file once you get to 5
    # words
    if word_count == 5:
        out_file.write_text(new_text)


# This prints the total word count of the text or file.
print(word_count)

# You could end the for loop above after reading the 
# target word_count (5) by adding `break` inside the if
# statement where we write to the new file
```