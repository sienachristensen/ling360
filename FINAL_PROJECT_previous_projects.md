# Previous final projects

Here are some final project topics from previous semester of this class:
- Movie genre decision tree
- Analysis of Tweets by company
- Gender prediction of authors
- Topic modeling of questions in online forum for a CS class
- Snapshot of writing style
- Personal pronoun use by Barack Obama in all of his speeches
- Readability analysis
- Corpus construction/cleaning
- GUI concordancer of the Quran

- The data for your final project can come from anywhere, given that the data helps you answer the 
question you have or the problem you are trying to solve. Data can come from anywhere, including 
(but definitely not limited to):

- Social media, like Twitter, Reddit, Tumblr, Yelp (Facebook is now less open for scraping data)
- Text archives, like Gutenberg Project
- Corpora
  - Mini-CORE
  - [See this NLTK corpora list](https://www.nltk.org/book/ch02.html)
- Web, with webscraping techniques to extract text out of webpages
- Many other possible sources of data are possible