# Winter Semester 2024 Ling 360 (aka Digital Hum 360)

## Programming for Text Processing and Analysis
Mon, Wed & Fri 3:00 pm - 3:50 pm <br>
 B013 JFSB <br>
[Canvas Course Link](https://byu.instructure.com/courses/25351)

Professor: <br>
Jon Forsyth, MA <br>
Office: 4001 JKB <br>
Office hours: Mon & Wed 1:30-2:30pm, or by appt. <br>
Zoom: https://byu.zoom.us/j/4093185498 <br>
jon_4syth@byu.edu*

*Note: The instructor normally responds to emails within 24 hours on Mon-Fri.

Teaching Assistant:
N/A


## Course purpose
As a result of taking the course, students will be capable of searching within and analyzing large amounts of written language from a variety of genres and in different formats with the computer programming language Python or Elixir (at student's option).

## Course description
This course is for students who wish to learn how computers can be used to analyze written language. During the course, students investigate language use and variation using computer programs or scripts that they develop. This course is designed for novices in computer programming and requires no prior experience with programming. However, this course is also beneficial to students with previous programming experience who would like to learn how to write programs to analyze written language. The course uses a hands-on approach to introduce programming logic and skills in a natural and logical way.  We write code to investigate linguistic patterns in words, sentences, texts, and untagged and tagged corpora (collections of text). This is an interesting introduction to programming logic, basic programming skills, and text processing for linguistic analysis. Students may use the programming language of their choice between Python or Elixir.

## Course learning outcomes

- Students will be able to solve practical problems in text processing.
- Students will be able to apply basic programming skills using data structures, regular expressions, and control statements.
- Students will be able to use Python or Elixir to process and manipulate texts and extract linguistic data.

## Materials and resources

All the materials and readings are freely available online, and therefore, students do not need to buy hard copies.  An optional book for the student's personal educational benefit is "A Philosophy of Software Design 2nd Edition".  No assignments or required reading will be based on this book, and it is for those who want to improve their skills in writing well-designed programs.

- Downey, Allen B. 2015. Think Python, 2nd ed. Sebastopol, CA: O'Reilly Media. Freely available online: [http://greenteapress.com/wp/think-python-2e/](http://greenteapress.com/wp/think-python-2e/)
- Jurafsky, Dan and James H. Martin. 2018. Speech and Language Processing, 3rd ed. Upper Saddle River, NJ: Prentice Hall. [https://web.stanford.edu/~jurafsky/slp3/](https://web.stanford.edu/~jurafsky/slp3/)
- Bird, Steven, Ewan Klein, and Edward Loper. Natural Language Processing with Python: Analyzing Text with the Natural Language Toolkit. Sebastopol, CA: O'Reilly Media. Freely available online: [http://www.nltk.org/book/](http://www.nltk.org/book/)
- Mitchell, Ryan. 2018. Web Scraping with Python: Collecting Data from the Modern Web, 2nd ed. Sebastopol, CA: O'Reilly Media. Freely available online through the library's catalog [here](https://lib.byu.edu/search/byu/record/cram.126.on1029878774.1?holding=vqpf5q6qrxlps7nq).
- [pythex.org](https://pythex.org/)
- PyCharm Python Code Editor-community edition (optional) [https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)
- Elixir School [https://elixirschool.com/en](https://elixirschool.com/en)

## Quizzes

In order to be prepared for hands-on activities during lessons, students complete assigned readings and take the accompanying reading quiz in the Content Management System (CMS) by the due date and time listed in the CMS. Students have at most two (2) attempts for each quiz, with the higher grade kept. Note: Quizzes cannot be taken late. Before the calculation of the final semester grade, the lowest quiz score (one quiz) will be dropped.

## Assignments

In order to apply the programming concepts and data structures learned in the lessons to linguistic analysis, the students complete an assignment nearly every week. Students are highly encouraged to start the assignments early in the week in order to have time to work through problems that inevitably occur. Weekly assignments that are turned in late receive a penalty of one point out of ten points per 24-hour period, up to 7 days, after which time the assignment is not accepted. Students are encouraged to start the weekly assignments early in the week in order to have time to work through problems that they encounter. 

## Communication between Instructor and Students

It is students' responsibility to check for announcements in the CMS daily. Students are encouraged to ensure that the CMS is set to email them a notification when announcements are posted to the CMS. The instructor usually responds to email within 24 hours during the work week, but usually doesn't check email on weekends.

## Final grade

The components of the final grade are the following:

|    %     |          |
|----------|----------|
| 15% | Quizzes |
| 70% | (Nearly) weekly assignments |
| 15% | Final project and presentation |

## Grading

The final semester letter grade will be calculated according to the following scale. Students are encouraged to monitor their grades on the CMS throughout the semester to prevent surprises. NOTE: No rounding is applied, for example, an 89.99% is a B+ while 90.00% is an A-.

|    |        |    |       |    |       |      |       |
|----|--------|----|-------|----|-------|------|-------|
| A  | 93-100 | B  | 83-86 | C  | 73-76 | D    | 63-66 |
| A- | 90-92  | B- | 80-82 | C- | 70-72 | D-   | 60-62 |
| B+ | 87-89  | C+ | 77-79 | D+ | 67-69 | F    | 0-59  |

## Schedule

Students must view the week-by-week homework schedule in the CMS. Also, students are encouraged to view the lesson plans available on Gitlab linked to in the CMS. NOTE: The schedule should be considered tentative, as it may be changed as needed.  Each Week's dates are Monday through Friday except for the last two weeks of the semester.

| Week | Topics                                                                                                                                                      | Homework                                                                                                                                                                                                                                                              |
|------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Week 1 <br> Jan 8 - 12 | Data types <br> REPL vs. script <br> Variables                                                                                                              | [Read _Think Python Ch. 1 'The Way of the Program'](https://greenteapress.com/thinkpython2/html/thinkpython2002.html) and take quiz                                                                                                                                   |
| Week 2 <br> Jan 15 - 19 | Mon. Jan 15th - Martin Luther King Jr Day <br> Errors <br> Control flow (if...else) <br> String slicing                                                     | [Read Think Python, ch. 2](https://greenteapress.com/thinkpython2/html/thinkpython2003.html) and take quiz <br> [Read Think Python, ch. 8](https://greenteapress.com/thinkpython2/html/thinkpython2009.html) and take quiz <br> Turn in Asgn01                        |
| Week 3 <br> Jan 22 - 26 | Regular expressions <br> Measuring accuracy of searches <br> Loops (for and while) <br> The list data structure                                             | [Read Speech and Language Processing, Chap 2 through section 2.1.5](https://web.stanford.edu/~jurafsky/slp3/2.pdf) and take quiz <br> [Read Think Python, ch. 10](https://greenteapress.com/thinkpython2/html/thinkpython2011.html) and take quiz <br> Turn in Asgn02 |
| Week 4 <br> Jan 29 - Feb 2 | Defining functions <br> Filtering lists <br> Processing sentences                                                                                           | [Read Think Python, ch. 3](https://greenteapress.com/thinkpython2/html/thinkpython2004.html) and take quiz <br> Turn in Asgn03                                                                                                                                        |
| Week 5 <br> Feb 5 - 9 | File I/O (input/output) <br> Handling exceptions (try...except) <br> Counting and normalized counts                                                         | [Read Lesson 5.1-5.2](https://gitlab.com/flash4syth/ling360/-/blob/main/python_track/05_week/LESSONS.md) and take quiz <br> Turn in Asgn04                                                                                                                            |
| Week 6 <br> Feb 12 - 16 | Handling non-UTF-8 character encoding <br> Extracting text from Word documents <br> Extracting text from indexed PDF files                                  | Turn in Asgn05                                                                                                                                                                                                                                                        |
| Week 7 <br> Feb 19 - 23 | Mon. Feb 19th - Presidents Day <br> The dictionary data structure <br> The tuple data structure <br> Sorting and lambda functions <br> Word frequency lists | [Read Think Python, ch. 11](https://greenteapress.com/thinkpython2/html/thinkpython2012.html) and take quiz <br> Watch [Coursera dictionaries video](https://www.coursera.org/learn/python-data/lecture/rQKjf/9-1-dictionaries)<br> Turn in Asgn06                    |
| Week 8 <br> Feb 26 - Mar 1 | Basic descriptive statistics <br> Part-of-speech tagging with NLTK <br> Part-of-speech tagging with TreeTagger                                              | [Read Natural Language Processing with Python, sections 1 & 2 in ch. 5](https://www.nltk.org/book/ch05.html) and take quiz <br> Turn in Asgn07                                                                                                                        |
| Week 9 <br> Mar 4 - 8 | Processing tagged texts <br> Parsing XML data <br> Parsing JSON data <br> Read (to be determined)                                                           | Turn in Asgn08                                                                                                                                                                                                                                                        |
| Week 10 <br> Mar 11 - 15 | Sentiment analysis with VADER <br>                                                                                                                          | Turn in Asgn09                                                                                                                                                                                                                                                        |
| Week 11 <br> Mar 18 - 22 | Web Scraping with _requests_ and _Beautiful Soup_                                                                                                           | [Read Web Scraping with Python, ch. 3](https://lib.byu.edu/search/byu/record/cram.126.on1029878774.1?holding=vqpf5q6qrxlps7nq) and take quiz <br> Turn in Asgn10                                                                                                      |
| Week 12 <br> Mar 25 - 29 | Topic modeling <br> Document similarity <br> Fuzzy string matching                                                                                          | [Read Topic Modeling article](https://guides.library.upenn.edu/penntdm/methods/topic_modeling) <br> Turn in Asgn 11                                                                                                                                                   |
| Week 13 <br> Apr 1 - 5 | Analyzing language in Yelp reviews                                                                                                                          | Turn in Asgn12                                                                                                                                                                                                                                                        |
| Week 14 <br> Apr 8 - 12 | Assign 13 TBD                                                                                                                                               | Turn in Asgn 13                                                                                                                                                                                                                                                       |
| Week 15 <br> Apr 15 - 17 | **Wed April 17th** <br> Post final project: <br> -presentation <br> -paper <br> -program <br> Introduction to Neural Networks                               | **Final Project Due**                                                                                                                                                                                                                                                 |
| Finals period <br> Apr 18 - 24 | No final exam <br> **Wed., Apr 24th, 11:59 pm**: Respond to <br> at least 5 final project presentations of <br> classmates.                                 |                                                                                                                                                                                                                                                                       |


---

**Academic Honesty**
The first injunction of the BYU Honor Code is the call to be honest. Students come to the university not only to improve their minds, gain knowledge, and develop skills that will assist them in their life's work, but also to build character. President David O. McKay taught that "character is the highest aim of education" (The Aims of a BYU Education, p. 6). It is the purpose of the BYU Academic Honesty Policy to assist in fulfilling that aim. BYU students should seek to be totally honest in their dealings with others. They should complete their own work and be evaluated based upon that work. They should avoid academic dishonesty and misconduct in all its forms, including but not limited to plagiarism, fabrication or falsification, cheating, and other academic misconduct.

**Use of ChatGPT or other AI generated text or computer code**
Use of ChatGPT or other text generation tools is encouraged ONLY if you understand the output.  These tools can also be used to educate you on specific pieces of code or topics.  If the instructor asks you to explain a particular part of your code or writing, and you are unable, then you may only receive half credit of possible points for the assignment at the instructor's discretion.  Similarly, if you use automatic text generation for any write-ups, only include text that you understand and that is relevant to the assignment.  Proof read and edit both text and code to ensure they meet these requirements.


**Honor Code**
In keeping with the principles of the BYU Honor Code, students are expected to be honest in all of their academic work. Academic honesty means, most fundamentally, that any work you present as your own must in fact be your own work and not that of another. Violations of this principle may result in a failing grade in the course and additional disciplinary action by the university. Students are also expected to adhere to the Dress and Grooming Standards. Adherence demonstrates respect for yourself and others and ensures an effective learning and working environment. It is the university's expectation, and my own expectation in class, that each student will abide by all Honor Code standards. Please call the Honor Code Office at 422-2847 if you have questions about those standards.


**Preventing Sexual Harassment**
Title IX of the Education Amendments of 1972 prohibits sex discrimination against any participant in an educational program or activity that receives federal funds. The act is intended to eliminate sex discrimination in education. Title IX covers discrimination in programs, admissions, activities, and student-to-student sexual harassment. BYU's policy against sexual harassment extends not only to employees of the university, but to students as well. If you encounter unlawful sexual harassment or gender-based discrimination, please talk to your professor; contact the Equal Employment Office at 422-5895 or 367-5689 (24-hours); or contact the Honor Code Office at 422-2847.

**Students with Disabilities**
Brigham Young University is committed to providing a working and learning atmosphere that reasonably accommodates qualified persons with disabilities. If you have any disability which may impair your ability to complete this course successfully, please contact the Services for Students with Disabilities Office (422-2767). Reasonable academic accommodations are reviewed for all students who have qualified, documented disabilities. Services are coordinated with the student and instructor by the SSD Office. If you need assistance or if you feel you have been unlawfully discriminated against on the basis of disability, you may seek resolution through established grievance policy and procedures by contacting the Equal Employment Office at 422-5895, D-285 ASB.


