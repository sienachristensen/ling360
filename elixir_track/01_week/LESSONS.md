# Lessons by Week

## Week 1
- [Why Use Elixir?](https://youtu.be/jgDjKZ4dz_w) (video)
- Install Elixir with [see instructions](https://elixir-lang.org/install.html)
- [Installation video (Windows)](https://www.youtube.com/watch?v=qAT0VtcmSMM&list=PLnVun4ZywZmd_kVRQ2MljTWmi6dtQNTgM)
- Read these sections of Elixir School and practice the code examples:
    - [Elixir Basics](https://elixirschool.com/en/lessons/basics/basics)
    - [Elixir Basics Video](https://youtu.be/4UaRXGSo3dY)
    - [Collections](https://elixirschool.com/en/lessons/basics/collections)
    - [Pattern Matching](https://elixirschool.com/en/lessons/basics/pattern_matching)


- Take the related quiz in Canvas. (due Fri. Jan 15)
- Take the XY Problem quiz (see [this short reading](https://en.wikipedia.org/wiki/XY_problem)) (due Friday 12th Jan)
- Take the survey quiz (by Monday Jan 15th)

Extra Reading:
[Introduction](https://hexdocs.pm/elixir/1.16/introduction.html) and [Basic Types](https://hexdocs.pm/elixir/1.16/basic-types.html)
